package nvmlutils

import (
	"runtime"

	"github.com/NVIDIA/go-nvml/pkg/nvml"
	"go.uber.org/zap"
)

// InitUtils performs NVML initialization: dynamic libraries loading.
func InitUtils(logger *zap.Logger) {
	ret := nvml.Init()

	ErrorCheck(logger.Named("nvmlutils.InitUtils"), ret)
}

func GetDevices(logger *zap.Logger) (devices []nvml.Device) {
	for i := 0; i < GetTotalDevices(logger); i++ {
		device, ret := nvml.DeviceGetHandleByIndex(i)
		ErrorCheck(logger.Named("nvmlutils.GetDevices"), ret)
		devices = append(devices, device)
	}
	return
}

func GetDeviceIndex(logger *zap.Logger, device nvml.Device) int {
	index, ret := nvml.DeviceGetIndex(device)
	ErrorCheck(logger.Named("nvmlutils.GetDeviceIndex"), ret)
	return index
}

func GetDeviceUUID(logger *zap.Logger, device nvml.Device) string {
	uuid, ret := device.GetUUID()
	ErrorCheck(logger.Named("nvmlutils.GetDeviceUUID"), ret)
	return uuid
}

func GetDeviceName(logger *zap.Logger, device nvml.Device) string {
	name, ret := device.GetName()
	ErrorCheck(logger.Named("nvmlutils.GetDeviceName"), ret)
	return name
}

func IsMigDevice(logger *zap.Logger, device nvml.Device) bool {
	uuid := GetDeviceUUID(logger, device)

	gpuMode, _, ret := device.GetMigMode()

	// According to NVML documentation, the NOT_SUPPORTED error
	// is returned if the GPU in question does not have MIG mode support,
	// so in this case, we assume that it is not a MIG device.
	if ret == nvml.ERROR_NOT_SUPPORTED {
		if logger.Core().Enabled(zap.DebugLevel) {
			logger.Named("nvmlutils.IsMigDevice").Debug(
				"Device does not have MIG support", zap.String("device_uuid", uuid))
		}

		return false
	}

	ErrorCheck(logger, ret)
	if gpuMode == nvml.DEVICE_MIG_ENABLE {
		if logger.Core().Enabled(zap.DebugLevel) {
			logger.Named("nvmlutils.IsMigDevice").Debug(
				"Device with MIG support runs in MIG mode", zap.String("device_uuid", uuid))
		}
		return true
	} else {
		if logger.Core().Enabled(zap.DebugLevel) {
			logger.Named("nvmlutils.IsMigDevice").Debug(
				"Device with MIG support does not run in MIG mode", zap.String("device_uuid", uuid))
		}
		return false
	}
}

func GetMigInstanceId(logger *zap.Logger, migdevice nvml.Device) int {
	migId, ret := nvml.DeviceGetGpuInstanceId(migdevice)
	ErrorCheck(logger.Named("nvmlutils.GetMigInstanceId"), ret)
	return migId
}

func GetMigDevices(logger *zap.Logger, device nvml.Device) (devices []nvml.Device) {
	logger = logger.Named("nvmlutils.GetMigDevices")

	if !IsMigDevice(logger, device) {
		return
	}
	name := GetDeviceName(logger, device)
	uuid := GetDeviceUUID(logger, device)

	logger = logger.With(zap.String("device_name", name), zap.String("uuid", uuid))
	// Check possible MIG indexes and skip "not found"
	// as here: https://gitlab.com/nvidia/cloud-native/go-nvlib/-/blob/main/pkg/nvlib/device/device.go#L204
	maxMigs, ret := nvml.DeviceGetMaxMigDeviceCount(device)
	ErrorCheck(logger, ret)

	logger.Debug("Device max MIGs", zap.Int("max_migs", maxMigs))

	for j := 0; j < maxMigs; j++ {
		migDevice, ret := device.GetMigDeviceHandleByIndex(j)
		// Skip not found indexes.
		if ret == nvml.ERROR_NOT_FOUND {
			continue
		}
		// Invalid argument seems to be a valid error here.
		if ret == nvml.ERROR_INVALID_ARGUMENT {
			continue
		}
		ErrorCheck(logger, ret)
		migName := GetDeviceName(logger, migDevice)
		migUUID := GetDeviceUUID(logger, migDevice)

		if l := logger.Check(zap.DebugLevel, "Discovered MIG device"); l != nil {
			l.Write(
				zap.String("mig_name", migName), zap.String("mig_uuid", migUUID),
				zap.String("parent_name", name), zap.String("parent_uuid", uuid))
		}
		devices = append(devices, migDevice)
	}

	return
}

func GetTotalDevices(logger *zap.Logger) int {
	count, ret := nvml.DeviceGetCount()
	ErrorCheck(logger.Named("nvmlutils.GetTotalDevices"), ret)
	return count
}

func GetComputeRunningProcesses(logger *zap.Logger, deviceIdx int, migId int) []nvml.ProcessInfo {
	var device nvml.Device
	logger = logger.Named("nvmlutils.GetComputeRunningProcesses")
	if migId >= 0 {
		device = getMigDeviceByIdx(logger, deviceIdx, migId)
	} else {
		device = getDeviceByIdx(logger, deviceIdx)
	}

	processes, ret := device.GetComputeRunningProcesses()

	ErrorCheck(logger, ret)
	return processes
}

func EnableAccountingMode(logger *zap.Logger, device nvml.Device) {
	ret := device.SetAccountingMode(nvml.FEATURE_ENABLED)
	ErrorCheck(logger.Named("nvmlutils.EnableAccountingMode"), ret)
}

func GetAccountingMode(logger *zap.Logger, device nvml.Device) nvml.EnableState {
	state, ret := device.GetAccountingMode()
	ErrorCheck(logger.Named("nvmlutils.GetAccountingMode"), ret)
	return state
}

func GetAccountingStats(logger *zap.Logger, deviceIdx int, pid uint32) *nvml.AccountingStats {
	logger = logger.Named("nvmlutils.GetAccountingStats")
	stats, ret := getDeviceByIdx(logger, deviceIdx).GetAccountingStats(pid)
	ErrorCheck(logger, ret)
	return &stats
}

func SystemGetCudaDriverVersion(logger *zap.Logger) int {
	cudaVersion, ret := nvml.SystemGetCudaDriverVersion()
	ErrorCheck(logger.Named("nvmlutils.SystemGetCudaDriverVersion"), ret)
	return cudaVersion
}

func SystemGetDriverVersion(logger *zap.Logger) string {
	driver, ret := nvml.SystemGetDriverVersion()
	ErrorCheck(logger.Named("nvmlutils.SystemGetDriverVersion"), ret)
	return driver
}

func GetDeviceMemory(logger *zap.Logger, device nvml.Device) *nvml.Memory {
	memInfo, ret := device.GetMemoryInfo()
	ErrorCheck(logger.Named("nvmlutils.GetDeviceMemory"), ret)
	return &memInfo
}

func GetUtilizationRates(logger *zap.Logger, device nvml.Device) *nvml.Utilization {
	logger = logger.Named("nvmlutils.GetUtilizationRates")

	if IsMigDevice(logger, device) {
		// "On MIG-enabled GPUs, querying device utilization rates is not currently supported."
		return &nvml.Utilization{Gpu: 0, Memory: 0}
	} else {
		utilization, ret := device.GetUtilizationRates()
		ErrorCheck(logger, ret)
		return &utilization
	}
}

func GetDeviceByUUID(logger *zap.Logger, uuid string) nvml.Device {
	logger = logger.Named("nvmlutils.GetDeviceByUUID")

	for _, device := range GetDevices(logger) {
		devUuid := GetDeviceUUID(logger, device)
		if devUuid == uuid {
			return device
		}
	}
	return nil
}

func ErrorCheck(logger *zap.Logger, ret nvml.Return) {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		logger = logger.With(zap.String("caller_source_file", file), zap.Int("caller_source_file_line", line))
	}

	if ok {
		if ret == nvml.ERROR_NOT_FOUND {
			logger.Error("nvml error: ERROR_NOT_FOUND: [a query to find an object was unsuccessful]")
			return
		}
		if ret == nvml.ERROR_NOT_SUPPORTED {
			logger.Error("nvml error: ERROR_NOT_SUPPORTED: [device doesn't support this feature]")
			return
		}
		if ret == nvml.ERROR_NO_PERMISSION {
			logger.Error("nvml error: ERROR_NO_PERMISSION: [user doesn't have permission to perform this operation]")
			return
		}
		if ret != nvml.SUCCESS {
			logger.Fatal("fatal error during nvml operation", zap.String("nvml_error_string", nvml.ErrorString(ret)))
		}
	} else {
		if ret == nvml.ERROR_NOT_FOUND {
			logger.Error("nvml error: ERROR_NOT_FOUND: [a query to find an object was unsuccessful]")
			return
		}
		if ret == nvml.ERROR_NOT_SUPPORTED {
			logger.Error("nvml error: ERROR_NOT_SUPPORTED: [device doesn't support this feature]")
			return
		}
		if ret == nvml.ERROR_NO_PERMISSION {
			logger.Error("nvml error: ERROR_NO_PERMISSION: [user doesn't have permission to perform this operation]")
			return
		}
		if ret != nvml.SUCCESS {
			logger.Fatal("fatal error during nvml operation", zap.String("nvml_error_string", nvml.ErrorString(ret)))
		}
	}
}

func getDeviceByIdx(logger *zap.Logger, deviceIdx int) nvml.Device {
	device, ret := nvml.DeviceGetHandleByIndex(deviceIdx)
	ErrorCheck(logger.Named("nvmlutils.getDeviceByIdx"), ret)
	return device
}

func getMigDeviceByIdx(logger *zap.Logger, deviceIdx int, migId int) nvml.Device {
	logger = logger.Named("nvmlutils.getMigDeviceByIdx")

	device := getDeviceByIdx(logger, deviceIdx)
	migDevice, ret := nvml.DeviceGetMigDeviceHandleByIndex(device, migId)
	ErrorCheck(logger, ret)
	return migDevice
}
