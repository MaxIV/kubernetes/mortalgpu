package podapi

import (
	"go.uber.org/zap"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	k8sClientConfig "sigs.k8s.io/controller-runtime/pkg/client/config"
)

func GetK8sClient(logger *zap.Logger) (client.Client, error) {
	logger = logger.Named("GetK8sClient")

	rc := k8sClientConfig.GetConfigOrDie()
	scheme := runtime.NewScheme()
	if err := corev1.AddToScheme(scheme); err != nil {
		logger.Fatal("error adding to scheme", zap.Error(err))
	}
	controllerClient, err := client.New(rc, client.Options{Scheme: scheme})
	if err != nil {
		logger.Error("error creating new client", zap.Error(err))
		return nil, err
	}

	return controllerClient, nil
}
