package podmonitor

import (
	"context"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	v1core "k8s.io/api/core/v1"
	podresources "k8s.io/kubelet/pkg/apis/podresources/v1"
)

const (
	kubeletSocket        = "/var/lib/kubelet/pod-resources/kubelet.sock"
	kubeletSocketTimeout = 5 * time.Second
)

var ErrUninitialized = errors.New("PodDevices are not Loaded")

type PodDevices struct {
	devicesCache map[string]map[string]map[string]map[v1core.ResourceName][]string
	mtx          sync.RWMutex
}

func New() *PodDevices {
	return &PodDevices{}
}

// GetDeviceIDs returns known device IDs.
func (pm *PodDevices) GetDeviceIDs(namespace, pod, container string, resourceName v1core.ResourceName) ([]string, error) {
	pm.mtx.RLock()
	defer pm.mtx.RUnlock()

	if pm.devicesCache == nil {
		return nil, ErrUninitialized
	}

	namespacePods, ok := pm.devicesCache[namespace]
	if !ok {
		return nil, nil
	}

	podContainers, ok := namespacePods[pod]
	if !ok {
		return nil, nil
	}

	containerDevices, ok := podContainers[container]
	if !ok {
		return nil, nil
	}

	deviceIDs, ok := containerDevices[resourceName]
	if !ok {
		return nil, nil
	}

	return deviceIDs, nil
}

// Load - retrieves devices from Kubelet socket and saves them in PodDevices.
func (pm *PodDevices) Load(ctx context.Context, logger *zap.Logger,
	resourceNames ...v1core.ResourceName) error {
	pm.mtx.Lock()
	defer pm.mtx.Unlock()

	logger = logger.Named("PodMonitor").Named("List").
		With(zap.String("kubelet_socket", kubeletSocket)).
		With(zap.Stringers("resource_names", resourceNames))

	// Connect Kubelet.
	logger.Debug("Connecting to Kubelet")

	conn, err := grpc.Dial(
		kubeletSocket, grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithContextDialer(
			func(ctx context.Context, s string) (net.Conn, error) {
				return net.DialTimeout("unix", kubeletSocket, kubeletSocketTimeout)
			},
		),
	)
	if err != nil {
		logger.Error("Can not connect to Kubelet socket", zap.Error(err))
		return fmt.Errorf("can not connect to Kubelet socket %q: %w", kubeletSocket, err)
	}
	defer func() {
		e := conn.Close()
		if e != nil {
			panic(e)
		}
	}()

	response, err := podresources.NewPodResourcesListerClient(conn).List(ctx, &podresources.ListPodResourcesRequest{})
	if err != nil {
		logger.Error("Can not query PodResources", zap.Error(err))
		return fmt.Errorf("can not query PodResources %s: %w", "/v1.PodResourcesLister/List", err)
	}

	if l := logger.Check(zap.DebugLevel, "/v1.PodResourcesLister/List response"); l != nil {
		l.Write(zap.Any("response", &response))
	}

	// The result type is: map[ NAMESPACE NAME ]map[ POD NAME ]map[ CONTAINER NAME ]map[ RESOURCE NAME ][]string,
	// where the []string contains device IDs.
	var result = make(map[string]map[string]map[string]map[v1core.ResourceName][]string, len(response.GetPodResources()))

	for _, resources := range response.GetPodResources() {
		var (
			name      = resources.GetName()
			namespace = resources.GetNamespace()
		)

		namespacePods, ok := result[namespace]
		if !ok {
			namespacePods = make(map[string]map[string]map[v1core.ResourceName][]string)
			result[namespace] = namespacePods
		}

		podContainers, ok := namespacePods[name]
		if !ok {
			podContainers = make(map[string]map[v1core.ResourceName][]string)
			namespacePods[name] = podContainers
		}

		for _, container := range resources.GetContainers() {
			containerDevices, ok := podContainers[container.GetName()]
			if !ok {
				containerDevices = make(map[v1core.ResourceName][]string, 0)
				podContainers[container.GetName()] = containerDevices
			}

			for _, device := range container.GetDevices() {
				var devResName = v1core.ResourceName(device.GetResourceName())

				// Check if the device must be loaded.
				var toAdd bool

				if len(resourceNames) == 0 {
					toAdd = true
				} else {
					for i := range resourceNames {
						if resourceNames[i] == devResName {
							toAdd = true
							break
						}
					}
				}

				if !toAdd {
					continue
				}

				// Save device IDs.
				containerDevices[devResName] = append(containerDevices[devResName], device.GetDeviceIds()...)
			}
		}
	}

	pm.devicesCache = result

	if l := logger.Check(zap.DebugLevel, "Loaded Container devices"); l != nil {
		l.Write(zap.Any("devices", result))
	}

	return nil
}
