package sharecfg

import (
	"errors"
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// MB - 1 megabyte.
const MB uint64 = 1024 * 1024

var ErrNegativeSharesCount = errors.New("MortalGPU shares count is negative")

// SharesInfo contains information about how a GPU device is supposed to be split.
type SharesInfo struct {
	// ShareSize size of one share in memory bytes.
	ShareSize uint64
	// SharesCount number of shares.
	SharesCount uint64
}

// MarshalLogObject - implements zap.ObjectLogMarshaler.
func (si *SharesInfo) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint64("share_size", si.ShareSize)
	enc.AddUint64("shares_count", si.SharesCount)

	return nil
}

// Copy returns a copy of the data.
func (si *SharesInfo) Copy() *SharesInfo {
	return &SharesInfo{
		ShareSize:   si.ShareSize,
		SharesCount: si.SharesCount,
	}
}

// SharedDevice a physical GPU (or a MIG partition) which is shared between containers.
type SharedDevice struct {
	// UUID shared device UUID (or MIG UUID).
	UUID string `json:"uuid,omitempty" yaml:"uuid"`
	// Index shared device index.
	Index int `json:"index,omitempty" yaml:"index"`
	// Shares extra information about the device.
	Shares SharesInfo `json:"shares,omitempty" yaml:"shares"`
}

// MarshalLogObject - implements zap.ObjectLogMarshaler.
func (sd *SharedDevice) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("shared_device_uuid", sd.UUID)
	enc.AddInt("shared_device_index", sd.Index)

	if err := enc.AddObject("shared_device_shares", &sd.Shares); err != nil {
		return fmt.Errorf("can not Marshal SharedDevice: %w", err)
	}

	return nil
}

// Copy returns a copy of the data.
func (sd *SharedDevice) Copy() *SharedDevice {
	return &SharedDevice{
		UUID:   sd.UUID,
		Index:  sd.Index,
		Shares: *sd.Shares.Copy(),
	}
}

// ComputeMemFromShares returns how much GPU RAM is the given number of shares represent.
func (sd *SharedDevice) ComputeMemFromShares(sharesCount int) uint64 {
	return sd.Shares.ShareSize * uint64(sharesCount)
}

type DeviceSharingConfig struct {
	// Uuid - NVIDIA device UUID.
	Uuid []string `json:"uuid,omitempty" yaml:"uuid,omitempty"`
	// Model - human readable GPU model name
	// used for selecting GPUs to handle.
	// If Uuid is set, then Model is ignored.
	ModelName []string `json:"modelName,omitempty" yaml:"modelName,omitempty"`
	// Migid - MIG device id.
	Migid []int `json:"migid,omitempty" yaml:"migid,omitempty"`
	// ResourceName - Kubernetes resource name to handle.
	ResourceName string `json:"resourceName,omitempty" yaml:"resourceName,omitempty"`
	// MetagpusPerGpu - number of meta GPUs to split the GPU on.
	MetagpusPerGpu uint64 `json:"metagpusPerGpu,omitempty" yaml:"metagpusPerGpu,omitempty"`
	// MetagpusPerGpuMemoryChunkMB - GPU memory chunk size to
	// split the GPU to meta GPUs with the MetagpusPerGpuMemoryChunkMB
	// memory sizes.
	MetagpusPerGpuMemoryChunkMB uint64 `json:"metagpusPerGpuMemoryChunkMB,omitempty" yaml:"metagpusPerGpuMemoryChunkMB,omitempty"` //nolint:lll // Struct tags.
	// ReservedChunks - number of MortalGPU chunks which will be excluded from
	// allocation so that they can be used by other workloads.
	ReservedChunks uint64 `json:"reservedChunks,omitempty" yaml:"reservedChunks,omitempty"`
}

// MarshalLogObject - implements zap.ObjectLogMarshaler.
func (d *DeviceSharingConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("device_sharing_config_resource_name", d.ResourceName)
	enc.AddUint64("device_sharing_config_metagpus_per_gpu", d.MetagpusPerGpu)
	enc.AddUint64("device_sharing_config_metagpus_per_gpu_memory_chunk_mb", d.MetagpusPerGpuMemoryChunkMB)
	enc.AddUint64("device_sharing_config_reserved_chunks", d.ReservedChunks)

	zap.Strings("device_sharing_config_uuid", d.Uuid).AddTo(enc)
	zap.Strings("device_sharing_config_model_name", d.ModelName).AddTo(enc)
	zap.Ints("device_sharing_config_mig_id", d.Migid).AddTo(enc)

	return nil
}

// ComputeSharesInfo computes shares info based on the sharing configuration and total device memory.
func (d *DeviceSharingConfig) ComputeSharesInfo(totalMemory uint64) (SharesInfo, error) {
	var result SharesInfo

	// 3 cases are possible: only MetagpusPerGpu is set, only MetagpusPerGpuMemoryChunkMB is set and both are set.
	// If MetagpusPerGpu is set, it will always be used.
	// If MetagpusPerGpu is not set and MetagpusPerGpuMemoryChunkMB is set, MetagpusPerGpuMemoryChunkMB will be used.
	// If none is set, then the configuration is invalid (handled during validation in sharecfg.NewDeviceSharingConfig).

	if d.MetagpusPerGpu > 0 {
		// Splitting by fixed number of MortalGPU devices.
		result.SharesCount = d.MetagpusPerGpu
		result.ShareSize = totalMemory / result.SharesCount
	} else {
		// We are splitting based on memory size chunks.
		// memory.Total is in bytes according to NVIDIA API.
		result.ShareSize = d.MetagpusPerGpuMemoryChunkMB * MB
		result.SharesCount = totalMemory / result.ShareSize
	}

	// Accommodate reserved chunks.
	if d.ReservedChunks > 0 {
		if d.ReservedChunks > result.SharesCount {
			return SharesInfo{}, fmt.Errorf(
				"%w: %q: computed SharesCount is %d while ReservedChunks is %d",
				ErrNegativeSharesCount, d.ResourceName, result.SharesCount, d.ReservedChunks)
		}

		result.SharesCount -= d.ReservedChunks
	}

	return result, nil
}

type DevicesSharingConfigs struct {
	Configs []*DeviceSharingConfig

	logger *zap.Logger
}

var shareCfg *DevicesSharingConfigs

func NewDeviceSharingConfig(logger *zap.Logger, cfg []*DeviceSharingConfig) *DevicesSharingConfigs {
	if shareCfg != nil {
		return shareCfg
	}

	shareCfg = &DevicesSharingConfigs{Configs: cfg, logger: logger.Named("DeviceSharingConfig")}
	shareCfg.validateSharingConfiguration()

	return shareCfg
}

func (c *DevicesSharingConfigs) validateSharingConfiguration() {
	if len(c.Configs) == 0 {
		c.logger.Fatal("missing gpu sharing configuration, can't proceed")
	}

	for i, devCfg := range c.Configs {
		// TODO: check config sanity (resourceName regex)

		// 3 cases are possible: only MetagpusPerGpu is set, only MetagpusPerGpuMemoryChunkMB is set and both are set.
		// If MetagpusPerGpu is set, it will always be used.
		// If MetagpusPerGpu is not set and MetagpusPerGpuMemoryChunkMB is set, MetagpusPerGpuMemoryChunkMB will be used.
		// If none is set, then the configuration is invalid.
		if devCfg.MetagpusPerGpu == 0 && devCfg.MetagpusPerGpuMemoryChunkMB == 0 {
			c.logger.Fatal("Wrong GPU sharing configuration: either deviceSharing.metagpusPerGpu or deviceSharing.metagpusPerGpuMemoryChunkMB must be > 0",
				zap.Int("device_sharing_config_index", i),
				zap.Object("device_sharing_config", devCfg),
			)
		} else if devCfg.MetagpusPerGpu != 0 && devCfg.MetagpusPerGpuMemoryChunkMB != 0 {
			c.logger.Warn("GPU sharing configuration: both deviceSharing.metagpusPerGpu and deviceSharing.metagpusPerGpuMemoryChunkMB are set, using deviceSharing.metagpusPerGpu",
				zap.Int("device_sharing_config_index", i),
				zap.Object("device_sharing_config", devCfg),
			)
		}
	}
}

func (c *DevicesSharingConfigs) GetDeviceSharingConfig(devUuid, modelName string, migId int) (*DeviceSharingConfig, error) {
	for _, devCfg := range c.Configs {
		if IsManagedDevice(devCfg, devUuid, modelName, migId) {
			return devCfg, nil
		}
	}

	if migId < 0 {
		return nil, fmt.Errorf("device uuid: %s not found in sharing configs", devUuid)
	} else {
		return nil, fmt.Errorf("MIG instance %d on device uuid: %s not found in sharing configs", migId, devUuid)
	}
}

// IsManagedDevice checks if the given device has sharing configuration.
func IsManagedDevice(devCfg *DeviceSharingConfig, devUuid, modelName string, migId int) bool {
	var devmatch bool = false

	if len(devCfg.Uuid) != 0 {
		// Match by device UUID.
		for _, uuid := range devCfg.Uuid {
			if uuid == devUuid {
				devmatch = true
				break
			}
		}
	} else if len(devCfg.ModelName) != 0 {
		// Match by device model name.
		for _, model := range devCfg.ModelName {
			if modelName == model {
				devmatch = true
				break
			}
		}
	} else {
		// Always matches, no filters.
		devmatch = true
	}

	if devmatch {
		// No MIG is enabled or no MIG id filter.
		if migId < 0 || len(devCfg.Migid) == 0 {
			return true
		}

		// MIG is enabled and the MIG id filter is set.
		for _, iid := range devCfg.Migid {
			if migId == iid {
				return true
			}
		}
	}

	return false
}
