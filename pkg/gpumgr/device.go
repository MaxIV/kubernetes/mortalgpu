package gpumgr

import (
	"fmt"
	"os"

	"github.com/NVIDIA/go-nvml/pkg/nvml"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type DeviceMemory struct {
	// Total - memory, bytes.
	Total uint64
	// Free - memory, bytes.
	Free uint64
	// Used - memory, bytes.
	Used uint64
	// ShareSize - memory, bytes.
	ShareSize uint64
}

var _ zapcore.ObjectMarshaler = (*DeviceMemory)(nil)

func (d *DeviceMemory) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint64("total_bytes", d.Total)
	enc.AddUint64("free_bytes", d.Free)
	enc.AddUint64("used_bytes", d.Used)
	enc.AddUint64("share_size_bytes", d.ShareSize)

	return nil
}

type DeviceUtilization struct {
	Gpu    uint32
	Memory uint32
}

var _ zapcore.ObjectMarshaler = (*DeviceUtilization)(nil)

func (d *DeviceUtilization) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint32("gpu", d.Gpu)
	enc.AddUint32("memory", d.Memory)

	return nil
}

// GPUMigInstanceIDNoMIG constant value to indicate that the device is not using MIG.
const GPUMigInstanceIDNoMIG = -1

type GpuDevice struct {
	logger *zap.Logger

	// uuid - MIG device UUID, if MIG mode, otherwise a normal device UUID.
	uuid string
	// DeviceUUID - Parent device UUID if MIG device, usual device UUID if not MIG mode.
	deviceUUID string
	// modelName - human readable device model name from NVIDIA API.
	modelName string

	// resourceName Kubernetes resource name.
	resourceName string

	// nodename Kubernetes worker node name.
	nodename string

	deviceIndex     int
	migID           int
	shares          int
	allocatedShares int

	utilization DeviceUtilization
	memory      DeviceMemory
}

// GetUUID - MIG device UUID, if MIG mode, otherwise a normal device UUID.
func (d *GpuDevice) GetUUID() string { return d.uuid }

// GetDeviceUUID - Parent device UUID if MIG device, usual device UUID if not MIG mode.
func (d *GpuDevice) GetDeviceUUID() string { return d.deviceUUID }

// GetModelName - a human readable device name from NVIDIA API.
func (d *GpuDevice) GetModelName() string { return d.modelName }

func (d *GpuDevice) GetDeviceIndex() int               { return d.deviceIndex }
func (d *GpuDevice) GetMigId() int                     { return d.migID }
func (d *GpuDevice) IsMigDevice() bool                 { return d.migID != GPUMigInstanceIDNoMIG }
func (d *GpuDevice) GetShares() int                    { return d.shares }
func (d *GpuDevice) GetAllocatedShares() int           { return d.allocatedShares }
func (d *GpuDevice) GetResourceName() string           { return d.resourceName }
func (d *GpuDevice) GetUtilization() DeviceUtilization { return d.utilization }
func (d *GpuDevice) GetMemory() DeviceMemory           { return d.memory }
func (d *GpuDevice) GetNodeName() string               { return d.nodename }
func (d *GpuDevice) GetCopy() *GpuDevice {
	return &GpuDevice{
		uuid:            d.uuid,
		deviceUUID:      d.deviceUUID,
		deviceIndex:     d.deviceIndex,
		migID:           d.migID,
		shares:          d.shares,
		allocatedShares: d.allocatedShares,
		resourceName:    d.resourceName,
		utilization:     d.utilization,
		memory:          d.memory,
		nodename:        d.nodename,
		logger:          d.logger,
		modelName:       d.modelName,
	}
}

// SetAllocatedShares sets allocated shares.
func (d *GpuDevice) SetAllocatedShares(s int) { d.allocatedShares = s }

// SetUUID set MIG device UUID, if MIG mode, otherwise a normal device UUID.
func (d *GpuDevice) SetUUID(u string) { d.uuid = u }

var _ zapcore.ObjectMarshaler = (*GpuDevice)(nil)

func (d *GpuDevice) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("uuid", d.uuid)
	enc.AddString("device_uuid", d.deviceUUID)
	enc.AddString("device_model_name", d.modelName)
	enc.AddInt("device_index", d.deviceIndex)
	enc.AddInt("mig_id", d.migID)
	enc.AddInt("shares", d.shares)
	enc.AddInt("allocated_shares", d.allocatedShares)
	enc.AddString("resource_name", d.resourceName)

	if err := enc.AddObject("utilization", &d.utilization); err != nil {
		return fmt.Errorf("can not marshal utilization: %w", err)
	}

	if err := enc.AddObject("memory", &d.memory); err != nil {
		return fmt.Errorf("can not marshal memory: %w", err)
	}

	enc.AddString("node_name", d.nodename)

	return nil
}

func NewGpuDevice(logger *zap.Logger,
	miguuid, deviceuuid, modelName string,
	index, miginstanceid int, utilization nvml.Utilization,
) *GpuDevice {
	dev := &GpuDevice{
		uuid:        miguuid,
		deviceUUID:  deviceuuid,
		deviceIndex: index,
		modelName:   modelName,
		migID:       miginstanceid,
		utilization: DeviceUtilization{Gpu: utilization.Gpu, Memory: utilization.Memory / uint32(MB)},

		logger: logger.Named("GpuDevice").With(
			zap.String("gpu_device_uuid", deviceuuid),
			zap.String("gpu_device_mig_uuid", miguuid),
			zap.Int("gpu_device_index", index),
			zap.Int("gpu_device_mig_index", miginstanceid),
		),
	}

	if l := logger.Check(zap.DebugLevel, "NewGPUDevice"); l != nil {
		l.Write(
			zap.Uint32("gpu_utilization_memory", dev.utilization.Memory),
			zap.Uint32("gpu_utilization_gpu", dev.utilization.Gpu))
	}

	// set nodename
	dev.setNodename()

	return dev
}

func (d *GpuDevice) setNodename() {
	hostname, err := os.Hostname()
	if err != nil {
		d.logger.Error("failed to detect hostname", zap.Error(err))
	}

	d.logger = d.logger.With(zap.String("node_hostname", hostname))

	d.nodename = hostname
}

func (d *GpuDevice) setGpuSharingConfig(
	deviceSharingConfigs *sharecfg.DevicesSharingConfigs, memory *nvml.Memory,
) bool {
	deviceSharing, err := deviceSharingConfigs.GetDeviceSharingConfig(d.deviceUUID, d.modelName, d.migID)
	if err != nil {
		if l := d.logger.Check(zap.DebugLevel, "unable to find sharing configs for device"); l != nil {
			l.Write(zap.Error(err))
		}

		return false
	}

	sharesInfo, err := deviceSharing.ComputeSharesInfo(memory.Total)
	if err != nil {
		d.logger.Named("setGpuSharingConfig").Error(
			"Can not compute Shares", zap.Error(err))

		return false
	}

	d.shares = int(sharesInfo.SharesCount)
	d.resourceName = deviceSharing.ResourceName

	d.memory = DeviceMemory{
		Total:     memory.Total,
		Free:      memory.Free,
		Used:      memory.Used,
		ShareSize: sharesInfo.ShareSize,
	}

	// Enrich logger.
	d.logger = d.logger.With(
		zap.String("gpu_resource_name", d.resourceName),
		zap.Int("gpu_metagpu_shares", d.shares),
		zap.Uint64("gpu_metagpu_share_size", d.memory.ShareSize),
		zap.Bool("gpu_metagpu_split_by_count", deviceSharing.MetagpusPerGpu != 0),
		zap.Bool("gpu_metagpu_split_by_chunk_size", deviceSharing.MetagpusPerGpu == 0),
	)
	d.logger.Debug("sharing configs for device")

	return true
}

// getDeviceLogger returns logger enriched with the device-related fields.
func (d *GpuDevice) getDeviceLogger() *zap.Logger {
	return d.logger
}
