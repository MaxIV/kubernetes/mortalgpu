package gpumgr

import (
	"regexp"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	v1core "k8s.io/api/core/v1"
)

type ContainerDevice struct {
	gpuDevice       *GpuDevice
	allocatedShares int32
}

func (cd *ContainerDevice) GetGPUDevice() *GpuDevice  { return cd.gpuDevice }
func (cd *ContainerDevice) GetAllocatedShares() int32 { return cd.allocatedShares }
func (cd *ContainerDevice) GetCopy() *ContainerDevice {
	return &ContainerDevice{
		gpuDevice:       cd.gpuDevice.GetCopy(),
		allocatedShares: cd.allocatedShares,
	}
}

type GpuContainer struct {
	containerID       string
	containerName     string
	podID             string
	podNamespace      string
	resourceName      string
	nodename          string
	processes         []*GpuProcess
	devices           []*ContainerDevice
	gpuMemTotal       uint64
	podMetagpuRequest int64
	podMetagpuLimit   int64
}

func (g *GpuContainer) GetContainerId() string         { return g.containerID }
func (g *GpuContainer) GetContainerName() string       { return g.containerName }
func (g *GpuContainer) GetPodId() string               { return g.podID }
func (g *GpuContainer) GetPodNamespace() string        { return g.podNamespace }
func (g *GpuContainer) GetPodMetaGPURequest() int64    { return g.podMetagpuRequest }
func (g *GpuContainer) GetPodMetaGPULimit() int64      { return g.podMetagpuLimit }
func (g *GpuContainer) GetResourceName() string        { return g.resourceName }
func (g *GpuContainer) GetNodeName() string            { return g.nodename }
func (g *GpuContainer) GetProcesses() []*GpuProcess    { return g.processes }
func (g *GpuContainer) GetDevices() []*ContainerDevice { return g.devices }
func (g *GpuContainer) GetCopy() *GpuContainer {
	var (
		processes = make([]*GpuProcess, len(g.processes))
		devices   = make([]*ContainerDevice, len(g.devices))
	)

	for i, p := range g.processes {
		processes[i] = p.GetCopy()
	}

	for i, d := range g.devices {
		devices[i] = d.GetCopy()
	}

	return &GpuContainer{
		containerID:       g.containerID,
		containerName:     g.containerName,
		podID:             g.podID,
		podNamespace:      g.podNamespace,
		podMetagpuRequest: g.podMetagpuRequest,
		podMetagpuLimit:   g.podMetagpuLimit,
		resourceName:      g.resourceName,
		nodename:          g.nodename,
		processes:         processes,
		devices:           devices,
		gpuMemTotal:       g.gpuMemTotal,
	}
}

var _ zapcore.ObjectMarshaler = (*GpuContainer)(nil)

func (g *GpuContainer) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("pod_namespace", g.podNamespace)
	enc.AddString("pod_name", g.podID)
	enc.AddString("container_name", g.containerName)
	enc.AddString("resource_name", g.resourceName)
	enc.AddInt64("gpu_request", g.podMetagpuRequest)
	enc.AddInt64("gpu_limit", g.podMetagpuLimit)
	enc.AddUint64("gpu_mem_total", g.gpuMemTotal)
	enc.AddString("container_id", g.containerID)
	enc.AddString("node_name", g.nodename)

	return nil
}

var reMetaDevicePrefix = regexp.MustCompile(`mortalgpu-meta-\d+-\d+-`)

func getContainerID(log *zap.Logger, pod *v1core.Pod, containerName string) string {
	logger := log.Named("getContainerId")

	for i := range pod.Status.ContainerStatuses {
		status := &pod.Status.ContainerStatuses[i]

		if status.Name == containerName {
			idx := strings.Index(status.ContainerID, "//")
			if idx != -1 {
				logger.Debug("container id",
					zap.String("pod_name", pod.Name),
					zap.String("pod_namespace", pod.Namespace),
					zap.String("containerId", status.ContainerID[idx+2:]))

				return status.ContainerID[idx+2:]
			}

			logger.Error("can't extract container id",
				zap.String("pod_name", pod.Name),
				zap.String("pod_namespace", pod.Namespace))
		}
	}

	return ""
}

func (g *GpuContainer) setAllocatedGpus(log *zap.Logger, deviceIDs []string, gpuDevices map[string]*GpuDevice) {
	logger := log.Named("setAllocatedGpus")

	gpuAllocationMap := make(map[string]int32)

	// Summarize allocated MetaGPU shares per GPU UUID.
	for _, metaDeviceID := range deviceIDs {
		deviceUUID := strings.TrimSuffix(reMetaDevicePrefix.ReplaceAllString(metaDeviceID, ""), "\n")
		gpuAllocationMap[deviceUUID]++
	}

	if l := logger.Check(zap.DebugLevel, "gpuAllocationMap"); l != nil {
		l.Write(zap.Any("gpuAllocationMap", gpuAllocationMap))
	}

	for uuid, allocatedShares := range gpuAllocationMap {
		device, ok := gpuDevices[uuid]
		if ok {
			g.devices = append(g.devices, &ContainerDevice{
				gpuDevice:       device,
				allocatedShares: allocatedShares,
			})
		}
	}

	if l := logger.Check(zap.DebugLevel, "devices"); l != nil {
		l.Write(zap.Any("ContainerDevice_s", gpuDevices))
	}
}

func NewGpuContainer(log *zap.Logger,
	containerID, containerName, podID, podNamespace, resourceName, nodename string,
	metagpuRequests, metagpuLimits int64,
	deviceIDs []string,
	gpuDevices map[string]*GpuDevice,
) *GpuContainer {
	p := &GpuContainer{
		containerID:       containerID,
		podID:             podID,
		containerName:     containerName,
		podNamespace:      podNamespace,
		podMetagpuRequest: metagpuRequests,
		podMetagpuLimit:   metagpuLimits,
		resourceName:      resourceName,
		nodename:          nodename,
	}

	logger := log.With(
		zap.String("container_id", p.containerID),
		zap.String("pod_id", p.podID),
		zap.String("container_name", p.containerName),
		zap.String("pod_namespace", p.podNamespace),
		zap.String("node_name", p.nodename)).
		Named("GpuContainer")

	logger.Debug("NewGpuContainer",
		zap.String("resource_name", p.resourceName),
		zap.Int64("pod_metagpu_request", p.podMetagpuRequest),
		zap.Int64("pod_metagpu_limit", p.podMetagpuLimit))

	// discover allocated GPUs
	p.setAllocatedGpus(logger, deviceIDs, gpuDevices)

	return p
}
