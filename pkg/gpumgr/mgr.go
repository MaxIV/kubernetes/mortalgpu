// Package gpumgr implements MortalGPUs detection, processes accounting and
// memory limits enforcement.
package gpumgr

import (
	"context"
	"os"
	"strconv"
	"sync"
	"time"

	"go.uber.org/zap"
	v1core "k8s.io/api/core/v1"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/nvmlutils"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/podapi"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/podmonitor"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

const (
	MB uint64 = 1024 * 1024
	// gpuCacheUpdateInterval period to refresh physical GPU states.
	gpuCacheUpdateInterval = time.Second * 5
)

const (
	lkIsMigDevice                  = "is_mig_device"
	lkDeviceConfig                 = "device_config"
	lkGPUDevicesList               = "gpu_devices_list"
	lkGPUContainer                 = "gpu_container"
	lkGPUContainerProcesses        = "processes"
	lkGPUProcessPID                = "process_pid"
	lkGPUProcessGPUInstanceID      = "process_gpu_instance_id"
	lkGPUProcessComputeInstanceID  = "process_compute_instance_id"
	lkGPUProcessUsedGPUMemoryBytes = "process_used_gpu_memory_bytes"
	lkGPUDeviceUUID                = "device_uuid"
	lkGPUParentDeviceUUID          = "parent_device_uuid"

	lkNamespace     = "namespace"
	lkPodName       = "pod_name"
	lkContainerName = "container_name"
	lkResourceName  = "resource_name"
	lkAnnotationKey = "annotation_key"
)

type GpuMgr struct {
	containerLevelVisibilityToken string
	deviceLevelVisibilityToken    string

	hostNodeName string

	gpuUUIDDevices map[string]*GpuDevice

	gpuShareConfigs *sharecfg.DevicesSharingConfigs

	// map of gpu containers, i.e. a container + allocated GPU + resources data.
	// It is possible that one Kubernetes container will be multiple times in the
	// map because a container is added for each GPU resource.
	// One should use GpuContainer.GetResourceName() to get the name of the GPU.
	// Key is the MortalGPU Kubernetes resource name.
	gpuContainers map[string][]*GpuContainer

	// collection of the gpu processes: the anonymous and active running
	// First level key is the MortalGPU resource name.
	gpuContainersCollector map[string][]*GpuContainer

	logger *zap.Logger

	mtx sync.RWMutex
}

type GpuDeviceInfo struct {
	Node     string
	Metadata map[string]string
	Devices  []*GpuDevice
}

func (m *GpuMgr) startGpuStatusCache() {
	go func() {
		for {
			time.Sleep(gpuCacheUpdateInterval)

			// This "global" lock during the update procedure is not optimal
			// but might work somewhat well if not too high throughput.
			m.mtx.Lock()

			// set gpu devices
			m.setGpuDevices()
			// set gpu containers
			m.discoverGpuContainers()
			// set active gpu processes
			m.enrichGpuContainer()
			// set per physical GPU allocation statistics.
			m.updateAllocatedShares()
			// set final gpu containers list
			m.setGpuContainers()

			m.mtx.Unlock()

			// Print summary.
			m.logSummary()
		}
	}()
}

func (m *GpuMgr) logSummary() {
	if m.logger.Core().Enabled(zap.InfoLevel) {
		logger := m.logger.Named("logSummary")

		m.mtx.RLock()

		// Log GPUs information.
		{
			iLog := logger.Named("GPUInfo")
			for _, dev := range m.gpuUUIDDevices {
				iLog.Info("GPU summary", zap.Object("gpu", dev))
			}
		}

		for resourceName, containers := range m.gpuContainers {
			iLog := logger.With(zap.String("resource_name", resourceName))

			for _, container := range containers {
				if iLog.Core().Enabled(zap.DebugLevel) {
					iLog.Debug("GPU container verbose summary",
						zap.Object(lkGPUContainer, container),
						zap.Objects(lkGPUContainerProcesses, container.GetProcesses()),
					)
				} else {
					iLog.Info("GPU container summary", zap.Object(lkGPUContainer, container))
				}
			}
		}

		m.mtx.RUnlock()
	}
}

func (m *GpuMgr) setGpuDevices() {
	logger := m.logger.Named("setGpuDevices")

	gpuDevices := make(map[string]*GpuDevice, 0)

	for _, device := range nvmlutils.GetDevices(logger) {
		uuid := nvmlutils.GetDeviceUUID(logger, device)
		idx := nvmlutils.GetDeviceIndex(logger, device)

		iLogger := logger.With(zap.String("nvml_device_uuid", uuid), zap.Int("nvml_device_index", idx))

		// create GPUDevice for each MIG if running in MIG mode
		if nvmlutils.IsMigDevice(logger, device) {
			iLogger = iLogger.With(zap.Bool(lkIsMigDevice, true))

			iLogger.Debug("Device is a MIG device, getting child MIG devices")

			for _, migdevice := range nvmlutils.GetMigDevices(iLogger, device) {
				migidx := nvmlutils.GetMigInstanceId(iLogger, migdevice)
				miguuid := nvmlutils.GetDeviceUUID(iLogger, migdevice)
				deviceMemory := nvmlutils.GetDeviceMemory(iLogger, migdevice)
				utilization := nvmlutils.GetUtilizationRates(iLogger, device)
				name := nvmlutils.GetDeviceName(logger, device)

				// create new MetaGPU device and manage is there is share config for it
				metaGpuDevice := NewGpuDevice(iLogger, miguuid, uuid, name, idx, migidx, *utilization)

				if metaGpuDevice.setGpuSharingConfig(m.gpuShareConfigs, deviceMemory) {
					gpuDevices[metaGpuDevice.GetUUID()] = metaGpuDevice

					// Debug device config.
					if l := iLogger.Check(zap.DebugLevel, "Mortal GPU MIG device configured"); l != nil {
						l.Write(zap.Any(lkDeviceConfig, metaGpuDevice))
					}
				}
			}
		} else {
			iLogger = iLogger.With(zap.Bool(lkIsMigDevice, false))

			iLogger.Debug("Device is not a MIG device, working with entire GPU as GPUDevice")

			// otherwise work with entire GPU as GPUDevice
			deviceMemory := nvmlutils.GetDeviceMemory(iLogger, device)
			utilization := nvmlutils.GetUtilizationRates(iLogger, device)
			name := nvmlutils.GetDeviceName(logger, device)
			// create new MetaGPU device and manage is there is share config for it
			metaGpuDevice := NewGpuDevice(iLogger, uuid, uuid, name, idx, GPUMigInstanceIDNoMIG, *utilization)

			if metaGpuDevice.setGpuSharingConfig(m.gpuShareConfigs, deviceMemory) {
				gpuDevices[metaGpuDevice.GetUUID()] = metaGpuDevice

				// Debug device config.
				if l := iLogger.Check(zap.DebugLevel, "Mortal GPU device configured"); l != nil {
					l.Write(zap.Any(lkDeviceConfig, metaGpuDevice))
				}
			}
		}
	}

	if l := logger.Check(zap.DebugLevel, "Collected GPU devices"); l != nil {
		l.Write(zap.Any(lkGPUDevicesList, gpuDevices))
	}

	m.gpuUUIDDevices = gpuDevices
}

func (m *GpuMgr) enrichGpuContainer() {
	if l := m.logger.Check(zap.DebugLevel, "Starting: Matching running processes with GPU devices"); l != nil {
		l.Write(zap.Any(lkGPUDevicesList, m.gpuUUIDDevices))
	}

	for _, device := range m.gpuUUIDDevices {
		logger := device.getDeviceLogger().Named("enrichGpuContainer")

		logger.Debug("Getting running processes for a GPU Device")

		// Bug: It is supposed to be called with device.MigId but for some reason
		// the call fails with error in GetComputeRunningProcesses => getMigDeviceByIdx => nvml.DeviceGetMigDeviceHandleByIndex
		// so as a workaround we don't use MIG here.
		processes := nvmlutils.GetComputeRunningProcesses(logger, device.GetDeviceIndex(), GPUMigInstanceIDNoMIG)

		if l := logger.Check(zap.DebugLevel, "got GPU processes"); l != nil {
			l.Write(zap.Any("processes", processes))
		}

		for _, nvmlProcessInfo := range processes {
			iLogger := logger.With(
				zap.Uint32(lkGPUProcessPID, nvmlProcessInfo.Pid),
				zap.Uint32(lkGPUProcessGPUInstanceID, nvmlProcessInfo.GpuInstanceId),
				zap.Uint32(lkGPUProcessComputeInstanceID, nvmlProcessInfo.ComputeInstanceId),
				zap.Uint64(lkGPUProcessUsedGPUMemoryBytes, nvmlProcessInfo.UsedGpuMemory),
			)

			// skip if process does not belong to this MIG instance
			if device.IsMigDevice() && device.GetMigId() != int(nvmlProcessInfo.GpuInstanceId) {
				iLogger.Debug("Compute running process does not belong to this device")

				continue
			}

			iLogger.Debug("Found a GPU on which the process is running")

			var gpuProc *GpuProcess
			// MIG devices do not support accounting yet (2023, December, 06),
			// so the CPU utilization is set to 0.
			if device.IsMigDevice() {
				gpuProc = NewGpuProcess(iLogger,
					nvmlProcessInfo.Pid, 0, nvmlProcessInfo.UsedGpuMemory,
					device.GetUUID(), device.GetDeviceUUID(),
					device.GetMigId(), int32(nvmlProcessInfo.ComputeInstanceId), int32(nvmlProcessInfo.GpuInstanceId))
			} else {
				stats := nvmlutils.GetAccountingStats(iLogger, device.GetDeviceIndex(), nvmlProcessInfo.Pid)

				gpuProc = NewGpuProcess(iLogger,
					nvmlProcessInfo.Pid, stats.GpuUtilization, nvmlProcessInfo.UsedGpuMemory,
					device.GetUUID(), device.GetDeviceUUID(),
					device.GetMigId(), int32(nvmlProcessInfo.ComputeInstanceId), int32(nvmlProcessInfo.GpuInstanceId))
			}

			gpuProcContainerID := gpuProc.GetContainerId()

			for _, containers := range m.gpuContainersCollector {
				for _, container := range containers {
					if l := iLogger.Check(zap.DebugLevel, "Matching a GPU process with a GPU container"); l != nil {
						l.Write(
							zap.String("containers_collector_container_name", container.containerName),
							zap.String("containers_collector_pod_namespace", container.podNamespace),
							zap.String("containers_collector_container_id", container.containerID),
							zap.String("containers_collector_pod_id", container.podID),
							zap.String("gpu_process_container_id", gpuProcContainerID),
							zap.Bool("is_match", container.containerID == gpuProcContainerID),
							zap.Object("process_info", gpuProc))
					}

					if container.containerID == gpuProcContainerID {
						container.processes = append(container.processes, gpuProc)
						container.gpuMemTotal += gpuProc.gpuMemBytes
					}
				}
			}
		}
	}
}

func (m *GpuMgr) updateAllocatedShares() {
	logger := m.logger.Named("updateAllocatedShares")

	stats := make(map[string]int, len(m.gpuUUIDDevices))

	for _, containers := range m.gpuContainersCollector {
		for _, container := range containers {
			var iLogger *zap.Logger

			if logger.Core().Enabled(zap.DebugLevel) {
				iLogger = logger.With(zap.Object("gpu_container", container))
				iLogger.Debug("collecting allocation data for a container")
			}

			for _, device := range container.devices {
				if logger.Core().Enabled(zap.DebugLevel) {
					iLogger.Debug("container GPU device",
						zap.String("parent_device_uuid", device.gpuDevice.GetDeviceUUID()),
						zap.String("device_uuid", device.gpuDevice.GetUUID()),
						zap.Int32("allocated_shares", device.allocatedShares))
				}

				alloc, ok := stats[device.gpuDevice.GetUUID()]
				if !ok {
					alloc = int(device.allocatedShares)
				} else {
					alloc += int(device.allocatedShares)
				}

				stats[device.gpuDevice.GetUUID()] = alloc
			}
		}
	}

	for _, device := range m.gpuUUIDDevices {
		device.SetAllocatedShares(stats[device.GetUUID()])

		if logger.Core().Enabled(zap.DebugLevel) {
			logger.Debug("GPU allocated statistics",
				zap.String("device_uuid", device.GetUUID()),
				zap.Int("allocated_shares", device.GetAllocatedShares()))
		}
	}
}

func (m *GpuMgr) setGpuContainers() {
	m.gpuContainers = m.gpuContainersCollector

	if l := m.logger.Named("setGpuContainers").Check(zap.DebugLevel, "discovered GPU containers"); l != nil {
		l.Write(zap.Int("count", len(m.gpuContainers)))
	}
}

func (m *GpuMgr) discoverGpuContainers() {
	logger := m.logger.Named("discoverGpuContainers")

	// Get Pods.
	k8sClient, err := podapi.GetK8sClient(logger)
	if err != nil {
		logger.Error("can not init K8S client", zap.Error(err))

		return
	}

	// ToDo: investigate whether the List operation is optimal or we should set "resource version"
	// to query "Any" version and not query ALL KubeAPI servers to find the
	// cluster-consistent view.
	podsList := &v1core.PodList{}

	if err := k8sClient.List(context.Background(), podsList); err != nil {
		logger.Error("can not list Pods", zap.Error(err))

		return
	}

	// Load device sharing config.
	cfg := m.gpuShareConfigs

	deviceResourceNames := make([]v1core.ResourceName, 0, len(cfg.Configs))
	for _, c := range cfg.Configs {
		deviceResourceNames = append(deviceResourceNames, v1core.ResourceName(c.ResourceName))
	}

	// Get container devices.
	podmon := podmonitor.New()
	if err := podmon.Load(context.Background(), logger, deviceResourceNames...); err != nil {
		logger.Error("Can not load PodMonitor Container Devices", zap.Error(err))

		return
	}

	// reset gpu containers collector
	m.gpuContainersCollector = make(map[string][]*GpuContainer)

	for pLI := 0; pLI < len(podsList.Items); pLI++ {
		pod := &podsList.Items[pLI]

		for cLI := 0; cLI < len(pod.Spec.Containers); cLI++ {
			container := &pod.Spec.Containers[cLI]

			for _, resourceName := range deviceResourceNames {
				requests, isreq := container.Resources.Requests[resourceName]
				strResourceName := resourceName.String()

				if limits, ok := container.Resources.Limits[resourceName]; ok {
					// backward compatible logic: if no requests assume it is equal to limits
					if !isreq {
						requests = limits
					}

					limitShares := limits.Value()
					// as of now, kubernetes does not allow overcommit for custom resources
					// it enforces requests must be equal to limits and refuse to schhedule
					// the pod othervise
					// to mitigate this issue and allow overcommit, the cutoff limit
					// can be defined via Pod annotation
					annotationKey := ("gpu-mem-limit." + resourceName).String()

					if annotationValue, ok := pod.ObjectMeta.Annotations[annotationKey]; ok {
						annotationLimit, err := strconv.ParseInt(annotationValue, 10, 64)
						if err != nil {
							logger.Error("Failed to parse memory limit from annotation",
								zap.String(lkNamespace, pod.GetNamespace()),
								zap.String(lkPodName, pod.GetName()),
								zap.String(lkContainerName, container.Name),
								zap.String(lkResourceName, strResourceName),
								zap.String(lkAnnotationKey, annotationKey),
								zap.Error(err))
						} else {
							limitShares = annotationLimit
							if l := logger.Check(zap.DebugLevel, "Using memory enforcement limit from annotation"); l != nil {
								l.Write(
									zap.String(lkNamespace, pod.GetNamespace()),
									zap.String(lkPodName, pod.GetName()),
									zap.String(lkContainerName, container.Name),
									zap.String(lkResourceName, strResourceName),
									zap.String(lkAnnotationKey, annotationKey),
									zap.Int64("limit_shares", limitShares),
									zap.Int64("request_shares", requests.Value()),
								)
							}
						}
					}

					// Get information about assigned MetaGPU devices.
					deviceIDs, err := podmon.GetDeviceIDs(
						pod.GetNamespace(), pod.GetName(), container.Name, resourceName)
					if err != nil {
						logger.Error("Can not GetDeviceIDs",
							zap.String(lkNamespace, pod.GetNamespace()),
							zap.String(lkPodName, pod.GetName()),
							zap.String(lkContainerName, container.Name),
							zap.String(lkResourceName, string(resourceName)),
							zap.Error(err))

						return
					}

					if m.hostNodeName == "" || m.hostNodeName == pod.Spec.NodeName {

						m.gpuContainersCollector[strResourceName] = append(
							m.gpuContainersCollector[strResourceName],
							NewGpuContainer(
								logger,
								getContainerID(logger, pod, container.Name),
								container.Name,
								pod.Name,
								pod.Namespace,
								strResourceName,
								pod.Spec.NodeName,
								requests.Value(),
								limitShares,
								deviceIDs,
								m.gpuUUIDDevices,
							),
						)
					}
				}
			}
		}
	}
}

func (m *GpuMgr) GetDeviceInfo() GpuDeviceInfo {
	hostname, err := os.Hostname()
	if err != nil {
		m.logger.Named("GetDeviceInfo").Error("failed to detect hostname", zap.Error(err))
	}

	logger := m.logger.Named("GetDeviceInfo")

	info := make(map[string]string)
	cudaVersion := nvmlutils.SystemGetCudaDriverVersion(logger)
	info["cudaVersion"] = strconv.Itoa(cudaVersion)
	driver := nvmlutils.SystemGetDriverVersion(logger)
	info["driverVersion"] = driver

	// Copy devices.
	m.mtx.RLock()
	devices := make([]*GpuDevice, 0, len(m.gpuUUIDDevices))

	for _, d := range m.gpuUUIDDevices {
		devices = append(devices, d.GetCopy())
	}
	m.mtx.RUnlock()

	return GpuDeviceInfo{Node: hostname, Metadata: info, Devices: devices}
}

func (m *GpuMgr) GetProcesses(podName, podNamespace string) map[string][]*GpuContainer {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	// if podId is set, return single process
	if podName != "" && podNamespace != "" {
		gpuContainers := make(map[string][]*GpuContainer)

		for resourceName, containers := range m.gpuContainers {
			for _, container := range containers {
				if container.podNamespace == podNamespace && container.podID == podName {
					gpuContainers[resourceName] = append(gpuContainers[resourceName], container.GetCopy())
				}
			}
		}

		return gpuContainers
	}

	// return all processes.
	gpuContainers := make(map[string][]*GpuContainer)

	for resourceName, containers := range m.gpuContainers {
		for _, container := range containers {
			gpuContainers[resourceName] = append(gpuContainers[resourceName], container.GetCopy())
		}
	}

	return gpuContainers
}

func (m *GpuMgr) KillGpuProcess(pid uint32) error {
	logger := m.logger.Named("KillGpuProcess")

	p := NewGpuProcess(logger, pid, 0, 0, "", "", 0, 0, 0)

	return p.Kill()
}

func (m *GpuMgr) SetDeviceLevelVisibilityToken(token string) {
	m.mtx.Lock()
	m.deviceLevelVisibilityToken = token
	m.mtx.Unlock()
}

func (m *GpuMgr) SetContainerLevelVisibilityToken(token string) {
	m.mtx.Lock()
	m.containerLevelVisibilityToken = token
	m.mtx.Unlock()
}

// NewGpuManager creates a new GPU manager.
// nodeName - a Kubernetes worker hostname as it is registered in Kube API to filter Pods.
// enforceMem - if true, the manager will kill processes which use more GPU memory than allowed.
func NewGpuManager(logger *zap.Logger, enforceMem bool, nodeName string, shareConf *sharecfg.DevicesSharingConfigs) *GpuMgr {
	mgr := &GpuMgr{
		logger:          logger.Named("GpuManager"),
		gpuShareConfigs: shareConf,
		hostNodeName:    nodeName,
	}

	// init gpu devices
	mgr.setGpuDevices()
	// init gpu containers
	mgr.discoverGpuContainers()
	// init active gpu processes
	mgr.enrichGpuContainer()
	// set gpu processes
	mgr.setGpuContainers()
	// start gpu devices and processes cache
	mgr.startGpuStatusCache()
	// set per physical GPU allocation statistics.
	mgr.updateAllocatedShares()
	// start mem enforcer
	if enforceMem {
		mgr.StartMemoryEnforcer()
	}

	return mgr
}
