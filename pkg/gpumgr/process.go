package gpumgr

import (
	"errors"
	"path/filepath"
	"regexp"

	"github.com/prometheus/procfs"
	"github.com/shirou/gopsutil/v3/process"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// ErrParseProcessContainerID - can not parse/extract container ID for a process' PID
// from procfs/cgroups.
var ErrParseProcessContainerID = errors.New("can not parse a process container ID")

type GpuProcess struct {
	// A bit strange/illogical order of
	// structure members is to reduce RAM usage
	// via decreasing padding.

	logger *zap.Logger

	// deviceUUID - Parent device UUID if MIG device, usual device UUID if not MIG mode.
	deviceUUID string
	// metaGpuUUID - MIG device UUID, if MIG mode, otherwise a normal device UUID.
	metaGpuUUID string

	user        string
	containerID string

	cmdline []string

	migInstanceID int

	gpuMemBytes uint64

	pid uint32

	gpuUtilization    uint32
	gpuInstanceID     int32
	computeInstanceID int32
}

func (p *GpuProcess) GetPid() uint32            { return p.pid }
func (p *GpuProcess) GetDeviceUUID() string     { return p.deviceUUID }
func (p *GpuProcess) GetMetaGpuUUID() string    { return p.metaGpuUUID }
func (p *GpuProcess) GetGPUUtilization() uint32 { return p.gpuUtilization }
func (p *GpuProcess) GetGPUMemory() uint64      { return p.gpuMemBytes }
func (p *GpuProcess) GetCMDLine() []string {
	res := make([]string, len(p.cmdline))
	copy(res, p.cmdline)

	return res
}
func (p *GpuProcess) GetUser() string             { return p.user }
func (p *GpuProcess) GetContainerId() string      { return p.containerID }
func (p *GpuProcess) GetGPUInstanceId() int32     { return p.gpuInstanceID }
func (p *GpuProcess) GetComputeInstanceId() int32 { return p.computeInstanceID }
func (p *GpuProcess) GetMigInstanceId() int       { return p.migInstanceID }
func (p *GpuProcess) GetCopy() *GpuProcess {
	cmdline := make([]string, len(p.cmdline))
	copy(cmdline, p.cmdline)

	return &GpuProcess{
		pid:               p.pid,
		deviceUUID:        p.deviceUUID,
		metaGpuUUID:       p.metaGpuUUID,
		gpuUtilization:    p.gpuUtilization,
		gpuMemBytes:       p.gpuMemBytes,
		cmdline:           cmdline,
		user:              p.user,
		containerID:       p.containerID,
		gpuInstanceID:     p.gpuInstanceID,
		computeInstanceID: p.computeInstanceID,
		migInstanceID:     p.migInstanceID,
		logger:            p.logger,
	}
}

// MarshalLogObject implements zap.ObjectLogMarshaler.
func (p *GpuProcess) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint32("process_pid", p.pid)
	zap.Strings("process_cmdline", p.cmdline).AddTo(enc)
	enc.AddString("process_container_id", p.containerID)
	enc.AddString("process_user", p.user)
	enc.AddInt32("process_compute_instance_id", p.computeInstanceID)
	enc.AddInt32("process_gpu_instance_id", p.gpuInstanceID)
	enc.AddString("process_gpu_device_uuid", p.deviceUUID)
	enc.AddString("process_metagpu_uuid", p.metaGpuUUID)
	enc.AddInt("process_gpu_mig_instance_id", p.migInstanceID)
	enc.AddUint64("process_gpu_memory_bytes", p.gpuMemBytes)
	enc.AddUint32("process_gpu_utilization", p.gpuUtilization)

	return nil
}

//nolint:lll // contains examples of /proc/<pid>/cgroup.
/*
This regex is supposed to extract container ID as $2 from any of:
  /user.slice/user-1000.slice/user@1000.service/user.slice/libpod-3d9c4721c4f6b1ca3f0c622427eafc6e4d8d88aaf5f4a77f94b92b6ece46dfda.scope/container
  /system.slice/docker-80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57.scope
  /machine.slice/libpod-2025b1a51ba5c3c176cd95a0247a1c61cc12e89bfa7c2ce6c49acb6e76282a8a.scope/container
  /kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
  /kubepods.slice/kubepods-burstable.slice/kubepods-burstable-pod6a4db9b7_1945_455f_b95f_43aebf2ebc44.slice/crio-049b7a27ec2c038f584d7447db282f62abc534ed40ccded136f2287919dad827.scope
  /kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod67d2d30a_4500_4484_b237_c5fdf8672f26.slice/cri-containerd-4b7554909dc7d4ba8c50e6c4e661001dfc7f68334b7ae9d30ca0cdad56d3c64b.scope
Kubelet (kubelet/stats/cri_stats_provider.go) is doing: "trim anything before the final '-' and suffix .scope".
*/
var scopeContainerCgroupRe = regexp.MustCompile(`([^/]+)-([^-]+)\.scope`)

// See regex above.
const (
	scopeContainerCgroupReIdxContainerID = 2
	scopeContainerCgroupReIdxCgroup      = 0

	// scopeContainerCgroupReMatchCount number of matches in go regex
	// to check if the match is successful.
	scopeContainerCgroupReMatchCount = 3
)

func (p *GpuProcess) SetProcessCmdline() {
	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		var e error

		p.cmdline, e = pr.CmdlineSlice()

		if e != nil {
			p.logger.Named("SetProcessCmdline").Error("can not get process cmdline", zap.Error(err))
		} else {
			p.logger.Named("SetProcessCmdline").Debug("Process cmdline is set")
			p.logger = p.logger.With(zap.Strings("process_cmdline", p.cmdline))
		}
	} else {
		p.logger.Named("SetProcessCmdline").Error("can not get process cmdline", zap.Error(err))
	}
}

func (p *GpuProcess) SetProcessUsername() {
	logger := p.logger.Named("SetProcessUsername")

	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		var e error

		p.user, e = pr.Username()

		if e != nil {
			logger.Debug("can not get process username", zap.Error(e))
		} else {
			logger.Debug("process username is set")
			p.logger = p.logger.With(zap.String("process_username", p.user))
		}
	} else {
		logger.Debug("can not get process username", zap.Error(err))
	}
}

func (p *GpuProcess) Kill() error {
	p.logger.Named("Kill").Debug("Killing the process")

	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		return pr.Kill()
	} else {
		return err
	}
}

func (p *GpuProcess) SetProcessContainerId() {
	logger := p.logger.Named("SetProcessContainerId")

	if p.containerID != "" {
		logger.Info("Process already has its container id set, skipping")

		return
	}

	if proc, err := procfs.NewProc(int(p.pid)); err == nil {
		var (
			e       error
			cgroups []procfs.Cgroup
		)

		cgroups, e = proc.Cgroups()
		if e != nil {
			logger.Error("can not get process cgroups", zap.Error(err))
		}

		if len(cgroups) == 0 {
			logger.Error("cgroups list is empty")
		} else {
			if l := logger.Check(zap.DebugLevel, "process cgroups"); l != nil {
				l.Write(zap.Any("cgroups", cgroups))
			}
		}

		procContainerID, cgroupPath, err := parseProcessContainerID(logger, cgroups)
		if err != nil {
			logger.Error("Can not match process to container", zap.Error(err))

			return
		}

		p.containerID = procContainerID
		p.logger = p.logger.With(
			zap.String("process_container_id", p.containerID),
			zap.String("process_cgroup_path", cgroupPath),
		)
	}
}

// parseProcessContainerID tries to extract container id and cgroup "name"
// from gives cgroups for a process (PID).
func parseProcessContainerID(log *zap.Logger, cgroups []procfs.Cgroup) (procContainerID, processCGroupPath string, err error) {
	logger := log.Named("matchProcessContainerID")

	for cgroupIdx := range cgroups {
		cgroup := &cgroups[cgroupIdx]

		iLog := logger.With(zap.String("process_cgroup_path", cgroup.Path))

		if len(cgroup.Controllers) == 0 {
			// cgroupv2: see
			// https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html#organizing-processes-and-threads
			// contains only one line similar to:
			// "0::/system.slice/docker-80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57.scope"
			// so zero controllers but it is still possible to extract the container ID.
			cID, cgroupPath, parsed := parseOneCgroupPath(iLog, cgroup.Path)
			if parsed {
				return cID, cgroupPath, nil
			}
		} else {
			// cgroupv1: container a list of controllers.
			cID, cgroupPath, parsed := parseProcessContainerIDCgroupV1(iLog, cgroup)
			if parsed {
				return cID, cgroupPath, nil
			}
		}
	}

	logger.Warn("unable to set containerId for pid")

	return "", "", ErrParseProcessContainerID
}

// parseProcessContainerIDCgroupV1 tries to extract container ID from
// cgroupsv1 memory controller.
func parseProcessContainerIDCgroupV1(log *zap.Logger, cgroup *procfs.Cgroup) (procContainerID, processCGroupPath string, found bool) { //nolint:lll // let it be.
	logger := log.Named("parseProcessContainerIDCgroupv1")

	for _, controller := range cgroup.Controllers {
		if controller != "memory" {
			// We are interested only in memory controller.
			// if not, then we don't use this cgroup.
			continue
		}

		return parseOneCgroupPath(logger, cgroup.Path)
	}

	return "", "", false
}

// parseOneCgroupPath tries to extract container ID from a cgroup v1/v2 path, i.e.
// "0::/system.slice/docker-80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57.scope".
func parseOneCgroupPath(log *zap.Logger, path string) (procContainerID, processCGroupName string, parsed bool) {
	logger := log.Named("parseOneCgroupPath")

	// Docker-based runtime container cgroup path is "/something/something-else/docker-<ContainerId>.scope"
	// CRIO runtime container cgroup path is "/something/something-else/crio-<ContainerId>.scope"
	// Podman: "/something/something-else/libpod-<ContainerID>.scope/container"
	// Containerd: "/something/something-else/cri-containerd-<ContainerId>.scope"
	scopeContainerMatch := scopeContainerCgroupRe.FindStringSubmatch(path)

	if len(scopeContainerMatch) == scopeContainerCgroupReMatchCount {
		// Extract cgroup name (<prefix>-<ContainerId>.scope CRI case).
		procContainerID = scopeContainerMatch[scopeContainerCgroupReIdxContainerID]
		processCGroupName = scopeContainerMatch[scopeContainerCgroupReIdxCgroup]

		logger = logger.With(
			zap.String("process_container_id", procContainerID),
			zap.String("process_cgroup_name", processCGroupName))
		logger.Debug("Process cgroup name matching the <runtime>-<ContainerId>.scope pattern, assigning container ID for pid")
	} else {
		// Use full cgroup name (pre-CRI case).
		processCGroupName = filepath.Base(path)
		procContainerID = processCGroupName

		logger = logger.With(
			zap.String("process_container_id", procContainerID),
			zap.String("process_cgroup_name", processCGroupName))
		logger.Debug(
			"Process cgroup does not match the <runtime>-<ContainerId>.scope pattern, assigning the full cgroup name for pid")
	}

	// Found, check in case it is empty
	if procContainerID != "" {
		logger.Debug("Parsed container id and cgroup name")

		return procContainerID, processCGroupName, true
	}

	logger.Debug("Could not parse cgroup path to container ID")

	return "", "", false
}

func (p *GpuProcess) GetShortCmdLine() string {
	if len(p.cmdline) == 0 {
		return "-"
	}
	return p.cmdline[0]
}

func NewGpuProcess(logger *zap.Logger, pid,
	gpuUtil uint32, gpuMemBytes uint64,
	mgpuUUID, devUUID string,
	migInstanceID int, computeInstanceID, gpuInstanceID int32,
) *GpuProcess {
	proc := &GpuProcess{
		pid:               pid,
		gpuUtilization:    gpuUtil,
		gpuMemBytes:       gpuMemBytes,
		metaGpuUUID:       mgpuUUID,
		deviceUUID:        devUUID,
		gpuInstanceID:     gpuInstanceID,
		computeInstanceID: computeInstanceID,
		migInstanceID:     migInstanceID,

		logger: logger.Named("NewGpuProcess"),
	}

	proc.logger.Debug("Creating new Gpu process")

	proc.SetProcessUsername()
	proc.SetProcessCmdline()
	proc.SetProcessContainerId()

	return proc
}
