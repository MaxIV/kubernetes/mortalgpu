package gpumgr

import (
	"bufio"
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"testing"

	"github.com/prometheus/procfs"
	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
)

/*
We need to have correctly parsed cgroups from their files
to test matching but the prometheus/procfs
does not export functions which parse cgroups from procfs so
I have them copied here.

When a better way is found, it should be used.
*/

// Start of code copied from prometheus/procfs.

// parseCgroupString parses each line of the /proc/[pid]/cgroup file
// Line format is hierarchyID:[controller1,controller2]:path.
func parseCgroupString(cgroupStr string) (*procfs.Cgroup, error) {
	var err error

	fields := strings.SplitN(cgroupStr, ":", 3)
	if len(fields) < 3 {
		return nil, fmt.Errorf("%w: 3+ fields required, found %d fields in cgroup string: %s", procfs.ErrFileParse, len(fields), cgroupStr)
	}

	cgroup := &procfs.Cgroup{
		Path:        fields[2],
		Controllers: nil,
	}
	cgroup.HierarchyID, err = strconv.Atoi(fields[0])
	if err != nil {
		return nil, fmt.Errorf("%w: hierarchy ID: %q", procfs.ErrFileParse, cgroup.HierarchyID)
	}
	if fields[1] != "" {
		ssNames := strings.Split(fields[1], ",")
		cgroup.Controllers = append(cgroup.Controllers, ssNames...)
	}
	return cgroup, nil
}

// parseCgroups reads each line of the /proc/[pid]/cgroup file.
func parseCgroups(data []byte) ([]procfs.Cgroup, error) {
	var cgroups []procfs.Cgroup
	scanner := bufio.NewScanner(bytes.NewReader(data))
	for scanner.Scan() {
		mountString := scanner.Text()
		parsedMounts, err := parseCgroupString(mountString)
		if err != nil {
			return nil, err
		}
		cgroups = append(cgroups, *parsedMounts)
	}

	err := scanner.Err()
	return cgroups, err
}

// End of code copied from prometheus/procfs.

// parseCgroupsPanic wrapper over parseCgroup which panics instead of
// returning error. We are not testing Prometheus so it is acceptable.
func parseCgroupsPanic(data string) []procfs.Cgroup {
	cgroups, err := parseCgroups([]byte(data))
	if err != nil {
		panic(fmt.Sprintf("can not parse cgroups: %s", err))
	}

	return cgroups
}

// End of cgroups parsing code from prometheus/procfs

type TestParseProcessContainerID struct {
	suite.Suite
}

func (ts *TestParseProcessContainerID) Test_Docker_CGroupsV1() {
	type testCase struct {
		expectedContainerID string
		expectedCGroupName  string

		cgroups []procfs.Cgroup
	}

	testCases := []*testCase{
		{
			cgroups: parseCgroupsPanic(
				`12:perf_event:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
11:blkio:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
10:cpuset:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
9:devices:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
8:freezer:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
7:pids:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
6:net_cls,net_prio:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
5:rdma:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
4:hugetlb:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
3:memory:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
2:cpu,cpuacct:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
1:name=systemd:/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod7bfa35ab_f3be_419d_ab1f_d8084cdb2ad7.slice/docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope
0::/
`),

			expectedContainerID: "344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7",
			expectedCGroupName:  "docker-344887a1eaef52a54b29e778c0cf3a2aa18274a77167709ce887b966723101c7.scope",
		},
	}

	log := zap.Must(zap.NewDevelopment()).Named("Test_Docker_CGroupsV1")

	for idx, testCase := range testCases {
		logger := log.With(zap.Int("test_case", idx))

		containerID, cGroup, err := parseProcessContainerID(
			logger, testCase.cgroups)

		ts.Require().NoError(err)
		ts.Equal(testCase.expectedContainerID, containerID)
		ts.Equal(testCase.expectedCGroupName, cGroup)
	}
}

func (ts *TestParseProcessContainerID) Test_Docker_CGroupsV2() {
	type testCase struct {
		expectedContainerID string
		expectedCGroupName  string

		cgroups []procfs.Cgroup
	}

	testCases := []*testCase{
		{
			cgroups: parseCgroupsPanic(
				`0::/system.slice/docker-80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57.scope`),

			expectedContainerID: "80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57",
			expectedCGroupName:  "docker-80d77a80ee9e400cb3550569f2c4a6d7599b1bf5c3ba77409ff8bf2a288b1b57.scope",
		},
	}

	log := zap.Must(zap.NewDevelopment()).Named("Test_Docker_CGroupsV1")

	for idx, testCase := range testCases {
		logger := log.With(zap.Int("test_case", idx))

		containerID, cGroup, err := parseProcessContainerID(
			logger, testCase.cgroups)

		ts.Require().NoError(err)
		ts.Equal(testCase.expectedContainerID, containerID)
		ts.Equal(testCase.expectedCGroupName, cGroup)
	}
}

func (ts *TestParseProcessContainerID) Test_Podman_CGroupsV2() {
	type testCase struct {
		expectedContainerID string
		expectedCGroupName  string

		cgroups []procfs.Cgroup
	}

	testCases := []*testCase{
		{
			cgroups: parseCgroupsPanic(
				`0::/user.slice/user-1000.slice/user@1000.service/user.slice/libpod-3d9c4721c4f6b1ca3f0c622427eafc6e4d8d88aaf5f4a77f94b92b6ece46dfda.scope/container`), //nolint:lll // cgroup path.

			expectedContainerID: "3d9c4721c4f6b1ca3f0c622427eafc6e4d8d88aaf5f4a77f94b92b6ece46dfda",
			expectedCGroupName:  "libpod-3d9c4721c4f6b1ca3f0c622427eafc6e4d8d88aaf5f4a77f94b92b6ece46dfda.scope",
		},
	}

	log := zap.Must(zap.NewDevelopment()).Named("Test_Docker_CGroupsV1")

	for idx, testCase := range testCases {
		logger := log.With(zap.Int("test_case", idx))

		containerID, cGroup, err := parseProcessContainerID(
			logger, testCase.cgroups)

		ts.Require().NoError(err)
		ts.Equal(testCase.expectedContainerID, containerID)
		ts.Equal(testCase.expectedCGroupName, cGroup)
	}
}

func (ts *TestParseProcessContainerID) Test_Containerd_CGroupsV2() {
	type testCase struct {
		expectedContainerID string
		expectedCGroupName  string

		cgroups []procfs.Cgroup
	}

	testCases := []*testCase{
		{
			cgroups: parseCgroupsPanic(
				`0::/kubepods/burstable/pode42c2f20-6171-46d5-b2ea-4edac1ec6bcc/23f1929ed97e3ab7dffcb21d0a13e8e42757521131b97dfc296bcfb93ff36495`), //nolint:lll // cgroup path.

			expectedContainerID: "23f1929ed97e3ab7dffcb21d0a13e8e42757521131b97dfc296bcfb93ff36495",
			expectedCGroupName:  "23f1929ed97e3ab7dffcb21d0a13e8e42757521131b97dfc296bcfb93ff36495",
		},
		{
			cgroups: parseCgroupsPanic(
				`0::/kubepods.slice/kubepods-besteffort.slice/kubepods-besteffort-pod67d2d30a_4500_4484_b237_c5fdf8672f26.slice/cri-containerd-4b7554909dc7d4ba8c50e6c4e661001dfc7f68334b7ae9d30ca0cdad56d3c64b.scope
`), //nolint:lll // cgroup path.

			expectedContainerID: "4b7554909dc7d4ba8c50e6c4e661001dfc7f68334b7ae9d30ca0cdad56d3c64b",
			expectedCGroupName:  "cri-containerd-4b7554909dc7d4ba8c50e6c4e661001dfc7f68334b7ae9d30ca0cdad56d3c64b.scope",
		},
	}

	log := zap.Must(zap.NewDevelopment()).Named("Test_Docker_CGroupsV1")

	for idx, testCase := range testCases {
		logger := log.With(zap.Int("test_case", idx))

		containerID, cGroup, err := parseProcessContainerID(
			logger, testCase.cgroups)

		ts.Require().NoError(err)
		ts.Equal(testCase.expectedContainerID, containerID)
		ts.Equal(testCase.expectedCGroupName, cGroup)
	}
}

func (ts *TestParseProcessContainerID) Test_CRIO_CGroupsV2() {
	type testCase struct {
		expectedContainerID string
		expectedCGroupName  string

		cgroups []procfs.Cgroup
	}

	testCases := []*testCase{
		{
			cgroups: parseCgroupsPanic(
				`0::/kubepods.slice/kubepods-burstable.slice/kubepods-burstable-pod6a4db9b7_1945_455f_b95f_43aebf2ebc44.slice/crio-049b7a27ec2c038f584d7447db282f62abc534ed40ccded136f2287919dad827.scope`), //nolint:lll // cgroup path.

			expectedContainerID: "049b7a27ec2c038f584d7447db282f62abc534ed40ccded136f2287919dad827",
			expectedCGroupName:  "crio-049b7a27ec2c038f584d7447db282f62abc534ed40ccded136f2287919dad827.scope",
		},
	}

	log := zap.Must(zap.NewDevelopment()).Named("Test_Docker_CGroupsV1")

	for idx, testCase := range testCases {
		logger := log.With(zap.Int("test_case", idx))

		containerID, cGroup, err := parseProcessContainerID(
			logger, testCase.cgroups)

		ts.Require().NoError(err)
		ts.Equal(testCase.expectedContainerID, containerID)
		ts.Equal(testCase.expectedCGroupName, cGroup)
	}
}

func TestRunParseProcessContainerID(t *testing.T) {
	t.Parallel()

	suite.Run(t, &TestParseProcessContainerID{})
}
