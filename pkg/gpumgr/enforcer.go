package gpumgr

import (
	"errors"
	"sort"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// MemoryEnforcerLoopIntervalSec interval between memory enforcements
// for processes using MortalGPUs.
const MemoryEnforcerLoopIntervalSec = 5

// ErrBugNoProcessShareSize returned if the enforcer could not extract information about a GPU
// from any of processes of a GPU container. It is an indication of a potential bug.
var ErrBugNoProcessShareSize = errors.New(
	"can not find a GPU process in a container to extract " +
		"GPU ShareSize and compute maximum allowed GPU memory")

// StartMemoryEnforcer starts a loop which checks all processes using MortalGPUs
// on a node and kills processes if total memory usage of the processes of a container
// is larger than the limits.
func (m *GpuMgr) StartMemoryEnforcer() {
	logger := m.logger.Named("StartMemoryEnforcer")

	logger.Info("starting gpu memory enforcer")

	go func() {
		for {
			m.mtx.Lock() // Enforce can sort GPU processes, so it needs RW Lock.
			procsToKill := m.enforce(logger)
			m.mtx.Unlock()

			for _, p := range procsToKill {
				if err := p.Kill(); err != nil {
					// A bit copying of the logger name but let it be so.
					p.logger.Named("MemoryEnforcer").Error("Can not kill process", zap.Error(err))
				}
			}

			time.Sleep(MemoryEnforcerLoopIntervalSec * time.Second)
		}
	}()
}

func (m *GpuMgr) enforce(zlogger *zap.Logger) (gpuProcForKill []*GpuProcess) {
	const logKeyGPUResourceName = "gpu_resource_name"

	logger := zlogger.Named("enforce")

	logger.Debug("Starting enforce loop over GPU containers")

	for resourceName, containers := range m.gpuContainers {
		rLogger := logger.With(zap.String(logKeyGPUResourceName, resourceName))

		if len(containers) == 0 {
			rLogger.Warn("Unexpected: GPU resource does not have GPU containers, possible bug")

			continue
		}

		for _, container := range containers {
			processesToKill, err := m.enforceGPUContainer(rLogger, container)
			if err != nil {
				rLogger.Error("Can not enforce GPU container", zap.Error(err))

				continue
			}

			gpuProcForKill = append(gpuProcForKill, processesToKill...)
		}
	}

	if logger.Core().Enabled(zap.DebugLevel) {
		logger.With(zap.Objects("processes_to_kill", gpuProcForKill)).Debug("Processes to kill")
	}

	return gpuProcForKill
}

type oomVictim struct {
	pid     uint32
	usedMem uint64
}

// MarshalLogObject - serializes oomVictim for Zap logger.
func (vict oomVictim) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint32("pid", vict.pid)
	enc.AddUint64("used_memory", vict.usedMem)

	return nil
}

func (m *GpuMgr) enforceGPUContainer(zlogger *zap.Logger, container *GpuContainer) ([]*GpuProcess, error) {
	const (
		logKeyGPUMemoryBytesCurrent = "gpu_memory_bytes_current"
		logKeyGPUMemoryBytesMax     = "gpu_memory_bytes_max"
		logKeyFullProcessesInfo     = "processes_info"
		logKeyProcessesTotalCount   = "processes_total_count"
		logKeyOOMVictims            = "processes_to_kill"
		logKeyProcessesKillCount    = "processes_to_kill_count"
		logKeyExpectedMemToFree     = "gpu_memory_bytes_to_free"
	)

	cLogger := zlogger.Named("enforceGPUContainer")

	if len(container.processes) == 0 {
		cLogger.Debug("Container does not have any active GPU processes, no enforcement needed")

		return nil, nil
	}

	// Annotate logger with processes list.
	if cLogger.Core().Enabled(zap.DebugLevel) {
		cLogger = cLogger.With(zap.Objects(logKeyFullProcessesInfo, container.processes))
		cLogger.Debug("Processing GPU process")
	}

	var (
		maxAllowedMem uint64
		usedGPUMemory uint64
		memComputed   bool
	)

	for _, process := range container.processes {
		// Compute total used memory.
		usedGPUMemory += process.gpuMemBytes

		// Assuming that all the GPU processes of the same GPU have the same
		// "ShareSize", find and compute Max Allowed Memory for all
		// the processes of the GPU in the container.
		if !memComputed {
			if gpuDevice := m.getGpuDeviceByUUID(process.GetMetaGpuUUID()); gpuDevice != nil {
				maxAllowedMem = gpuDevice.GetMemory().ShareSize * uint64(container.podMetagpuLimit)
				memComputed = true
			}
		}
	}

	if !memComputed {
		cLogger.Error("no GPU ShareSize", zap.Error(ErrBugNoProcessShareSize))

		return nil, ErrBugNoProcessShareSize
	}

	// Find candidates to kill, if needed.
	if usedGPUMemory > maxAllowedMem {
		oomVictims, logVictims, memToFree := findOOMVictims(usedGPUMemory-maxAllowedMem, container.processes)

		cLogger.With(
			zap.Uint64(logKeyGPUMemoryBytesCurrent, usedGPUMemory),
			zap.Uint64(logKeyGPUMemoryBytesMax, maxAllowedMem),
			zap.Uint64(logKeyExpectedMemToFree, memToFree),
			zap.Int(logKeyProcessesKillCount, len(oomVictims)),
			zap.Objects[oomVictim](logKeyOOMVictims, logVictims),
			zap.Int(logKeyProcessesTotalCount, len(container.processes)),
		).Info("Out of memory: Container is using more GPU than allowed, killing processes")

		return oomVictims, nil
	}

	cLogger.With(
		zap.Uint64(logKeyGPUMemoryBytesCurrent, usedGPUMemory),
		zap.Uint64(logKeyGPUMemoryBytesMax, maxAllowedMem),
	).Debug("Pod memory use is under the limit, no processes will be killed")

	return nil, nil
}

func findOOMVictims(memToFree uint64, processes []*GpuProcess) ([]*GpuProcess, []oomVictim, uint64) {
	// Naive sort and select might not be optimal but will work
	// as I don't expect even hundreds processes.
	sort.Slice(processes, func(i, j int) bool {
		// The comparison is by design sorts items so that
		// lower indices have higher used memory values.
		return processes[i].gpuMemBytes > processes[j].gpuMemBytes
	})

	processesToKill := make([]*GpuProcess, 0)
	logVictims := make([]oomVictim, 0)
	freedMem := uint64(0)

	for _, proc := range processes {
		processesToKill = append(processesToKill, proc.GetCopy())
		logVictims = append(logVictims, oomVictim{
			pid:     proc.pid,
			usedMem: proc.gpuMemBytes,
		})

		freedMem += proc.gpuMemBytes

		if freedMem > memToFree {
			break
		}
	}

	return processesToKill, logVictims, freedMem
}

func (m *GpuMgr) getGpuDeviceByUUID(metaGpuUUID string) *GpuDevice {
	device, ok := m.gpuUUIDDevices[metaGpuUUID]
	if ok {
		return device
	}

	return nil
}
