package mgsrv

import (
	"context"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv/types"
)

type MetaGpuServerStream struct {
	grpc.ServerStream
	ctx context.Context
}

func (s *MetaGpuServerStream) Context() context.Context {
	return s.ctx
}

func (s *MetaGpuServer) streamServerInterceptor() grpc.StreamServerInterceptor {
	logger := s.logger.Named("streamServerInterceptor")

	return func(srv any, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		wrapper := &MetaGpuServerStream{ServerStream: ss}

		if !s.IsMethodPublic(info.FullMethod) {
			claims, err := authorize(ss.Context(), logger, s.authClient, s.privilegedSAs)
			if err != nil {
				return err
			}

			reqLogger := logger.With(
				zap.String("ServiceAccount_name", claims.KubernetesIo.Serviceaccount.Name),
				zap.String("ServiceAccount_Namespace", claims.KubernetesIo.Namespace),
				zap.String("Pod", claims.KubernetesIo.Pod.Name))

			wrapper.ctx = context.WithValue(ss.Context(), types.ContextClaimName, claims)
			wrapper.ctx = context.WithValue(wrapper.ctx, types.ContextGPUMGRName, s.gpuMgr)
			wrapper.ctx = context.WithValue(wrapper.ctx, types.ContextLoggerName, reqLogger)
		}

		return handler(srv, wrapper)
	}
}

func (s *MetaGpuServer) unaryServerInterceptor() grpc.UnaryServerInterceptor {
	logger := s.logger.Named("unaryServerInterceptor")

	return func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
		start := time.Now()

		if !s.IsMethodPublic(info.FullMethod) {
			claims, err := authorize(ctx, logger, s.authClient, s.privilegedSAs)
			if err != nil {
				logger.Error("Authenticate and authorize error", zap.Error(err))

				return nil, err
			}

			reqLogger := logger.With(
				zap.String("ServiceAccount_name", claims.KubernetesIo.Serviceaccount.Name),
				zap.String("ServiceAccount_Namespace", claims.KubernetesIo.Namespace),
				zap.String("Pod", claims.KubernetesIo.Pod.Name))

			ctx = context.WithValue(ctx, types.ContextClaimName, claims)
			ctx = context.WithValue(ctx, types.ContextLoggerName, reqLogger)
		} else {
			ctx = context.WithValue(ctx, types.ContextLoggerName, logger)
		}

		ctx = context.WithValue(ctx, types.ContextGPUMGRName, s.gpuMgr)

		if l := logger.Check(zap.DebugLevel, "Prepared Context"); l != nil {
			l.Write(zap.Any("context", ctx))
		}

		h, err := handler(ctx, req)

		logger.Debug("Method performance", zap.String("method", info.FullMethod), zap.Duration("duration", time.Since(start)))

		return h, err
	}
}
