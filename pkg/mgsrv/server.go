package mgsrv

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"net"
	"regexp"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	authv1 "k8s.io/api/authentication/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	reviewv1 "k8s.io/client-go/kubernetes/typed/authentication/v1"

	devicevpb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/gpumgr"
	devicevapi "gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv/deviceapi/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// tokenCutRE Regular expression to remove the prefix of the Token Header value
// which should have format: "Authorization: Bearer TOKENTEXT".
var tokenCutRE = regexp.MustCompile(`Bearer\s+(\S+)`)

type PrivilegedServiceAccount struct {
	Name      string `json:"name,omitempty"      yaml:"name"`
	Namespace string `json:"namespace,omitempty" yaml:"namespace"`
}

func (ev *PrivilegedServiceAccount) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("name", ev.Name)
	enc.AddString("namespace", ev.Namespace)

	return nil
}

type MetaGpuServer struct {
	gpuMgr *gpumgr.GpuMgr

	serverAddr string

	authClient reviewv1.TokenReviewInterface

	logger *zap.Logger

	privilegedSAs []*PrivilegedServiceAccount
}

// NewMetaGpuServer - creates a new server instance.
// nodeName - hostname of a worker on which this instance is running as it is in Kube API.
// enforceKill - if set, enables GPU OOMKill.
func NewMetaGpuServer(_logger *zap.Logger,
	nodeName string,
	enforceKill bool,
	serverAddr string,
	authClient reviewv1.TokenReviewInterface,
	privilegedSAs []*PrivilegedServiceAccount,
	shareConf *sharecfg.DevicesSharingConfigs,
) *MetaGpuServer {
	logger := _logger.Named("MetaGpuServer")

	return &MetaGpuServer{
		gpuMgr:        gpumgr.NewGpuManager(logger, enforceKill, nodeName, shareConf),
		serverAddr:    serverAddr,
		logger:        logger,
		authClient:    authClient,
		privilegedSAs: privilegedSAs,
	}
}

func (s *MetaGpuServer) Start() {
	logger := s.logger.Named("Start")

	go func() {
		lis, err := net.Listen("tcp", s.serverAddr)
		if err != nil {
			logger.Fatal("failed to listen", zap.Error(err))
		}

		logger.Info("metagpu gRPC management server listening", zap.String("address", s.serverAddr))

		opts := []grpc.ServerOption{
			grpc.UnaryInterceptor(s.unaryServerInterceptor()),
			grpc.StreamInterceptor(s.streamServerInterceptor()),
		}

		grpcServer := grpc.NewServer(opts...)

		dp := devicevapi.DeviceService{}
		devicevpb.RegisterDeviceServiceServer(grpcServer, &dp)
		reflection.Register(grpcServer)

		if err := grpcServer.Serve(lis); err != nil {
			logger.Fatal("gRPC server can not start", zap.Error(err))
		}
	}()
}

func (*MetaGpuServer) IsMethodPublic(fullMethod string) bool {
	publicMethods := []string{
		"/device.v1.DeviceService/PingServer",
	}
	for _, method := range publicMethods {
		if method == fullMethod {
			return true
		}
	}

	return false
}

func authorize(ctx context.Context, log *zap.Logger,
	authClient reviewv1.TokenReviewInterface,
	privilegedSAs []*PrivilegedServiceAccount,
) (*types.TokenClaims, error) {
	logger := log.Named("authorize")

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		logger.Error("Can not get incoming request metadata")

		return nil, status.Error(codes.InvalidArgument, "retrieving metadata is failed")
	}

	if l := logger.Check(zap.DebugLevel, "Request metadata"); l != nil {
		l.Write(zap.Any("metadata", md))
	}

	authHeader := md.Get("Authorization")
	if authHeader == nil {
		logger.Error("Authorization token is not supplied in request metadata")

		return nil, status.Error(codes.Unauthenticated, "authorization token is not supplied")
	}

	tokenMatch := tokenCutRE.FindStringSubmatch(authHeader[0])

	if len(tokenMatch) == 0 {
		logger.Error("Header Authorization has incorrect value",
			zap.String("raw_token_value", authHeader[0]))

		return nil, status.Error(codes.Unauthenticated, "Authorization token has wrong format. Must be Authorization: Bearer XXX")
	}

	tokenString := tokenMatch[1]

	reviewReq := authv1.TokenReview{
		Spec: authv1.TokenReviewSpec{
			Token: tokenString,
		},
	}

	reviewResp, err := authClient.Create(ctx, &reviewReq, metav1.CreateOptions{})
	if err != nil {
		logger.Error("Creating TokenReview error", zap.Error(err))

		return nil, status.Error(codes.Unauthenticated, "Bad token")
	}

	if !reviewResp.Status.Authenticated {
		logger.Error("TokenReview returned Status.Authenticated false")

		return nil, status.Error(codes.Unauthenticated, "Bad token")
	}

	// Token is 3 parts separated by "." and encoded in Base 64.
	// The middle part contains claims.

	reviewedToken, err := base64.RawStdEncoding.DecodeString(strings.Split(reviewResp.Spec.Token, ".")[1])
	if err != nil {
		logger.Error("Token decode error", zap.Error(err))

		return nil, status.Error(codes.Unauthenticated, "Bad token: not a correct Base64 encoding")
	}

	claims := &types.TokenClaims{}

	if err := json.Unmarshal(reviewedToken, claims); err != nil {
		logger.Error("Token JSON Unmarshal error", zap.Error(err))

		return nil, status.Error(codes.Unauthenticated, "Bad token: can nod decode claims")
	}

	// If the claims identify the Privileged Service Account (MortalGPU itself, likely),
	// then mark it.
	claims.Visibility = types.ContainerVisibility

	for _, pSA := range privilegedSAs {
		if claims.KubernetesIo.Namespace == pSA.Namespace &&
			claims.KubernetesIo.Serviceaccount.Name == pSA.Name {

			claims.Visibility = types.DeviceVisibility

			break
		}
	}

	if l := logger.Check(zap.DebugLevel, "Parsed Token Claims"); l != nil {
		l.Write(zap.Any("claims", claims), zap.Stringer("visibility_level_name", claims.Visibility))
	}

	return claims, nil
}
