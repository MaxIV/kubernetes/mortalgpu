package v1

import (
	"context"
	"errors"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/gpumgr"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv/types"
)

var ErrBadContext = errors.New("bad request Context")

type DeviceServiceContext struct {
	claims *types.TokenClaims
	logger *zap.Logger
	mgr    *gpumgr.GpuMgr
}

type DeviceService struct {
	pb.UnimplementedDeviceServiceServer
}

func (*DeviceService) LoadContext(ctx context.Context) (*DeviceServiceContext, error) {
	ret := DeviceServiceContext{}

	logger, ok := ctx.Value(types.ContextLoggerName).(*zap.Logger)
	if !ok {
		zap.L().Error("Zap logger is not set in context", zap.Error(ErrBadContext))
	}

	ret.logger = logger.Named("DeviceService")

	ret.mgr, ok = ctx.Value(types.ContextGPUMGRName).(*gpumgr.GpuMgr)
	if !ok {
		logger.Error("gpuMgr instance not set in context", zap.Error(ErrBadContext))

		return nil, ErrBadContext
	}

	ret.claims, ok = ctx.Value(types.ContextClaimName).(*types.TokenClaims)
	if !ok {
		logger.Error("Token Claims are not set in Context", zap.Error(ErrBadContext))

		return nil, ErrBadContext
	}

	if l := ret.logger.Check(zap.DebugLevel, "Loaded Context"); l != nil {
		l.Write(zap.Any("context", ret))
	}

	return &ret, nil
}

func (s *DeviceService) GetGpuContainers(ctx context.Context, req *pb.GetGpuContainersRequest) (*pb.GetGpuContainersResponse, error) {
	devCtx, err := s.LoadContext(ctx)
	if err != nil {
		return &pb.GetGpuContainersResponse{}, err
	}

	response := &pb.GetGpuContainersResponse{VisibilityLevel: uint32(devCtx.claims.Visibility)}

	// 4 cases are possible:
	// 1: privileged request and Pod Name and Pod Namespace are not set => All Containers.
	// 2: privileged request and Pod Name and Pod Namespace are set => The specified Container.
	// 3: non-privileged request and Pod Name and Pod Namespace are not set => The requesting Container.
	// 4: non-privileged request and Pod Name and Pod Namespace are set => The requesting Container if they match or forbidden.
	var (
		reqPodName      = req.GetPodName()
		reqPodNamespace = req.GetPodNamespace()

		ctxPodName      = devCtx.claims.KubernetesIo.Pod.Name
		ctxPodNamespace = devCtx.claims.KubernetesIo.Namespace
	)

	if devCtx.claims.Visibility == types.DeviceVisibility {
		// Privileged cases 1 and 2.
		response.GpuContainers = listDeviceProcesses(reqPodName, reqPodNamespace, devCtx.mgr)
	} else {
		// Non-privileged case 3.
		if req.GetPodNamespace() == "" && req.GetPodName() == "" {
			response.GpuContainers = listDeviceProcesses(ctxPodName, ctxPodNamespace, devCtx.mgr)
		} else if reqPodName == ctxPodName && reqPodNamespace == ctxPodNamespace {
			// Non-privileged case 4 with match.
			response.GpuContainers = listDeviceProcesses(ctxPodName, ctxPodNamespace, devCtx.mgr)
		} else {
			return response, status.Errorf(codes.PermissionDenied,
				"missing pod Name, Namespace and visibility level is to low (%d), can't proceed",
				devCtx.claims.Visibility)
		}
	}

	response.GpuContainers = listDeviceProcesses(req.GetPodName(), req.GetPodNamespace(), devCtx.mgr)

	return response, nil
}

func (s *DeviceService) GetDevices(ctx context.Context, req *pb.GetDevicesRequest) (*pb.GetDevicesResponse, error) {
	devCtx, err := s.LoadContext(ctx)
	if err != nil {
		return &pb.GetDevicesResponse{}, err
	}

	response := &pb.GetDevicesResponse{}

	// 4 cases are possible:
	// 1: privileged request and Pod Name and Pod Namespace are not set => All Containers.
	// 2: privileged request and Pod Name and Pod Namespace are set => The specified Container.
	// 3: non-privileged request and Pod Name and Pod Namespace are not set => The requesting Container.
	// 4: non-privileged request and Pod Name and Pod Namespace are set => The requesting Container if they match or forbidden.
	var (
		reqPodName      = req.GetPodName()
		reqPodNamespace = req.GetPodNamespace()

		ctxPodName      = devCtx.claims.KubernetesIo.Pod.Name
		ctxPodNamespace = devCtx.claims.KubernetesIo.Namespace
	)

	if devCtx.claims.Visibility == types.DeviceVisibility {
		// Privileged cases 1 and 2.
		response.Device = listPodDevices(reqPodName, reqPodNamespace, devCtx.mgr)
	} else {
		// Non-privileged case 3.
		if req.GetPodNamespace() == "" && req.GetPodName() == "" {
			response.Device = listPodDevices(ctxPodName, ctxPodNamespace, devCtx.mgr)
		} else if reqPodName == ctxPodName && reqPodNamespace == ctxPodNamespace {
			// Non-privileged case 4 with match.
			response.Device = listPodDevices(ctxPodName, ctxPodNamespace, devCtx.mgr)
		} else {
			return response, status.Errorf(codes.PermissionDenied,
				"missing pod Name, Namespace and visibility level is to low (%d), can't proceed",
				devCtx.claims.Visibility)
		}
	}

	return response, nil
}

func (s *DeviceService) GetMetaDeviceInfo(ctx context.Context, r *pb.GetMetaDeviceInfoRequest) (*pb.GetMetaDeviceInfoResponse, error) {
	resp := &pb.GetMetaDeviceInfoResponse{}

	devCtx, err := s.LoadContext(ctx)
	if err != nil {
		return resp, err
	}

	if devCtx.claims.Visibility != types.DeviceVisibility {
		return resp, status.Errorf(codes.PermissionDenied, "wrong visibility level: %d", devCtx.claims.Visibility)
	}

	deviceInfo := devCtx.mgr.GetDeviceInfo()

	resp.Node = deviceInfo.Node
	resp.Metadata = deviceInfo.Metadata

	for _, device := range deviceInfo.Devices {
		var (
			utilization = device.GetUtilization()
			memory      = device.GetMemory()
		)

		resp.Devices = append(resp.GetDevices(), &pb.Device{
			Uuid:              device.GetUUID(),
			ModelName:         device.GetModelName(),
			Index:             uint32(device.GetDeviceIndex()),
			AllocatedShares:   uint32(device.GetAllocatedShares()),
			Shares:            uint32(device.GetShares()),
			GpuUtilization:    utilization.Gpu,
			MemoryUtilization: utilization.Memory,
			MemoryShareSize:   memory.ShareSize,
			MemoryTotal:       memory.Total,
			MemoryFree:        memory.Free,
			MemoryUsed:        memory.Used,
			ResourceName:      device.GetResourceName(),
			NodeName:          device.GetNodeName(),
		})
	}

	return resp, nil
}

func (s *DeviceService) PingServer(ctx context.Context, r *pb.PingServerRequest) (*pb.PingServerResponse, error) {
	return &pb.PingServerResponse{}, nil
}
