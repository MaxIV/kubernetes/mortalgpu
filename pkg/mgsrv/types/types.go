package types

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

const (
	// DeviceVisibility - privileged visibility: all data is returned.
	DeviceVisibility VisibilityLevel = 1
	// ContainerVisibility - only data for a Client Container's API called is
	// returned.
	ContainerVisibility VisibilityLevel = 4
)

const (
	ContextClaimName  = "claims"
	ContextGPUMGRName = "gpuMGR"
	ContextLoggerName = "logger"
)

type TokenClaims struct {
	Aud          []string `json:"aud"`
	Exp          int      `json:"exp"`
	Iat          int      `json:"iat"`
	Iss          string   `json:"iss"`
	KubernetesIo struct {
		Namespace string `json:"namespace"`
		Pod       struct {
			Name string `json:"name"`
			UID  string `json:"uid"`
		} `json:"pod"`
		Serviceaccount struct {
			Name string `json:"name"`
			UID  string `json:"uid"`
		} `json:"serviceaccount"`
		Warnafter int `json:"warnafter"`
	} `json:"kubernetes.io"`
	Nbf int    `json:"nbf"`
	Sub string `json:"sub"`

	Visibility VisibilityLevel `json:"-"`
}

var _ json.Unmarshaler = (*TokenClaims)(nil)

var ErrTokenClaimsInvalid = errors.New("invalid data in Token Claims")

func (tc *TokenClaims) UnmarshalJSON(data []byte) error {
	type tmpClaims TokenClaims

	var tmp tmpClaims

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	if tmp.KubernetesIo.Namespace == "" {
		return fmt.Errorf("%w: /kubernetes.io/namespace must not be empty", ErrTokenClaimsInvalid)
	}

	if tmp.KubernetesIo.Pod.Name == "" {
		return fmt.Errorf("%w: /kubernetes.io/pod/name must not be empty", ErrTokenClaimsInvalid)
	}

	if tmp.KubernetesIo.Serviceaccount.Name == "" {
		return fmt.Errorf("%w: /kubernetes.io/serviceaccount/name must not be empty", ErrTokenClaimsInvalid)
	}

	if tmp.Sub == "" {
		return fmt.Errorf("%w: /sub must not be empty", ErrTokenClaimsInvalid)
	}

	*tc = TokenClaims(tmp)

	return nil
}

type VisibilityLevel uint32

func (vis VisibilityLevel) String() string {
	switch vis {
	case ContainerVisibility:
		return "ContainerVisibility"
	case DeviceVisibility:
		return "DeviceVisibility"
	default:
		return "Unknown value: " + strconv.Itoa(int(vis))
	}
}
