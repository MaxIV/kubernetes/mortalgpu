package plugin

import (
	"context"
	"sync"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// EnvVar environment variable to override.
type EnvVar struct {
	// Name environment variable name.
	Name string `json:"name,omitempty" yaml:"name,omitempty"`
	// Value environment variable value.
	Value string `json:"value,omitempty" yaml:"value,omitempty"`
}

// MarshalLogObject implements zap.ObjectMarshaler.
func (ev *EnvVar) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("name", ev.Name)
	enc.AddString("value", ev.Value)

	return nil
}

// DeviceManager main interface of device plugin.
type DeviceManager interface {
	GetPluginDevices(logger *zap.Logger) []*pluginapi.Device
	GetSharedDeviceResourceName() string
	GetUnixSocket() string
	ParseRealDeviceIDs(logger *zap.Logger, mortalDevicesIDs []string) map[string]int
	GetAllocatedDeviceSpecs(logger *zap.Logger, devUUIDs []string) []*pluginapi.DeviceSpec
	LookupSharedDevice(devUUID string) *sharecfg.SharedDevice
	MetagpuAllocation(logger *zap.Logger, allocationSize int, availableDevIDs []string) ([]string, error)
}

// DevicePluginMount for mounting host files.
type DevicePluginMount struct {
	HostPath  string `json:"hostPath,omitempty"  yaml:"hostPath,omitempty"`
	MountPath string `json:"mountPath,omitempty" yaml:"mountPath,omitempty"`
}

func (dpm *DevicePluginMount) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("host_path", dpm.HostPath)
	enc.AddString("mount_path", dpm.MountPath)

	return nil
}

// MetaGpuDevicePlugin device plugin implementation.
type MetaGpuDevicePlugin struct {
	DeviceManager
	server               *grpc.Server
	socket               string
	ctx                  context.Context
	ctxCf                context.CancelFunc
	metaGpuRecalculation chan bool

	pluginMounts []*DevicePluginMount

	deviceSpecs              bool
	containerEnvVarsOverride []*EnvVar

	logger *zap.Logger
}

// NvidiaDeviceManager GPU manager.
type NvidiaDeviceManager struct {
	devices  []*sharecfg.SharedDevice
	cacheTTL time.Duration
	shareCfg *sharecfg.DeviceSharingConfig

	allocator types.Allocator

	mtx sync.RWMutex
}
