package plugin

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"path"
	"strings"
	"time"

	"go.uber.org/zap"
	"golang.org/x/exp/slices"
	"google.golang.org/grpc"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

const (
	kubeletSocket               = pluginapi.KubeletSocket
	kubeletSocketWatchInterval  = 5 * time.Second
	kubeletSocketConnectTimeout = 5 * time.Second
)

const (
	pluginServerConnectionTestTO = 3 * time.Second
)

func (*MetaGpuDevicePlugin) dial(socket string, timeout time.Duration) (*grpc.ClientConn, error) {
	c, err := grpc.Dial(socket, grpc.WithInsecure(), grpc.WithBlock(),
		grpc.WithContextDialer(func(_ context.Context, _ string) (net.Conn, error) {
			conn, err := net.DialTimeout("unix", socket, timeout)
			if err != nil {
				return nil, fmt.Errorf("can not connect to socket: %w", err)
			}

			return conn, nil
		}),
	)
	if err != nil {
		return nil, err
	}

	return c, nil
}

// Register implements Register device plugin API call.
func (p *MetaGpuDevicePlugin) Register(logger *zap.Logger) error {
	iLog := logger.Named("gRPC_method").Named("Register")

	conn, err := p.dial(kubeletSocket, kubeletSocketConnectTimeout)
	if err != nil {
		iLog.With(zap.Error(err)).Error("can not connect to Kubelet")

		return err
	}

	defer func() {
		if e := conn.Close(); e != nil {
			iLog.Error("can not close UNIX socket connection",
				zap.Error(e), zap.String("socket", kubeletSocket))
		}
	}()

	client := pluginapi.NewRegistrationClient(conn)
	req := &pluginapi.RegisterRequest{
		Version:      pluginapi.Version,
		Endpoint:     path.Base(p.socket),
		ResourceName: p.GetSharedDeviceResourceName(),
		Options: &pluginapi.DevicePluginOptions{
			GetPreferredAllocationAvailable: true,
		},
	}

	if _, err := client.Register(context.Background(), req); err != nil {
		iLog.With(zap.Error(err)).Error("can not register")

		return fmt.Errorf("can not register device plugin: %w", err)
	}

	return nil
}

// GetDevicePluginOptions implements GetDevicePluginOptions device plugin API call.
func (*MetaGpuDevicePlugin) GetDevicePluginOptions(context.Context,
	*pluginapi.Empty,
) (*pluginapi.DevicePluginOptions, error) {
	return &pluginapi.DevicePluginOptions{GetPreferredAllocationAvailable: true}, nil
}

// ListAndWatch implements ListAndWatch device plugin API call.
func (p *MetaGpuDevicePlugin) ListAndWatch(_e *pluginapi.Empty, srv pluginapi.DevicePlugin_ListAndWatchServer) error {
	logger := p.logger.Named("gRPC_method").Named("ListAndWatch")

	devices := p.GetPluginDevices(logger)

	if l := logger.Check(zap.DebugLevel, "GetPluginDevices result"); l != nil {
		l.Write(zap.Any("devices", devices))
	}

	if err := srv.Send(&pluginapi.ListAndWatchResponse{Devices: devices}); err != nil {
		logger.Error("can not connect to DevicePlugin_ListAndWatchServer", zap.Error(err))
	}

	for {
		select {
		case <-p.ctx.Done():
			return nil
		case <-p.metaGpuRecalculation:
			devices := p.GetPluginDevices(logger)

			if l := logger.Check(zap.DebugLevel, "GetPluginDevices result"); l != nil {
				l.Write(zap.Any("devices", devices))
			}

			if err := srv.Send(&pluginapi.ListAndWatchResponse{Devices: devices}); err != nil {
				logger.Error("can not connect to DevicePlugin_ListAndWatchServer", zap.Error(err))
			}
		}
	}
}

// GetPreferredAllocation implements GetPreferredAllocation device plugin API call.
func (p *MetaGpuDevicePlugin) GetPreferredAllocation(_ context.Context,
	request *pluginapi.PreferredAllocationRequest,
) (*pluginapi.PreferredAllocationResponse, error) {
	plLogger := p.logger.Named("gRPC_method").Named("GetPreferredAllocation")

	allocResponse := &pluginapi.PreferredAllocationResponse{}

	// Just to ensure that 2 calls to handle container device
	// requests do to get the same device names as responses,
	// we will store the state here and use it to filter available device IDs
	// from the ContainerRequests.
	var usedFilter *deviceIDsFilter

	if len(request.ContainerRequests) > 0 {
		usedFilter = newDeviceIDsFilter()
	}

	for _, req := range request.ContainerRequests {
		// Annotate logger with available fields.
		logger := plLogger.With(zap.Int32("req_allocation_size", req.GetAllocationSize()))

		// Filter if necessary, see above.
		var availableDevIDs []string

		if usedFilter != nil {
			availableDevIDs = usedFilter.filterContainerRequests(req.GetAvailableDeviceIDs())
		} else {
			availableDevIDs = req.GetAvailableDeviceIDs()
		}

		if logger.Level().Enabled(zap.DebugLevel) {
			logger = logger.
				With(zap.Strings("req_available_device_ids", req.GetAvailableDeviceIDs())).
				With(zap.Strings("req_filtered_available_device_ids", availableDevIDs)).
				With(zap.Strings("req_must_include_device_ids", req.GetMustIncludeDeviceIDs()))
		}

		var (
			err                    error
			allocContainerResponse = &pluginapi.ContainerPreferredAllocationResponse{}
		)

		allocContainerResponse.DeviceIDs, err = p.MetagpuAllocation(logger,
			int(req.AllocationSize), availableDevIDs)
		if err != nil {
			// It might be not the best idea to fail early
			// but on the other hand, it is better to know that something happened
			// than ignore this error.
			logger.Error("can not allocate request",
				zap.Error(err),
				zap.Uint64("allocation_request_size", uint64(req.AllocationSize)),
				zap.Strings("available_mortal_gpus", req.GetAvailableDeviceIDs()),
				zap.Strings("available_filtered_mortal_gpus", availableDevIDs),
			)

			return nil, err
		}

		if logger.Level().Enabled(zap.DebugLevel) {
			logger.Debug("preferred devices ids:", zap.Any("device_ids", allocContainerResponse.DeviceIDs))
		}

		if usedFilter != nil {
			usedFilter.appendUsed(allocContainerResponse.DeviceIDs)
		}

		allocResponse.ContainerResponses = append(allocResponse.ContainerResponses, allocContainerResponse)
	}

	return allocResponse, nil
}

// Allocate implements Allocate device plugin API call.
func (p *MetaGpuDevicePlugin) Allocate(
	_ctx context.Context,
	request *pluginapi.AllocateRequest,
) (*pluginapi.AllocateResponse, error) {
	pLogger := p.logger.Named("gRPC_method").Named("Allocate")

	allocResponse := &pluginapi.AllocateResponse{}

	for _, req := range request.ContainerRequests {
		logger := pLogger

		if logger.Level().Enabled(zap.DebugLevel) {
			logger = logger.With(zap.Strings("requested_device_ids", req.GetDevicesIDs()))
		}

		response := pluginapi.ContainerAllocateResponse{}
		// set GPU container env variables
		slices.Sort(req.DevicesIDs)

		logger.Info("requested devices ids:", zap.Strings("devices_ids", req.DevicesIDs))

		// Compute requested GPU RAM size.
		parsedRealDevices := p.ParseRealDeviceIDs(logger, req.DevicesIDs)
		gpuUUIDs := make([]string, 0, len(parsedRealDevices))

		for gpu, mortals := range parsedRealDevices {
			if mortals > 0 {
				gpuUUIDs = append(gpuUUIDs, gpu)
			}
		}

		// return list of container devices added by Nvidia Container Runtime
		if p.deviceSpecs {
			response.Devices = p.GetAllocatedDeviceSpecs(logger, gpuUUIDs)
		}

		// set response environment
		response.Envs = map[string]string{
			"NVIDIA_VISIBLE_DEVICES": strings.Join(gpuUUIDs, ","),
			"MG_CTL_ADDR":            os.Getenv("POD_IP") + ":" + os.Getenv("MGDP_PORT"),
		}

		// Override environment, if requested.
		for _, envVar := range p.containerEnvVarsOverride {
			response.Envs[envVar.Name] = envVar.Value
		}

		// set response host mounts (including mgctl mounting)
		var mounts []*pluginapi.Mount

		for _, mount := range p.pluginMounts {
			mounts = append(mounts, &pluginapi.Mount{
				HostPath:      mount.HostPath,
				ContainerPath: mount.MountPath,
				ReadOnly:      true,
			})
		}

		response.Mounts = mounts

		allocResponse.ContainerResponses = append(allocResponse.ContainerResponses, &response)
	}

	if l := pLogger.Check(zap.DebugLevel, "Admission response"); l != nil {
		l.Write(zap.Any("response", allocResponse))
	}

	return allocResponse, nil
}

// PreStartContainer stub for deviceplugin API hook.
func (*MetaGpuDevicePlugin) PreStartContainer(context.Context,
	*pluginapi.PreStartContainerRequest,
) (*pluginapi.PreStartContainerResponse, error) {
	return &pluginapi.PreStartContainerResponse{}, nil
}

// Serve starts GRPC server to handle deviceplugin API calls.
func (p *MetaGpuDevicePlugin) Serve(log *zap.Logger) error {
	logger := log.Named("Serve")

	// Start gRPC socket server.
	go func() {
		iLog := logger.Named("gRPCServer")

		if err := os.Remove(p.socket); err != nil {
			if !errors.Is(err, os.ErrNotExist) {
				iLog.Error("can not remove old socket", zap.Error(err))
			}
		}

		sock, err := net.Listen("unix", p.socket)
		if err != nil {
			iLog.With(zap.Error(err)).Error("can not listen to GRPC socket")
		}

		defer func() {
			if err := sock.Close(); err != nil {
				iLog.Error("can not close server socket", zap.Error(err))
			}
		}()

		iLog.Info("gRCP socket listening", zap.String("socket", p.socket))
		iLog.Info("Initializing gRPC server")

		p.server = grpc.NewServer([]grpc.ServerOption{}...)

		pluginapi.RegisterDevicePluginServer(p.server, p)

		if err := p.server.Serve(sock); err != nil {
			iLog.Fatal("gRPC server crashed", zap.Error(err))
		}
	}()

	if conn, err := p.dial(p.socket, pluginServerConnectionTestTO); err != nil {
		logger.With(zap.Error(err)).Error("GRPC server socket connection test failed")

		return err
	} else {
		if err := conn.Close(); err != nil {
			logger.Error("can not close test client connection", zap.Error(err))
		}

		logger.Info("gRPC server successfully started and ready accept new connections")
	}

	return nil
}

// Start starts internal plugin loops.
func (p *MetaGpuDevicePlugin) Start() {
	logger := p.logger.Named("Start")

	p.ctx, p.ctxCf = context.WithCancel(context.Background())

	if err := p.Serve(logger); err != nil {
		logger.Fatal("GRPC server failed", zap.Error(err))
	}

	if err := p.Register(logger); err != nil {
		logger.Fatal("Plugin registration failed", zap.Error(err))
	}

	p.startWatchKubelet(logger)
}

// Stop stops the plugin.
func (p *MetaGpuDevicePlugin) Stop() {
	var logger *zap.Logger

	if p == nil {
		logger = zap.L().Named("MetaGpuDevicePlugin").Named("Stop") // Fallback to default logger.
	} else {
		logger = p.logger
	}

	logger = logger.Named("Stop")

	logger.Info("stopping GRPC server")

	if p != nil && p.server != nil {
		p.server.Stop()
	}

	logger.Info("removing unix socket")

	if err := os.Remove(p.socket); err != nil {
		logger.Error("can not remove server socket", zap.Error(err))
	}

	logger.Info("closing all channels")

	p.ctxCf()
}

// startWatchKubelet starts a loop to check if Kubelet was restarted
// and perform register of Mortal GPUs.
func (p *MetaGpuDevicePlugin) startWatchKubelet(logger *zap.Logger) {
	// Inspired by https://github.com/Mellanox/k8s-rdma-shared-dev-plugin/blob/master/pkg/resources/server.go#L224.
	go func() {
		const (
			logKeySocketPath         = "socket_path"
			logKeyKubeletSockModTime = "kubelet_socket_mod_time"
		)

		iLog := logger.Named("StartWatchKubelet")

		iLog.Info("Starting watching Kubelet socket to reach on Kubelet restarts")

		sockStat, err := os.Lstat(kubeletSocket)
		if err != nil {
			iLog.Fatal("Kubelet socket is not found", zap.Error(err), zap.String(logKeySocketPath, kubeletSocket))
		}

		iLog.Info("Kubelet socket is present",
			zap.String(logKeySocketPath, kubeletSocket), zap.Time(logKeyKubeletSockModTime, sockStat.ModTime()))

		initKubeletSocketTS := sockStat.ModTime()

		ticker := time.NewTicker(kubeletSocketWatchInterval)

		for {
			select {
			case <-p.ctx.Done():
				iLog.Info("Stopping")
				ticker.Stop()

				return

			case tm := <-ticker.C:
				if l := iLog.Check(zap.DebugLevel, "Checking Kubelet socket os.stat"); l != nil {
					l.Write(zap.Time("tick_ts", tm),
						zap.String(logKeySocketPath, kubeletSocket))
				}

				sockStat, err := os.Lstat(kubeletSocket)
				if err != nil {
					iLog.Warn("Kubelet socket is not found, most likely Kubelet was restarted",
						zap.Error(err), zap.String(logKeySocketPath, kubeletSocket))
					iLog.Info("Waiting a bit and restarting MetaGPU device plugin",
						zap.Duration("duration", kubeletSocketWatchInterval))
					time.Sleep(kubeletSocketWatchInterval)
					iLog.Info("Restarting MetaGPU device plugin")

					p.Stop()
					p.Start()

					return
				}

				if sockStat.ModTime().Compare(initKubeletSocketTS) == 0 {
					if l := iLog.Check(zap.DebugLevel, "Kubelet socket is present, modification time is not changed"); l != nil {
						l.Write(zap.String(logKeySocketPath, kubeletSocket), zap.Time(logKeyKubeletSockModTime, sockStat.ModTime()))
					}
				} else {
					iLog.Warn("Kubelet socket is found but its modification time is changed. most likely Kubelet was restarted",
						zap.String(logKeySocketPath, kubeletSocket),
						zap.Time("old_mod_time", initKubeletSocketTS),
						zap.Time("new_mod_time", sockStat.ModTime()))
					iLog.Info("Waiting a bit and restarting MetaGPU device plugin",
						zap.Duration("duration", kubeletSocketWatchInterval))
					time.Sleep(kubeletSocketWatchInterval)
					iLog.Info("Restarting MetaGPU device plugin")

					p.Stop()
					p.Start()

					return
				}
			}
		}
	}()
}

// NewMetaGpuDevicePlugin constructor for the plugin.
func NewMetaGpuDevicePlugin(logger *zap.Logger,
	pluginMounts []*DevicePluginMount,
	deviceSpecsEnable bool,
	containerEnvVarsOverride []*EnvVar,
	metaGpuRecalculation chan bool,
	deviceMgr DeviceManager,
) *MetaGpuDevicePlugin {
	log := logger.Named("MetaGpuDevicePlugin")

	return &MetaGpuDevicePlugin{
		server:                   nil,
		socket:                   fmt.Sprintf("%s%s", pluginapi.DevicePluginPath, deviceMgr.GetUnixSocket()),
		DeviceManager:            deviceMgr,
		metaGpuRecalculation:     metaGpuRecalculation,
		logger:                   log,
		pluginMounts:             pluginMounts,
		deviceSpecs:              deviceSpecsEnable,
		containerEnvVarsOverride: containerEnvVarsOverride,
	}
}

type deviceIDsFilter struct {
	used map[string]bool
}

func newDeviceIDsFilter() *deviceIDsFilter {
	return &deviceIDsFilter{used: make(map[string]bool)}
}

// filterContainerRequests filters availableDevIDs to exclude usedDeviceIDs from them.
func (dif *deviceIDsFilter) filterContainerRequests(availableDevIDs []string) []string {
	result := make([]string, 0, len(availableDevIDs)-len(dif.used))

	for i := range availableDevIDs {
		if _, used := dif.used[availableDevIDs[i]]; !used {
			result = append(result, availableDevIDs[i])
		}
	}

	return result
}

func (dif *deviceIDsFilter) appendUsed(usedIDs []string) {
	for i := range usedIDs {
		dif.used[usedIDs[i]] = true
	}
}
