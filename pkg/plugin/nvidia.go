package plugin

import (
	b64 "encoding/base64"
	"fmt"
	"os"
	"time"

	"github.com/NVIDIA/go-nvml/pkg/nvml"
	"go.uber.org/zap"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	mortalnames "gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mortal-names"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/nvmlutils"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// Just a dictionary of keys for logging.
// Starts from "lk" like "logging key".
const (
	lkSharedDevice = "shared_device"

	lkIsMigDevice = "is_mig_device"
)

// CacheDevices starts a loop which retrieves available physical GPUs
// and populates local cache.
func (m *NvidiaDeviceManager) CacheDevices(logger *zap.Logger) {
	iLog := logger.Named("CacheDevices")

	go func() {
		for {
			<-time.After(m.cacheTTL)
			if err := m.setDevices(iLog); err != nil {
				iLog.Error("Nvidia Device Manager loop step error", zap.Error(err))
			}
		}
	}()
}

// ParseRealDeviceIDs extracts physical GPU UUIDs from mortalDevicesIDs.
// Returns a map where key - GPU UUID, value - how many MortalGPUs
// from mortalDevicesIDs belong to the GPU.
func (m *NvidiaDeviceManager) ParseRealDeviceIDs(
	logger *zap.Logger, mortalDevicesIDs []string,
) map[string]int {
	iLog := logger.Named("ParseRealDeviceIDs")

	realDevicesIDsMap := make(map[string]int, len(mortalDevicesIDs))

	m.mtx.RLock()
	defer m.mtx.RUnlock()

	for _, mortalDevicesID := range mortalDevicesIDs {
		deviceID, err := mortalnames.ParseDeviceUUID(mortalDevicesID)
		if err != nil {
			iLog.Error("Can not parse MortalGPU name, ignoring it", zap.Error(err))

			continue
		}

		if !m.DeviceExists(deviceID) {
			iLog.Error("device does not exists, but was claimed", zap.String("mortal_device_id", mortalDevicesID))

			continue
		}

		realDevicesIDsMap[deviceID]++
	}

	return realDevicesIDsMap
}

// GetAllocatedDeviceSpecs return the list of pluginapi.DeviceSpec matching Nvidia Container Toolkit mounts
func (m *NvidiaDeviceManager) GetAllocatedDeviceSpecs(
	logger *zap.Logger, devUUIDs []string,
) []*pluginapi.DeviceSpec {
	var devSpecs []*pluginapi.DeviceSpec

	m.mtx.RLock()
	defer m.mtx.RUnlock()

	iLog := logger.Named("GetAllocatedDeviceSpecs")

	// common nvidia dev files (optional, if exists)
	optionalNvidiaPaths := []string{
		"/dev/nvidiactl",
		"/dev/nvidia-uvm",
		"/dev/nvidia-uvm-tools",
		"/dev/nvidia-modeset",
	}

	for _, devPath := range optionalNvidiaPaths {
		if _, err := os.Stat(devPath); err != nil {
			continue
		}

		spec := &pluginapi.DeviceSpec{
			ContainerPath: devPath,
			HostPath:      devPath,
			Permissions:   "rw",
		}

		devSpecs = append(devSpecs, spec)
	}

	// device files matching allocated devices
	for _, devUUID := range devUUIDs {
		nvmlDevice := nvmlutils.GetDeviceByUUID(iLog, devUUID)
		if nvmlDevice == nil {
			iLog.Error("failed to get device by UUID", zap.String("device_uuid", devUUID))

			continue
		}

		devMinorNumber, ret := nvmlDevice.GetMinorNumber()
		nvmlutils.ErrorCheck(iLog, ret)

		devPath := fmt.Sprintf("/dev/nvidia%d", devMinorNumber)
		spec := &pluginapi.DeviceSpec{
			ContainerPath: devPath,
			HostPath:      devPath,
			Permissions:   "rw",
		}

		devSpecs = append(devSpecs, spec)
	}

	return devSpecs
}

// LookupSharedDevice returns a copy of a shared device, if found. nil otherwise.
func (m *NvidiaDeviceManager) LookupSharedDevice(devUUID string) *sharecfg.SharedDevice {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	for _, d := range m.devices {
		if d.UUID == devUUID {
			return d.Copy()
		}
	}

	return nil
}

// DeviceExists checks if the given UUID is in shared devices.
func (m *NvidiaDeviceManager) DeviceExists(physDeviceUUID string) bool {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	for _, d := range m.devices {
		if d.UUID == physDeviceUUID {
			return true
		}
	}

	return false
}

// GetPluginDevices implements device plugin API GetPluginDevices.
func (m *NvidiaDeviceManager) GetPluginDevices(logger *zap.Logger) []*pluginapi.Device {
	var (
		metaGpus []*pluginapi.Device
		iLog     = logger.Named("GetPluginDevices")
	)

	m.mtx.RLock()
	defer m.mtx.RUnlock()

	iLog.Info("Generating MortalGPU devices", zap.Int("gpu_devices_count", len(m.devices)))

	for _, d := range m.devices {
		for j := 0; j < int(d.Shares.SharesCount); j++ {
			metaGpus = append(metaGpus, &pluginapi.Device{
				ID: mortalnames.FormatDeviceID(&mortalnames.DeviceIDInfo{
					GPUUUID: d.UUID, GPUIndex: d.Index, MortalGPUIndex: j,
				}),
				Health: pluginapi.Healthy,
			})
		}
	}

	iLog.Info("Created MortalGPU devices", zap.Int("count", len(metaGpus)))

	if l := iLog.Check(zap.DebugLevel, "MortalGPU devices list"); l != nil {
		l.Write(zap.Any("plugin_devices", metaGpus))
	}

	return metaGpus
}

func (m *NvidiaDeviceManager) newManagedDevice(log *zap.Logger,
	device nvml.Device, uuid string, idx int,
) (*sharecfg.SharedDevice, error) {
	logger := log.Named("appendManagedDevice")

	memory := nvmlutils.GetDeviceMemory(logger, device)

	sharesInfo, err := m.shareCfg.ComputeSharesInfo(memory.Total)
	if err != nil {
		logger.Error("Can not compute Shares", zap.Error(err))

		return nil, fmt.Errorf("can not compute GPU device shares: %w", err)
	}

	newDev := &sharecfg.SharedDevice{
		UUID:   uuid,
		Index:  idx,
		Shares: sharesInfo,
	}

	if l := logger.Check(zap.DebugLevel, "Added a managed device"); l != nil {
		l.Write(zap.Object(lkSharedDevice, newDev))
	}

	return newDev, nil
}

// setDevices performs update of devices cache.
func (m *NvidiaDeviceManager) setDevices(logger *zap.Logger) error {
	iLog := logger.Named("setDevices")

	var (
		devices     []*sharecfg.SharedDevice
		nvmlDevices = nvmlutils.GetDevices(iLog)
	)

	iLog.Info("refreshing nvidia devices cache", zap.Int("total", len(nvmlDevices)))

	idx := 0

	for _, device := range nvmlDevices {
		uuid := nvmlutils.GetDeviceUUID(iLog, device)
		modelName := nvmlutils.GetDeviceName(iLog, device)
		iiLog := iLog.With(zap.String("device_uuid", uuid), zap.String("model_name", modelName))

		iiLog.Debug("Checking device")

		if nvmlutils.IsMigDevice(iiLog, device) {
			iiLog = iiLog.With(zap.Bool(lkIsMigDevice, true))

			for _, migdevice := range nvmlutils.GetMigDevices(iiLog, device) {
				miguuid := nvmlutils.GetDeviceUUID(iiLog, migdevice)
				migidx := nvmlutils.GetMigInstanceId(iiLog, migdevice)

				iiLog = iiLog.With(zap.String("mig_uuid", miguuid), zap.Int("mig_index", migidx))

				if m.isManagedDevice(uuid, modelName, migidx) {
					newDev, err := m.newManagedDevice(iiLog, migdevice, miguuid, idx)
					if err != nil {
						return fmt.Errorf("can not create a new managed MIG device: %w", err)
					}

					devices = append(devices, newDev)
					idx++
				} else {
					iiLog.Debug("The device is not a managed device, ignoring")
				}
			}
		} else {
			iiLog = iiLog.With(zap.Bool(lkIsMigDevice, false))

			if m.isManagedDevice(uuid, modelName, -1) {
				newDev, err := m.newManagedDevice(iiLog, device, uuid, idx)
				if err != nil {
					return fmt.Errorf("can not create a new managed non-MIG device: %w", err)
				}

				// verify accounting mode is on
				state := nvmlutils.GetAccountingMode(iiLog, device)
				if state != nvml.FEATURE_ENABLED {
					// enable accounting mode
					nvmlutils.EnableAccountingMode(iiLog, device)
				}

				iiLog.Info("accounting mode for device", zap.String("device", uuid), zap.Int32("state", int32(state)))

				devices = append(devices, newDev)
				idx++
			} else {
				iiLog.Debug("The device is not a managed device, ignoring")
			}
		}
	}

	m.mtx.Lock()
	m.devices = devices
	m.mtx.Unlock()

	return nil
}

// isManagedDevice checks if a device is managed, i.e. in shared devices
// configuration.
func (m *NvidiaDeviceManager) isManagedDevice(deviceUUID, modelName string, migID int) bool {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	return sharecfg.IsManagedDevice(m.shareCfg, deviceUUID, modelName, migID)
}

// GetSharedDeviceResourceName returns a shared container resource name.
func (m *NvidiaDeviceManager) GetSharedDeviceResourceName() string {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	return m.shareCfg.ResourceName
}

// GetUnixSocket create a predictable and unique name for UNIX socket
// for device registration.
func (m *NvidiaDeviceManager) GetUnixSocket() string {
	return b64.StdEncoding.EncodeToString([]byte(m.shareCfg.ResourceName))
}

// MetagpuAllocation performs devices allocation to fulfill request from the
// available devices.
// usedPreAllocsDevIDs is a bring-your-own state storage to keep track
// of device allocations performed by previous calls of the MetagpuAllocation
// during processing of the same ContainerPreferredAllocationRequest.
func (m *NvidiaDeviceManager) MetagpuAllocation(logger *zap.Logger,
	allocationSize int, availableMortalGPUIDs []string,
) ([]string, error) {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	iLog := logger.Named("MetagpuAllocation")

	allocation, err := m.allocator.Allocate(iLog, &types.AllocationRequest{
		PhysicalGPUs:        m.devices,
		AvailableMortalGPUs: availableMortalGPUIDs,
		Size:                allocationSize,
	})
	if err != nil {
		return nil, fmt.Errorf("allocation error: %w", err)
	}

	return allocation.GetMortalGPUs(), nil
}

// NewNvidiaDeviceManager creates a new instance of the device manager.
func NewNvidiaDeviceManager(logger *zap.Logger,
	cacheTTL time.Duration,
	shareCfg *sharecfg.DeviceSharingConfig,
	allocator types.Allocator,
) (*NvidiaDeviceManager, error) {
	iLog := logger.Named("NvidiaDeviceManager")

	ndm := &NvidiaDeviceManager{
		cacheTTL:  cacheTTL,
		shareCfg:  shareCfg,
		allocator: allocator,
	}

	// Do a first loop step to validate configuration.
	if err := ndm.setDevices(iLog); err != nil {
		return nil, fmt.Errorf("can not init NvidiaDeviceManager, probably incorrect configuration: %w", err)
	}

	// start devices cache loop
	ndm.CacheDevices(iLog)

	return ndm, nil
}
