// Package allocator2 implements Mortal GPUs allocation
// from allocatable GPUs.
// The algorithm tries to pack all the workloads to as small number of
// physical GPUs as possible by collocating containers to the same GPUs.
package collocate

import (
	"fmt"

	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
)

// Just a dictionary of keys for logging.
// Starts from "lk" like "logging key".
const (
	lkAllocationRequest = "allocation_request"
	lkLeftToAllocate    = "left_to_allocate"

	lkAllocationResult = "allocation_result"

	lkPhysicalGPUUUIDs = "physical_gpu_uuids"
	lkPhysicalGPUUUID  = "physical_gpu_uuid"

	lkCandidateGPUs     = "gpu_candidates"
	lkCandidateGPUIndex = "gpu_candidate_index"

	lkGPUTotalShares = "gpu_total_shares"
	lkGPUFreeShares  = "gpu_free_shares"

	lkSmallestSharesCnt = "smallest_gpu_shares"
	lkLargestSharesCnt  = "largest_gpu_shares"

	lkLogNameWUG  = "WholeUnusedGPUs"
	lkLogNameWSUG = "WholeSomewhatUsedGPUs"
	lkLogNameFSUG = "FractionsSomewhatUsedGPUs"
	lkLogNameFUG  = "FractionsUnusedGPUs"
)

const (
	errAllocMsgFmt      = "can not allocate whole GPUs from %s GPUs: %w"
	errAllocLeftMsgFmt  = "%w: left to allocate %d shares"
	errAllocWholeGPUFmt = "can not allocate a whole GPU: %w: candidate GPU index=%d, allocation request size=%d"
	errAllocFromGPUFmt  = "can not allocate from a GPU: %w: candidate GPU index=%d, allocation request size=%d"
)

const (
	logMsgTooSmallReq     = "Allocation request is smaller than the smallest GPU, cannot allocate a whole GPU with such a request" //nolint:lll // log message
	logMsgTooBigReq       = "Allocation request is larger than the largest GPU, cannot fulfill the request with this GPUs set"     //nolint:lll // log message
	logMsgAllocBug        = "Can not allocate from a GPU, it is probably a bug"
	logMsgGPUsSetNotAlloc = "Given GPUs set does not have allocatable MortalGPUs"
	logMsgAllocResult     = "Allocation result"
)

// DeviceAllocator performs allocation of MortalGPUs.
type DeviceAllocator struct {
	noSplitRequest bool
}

var _ types.Allocator = (*DeviceAllocator)(nil) // type check.

// NewDeviceAllocator creates a new DeviceAllocator.
func NewDeviceAllocator(noSplitRequest bool) *DeviceAllocator {
	return &DeviceAllocator{noSplitRequest: noSplitRequest}
}

func (da *DeviceAllocator) Allocate(log *zap.Logger,
	request *types.AllocationRequest,
) (*types.AllocationResult, error) {
	logger := log.Named("Allocate")

	loadMaps, err := types.NewLoadMap(logger, request.AvailableMortalGPUs, request.PhysicalGPUs)
	if err != nil {
		return nil, fmt.Errorf("can not compute current GPUs usage: %w", err)
	}

	var (
		unusedGPUs            = loadMaps.GetUnusedGPUs()
		somewhatAllocatedGPUs = loadMaps.GetAllocatableGPUs()
	)

	if da.noSplitRequest {
		return da.allocateNoSplit(logger, unusedGPUs, somewhatAllocatedGPUs, request.Size)
	} else { //nolint:revive // indent-error-flow - if-else is intentional to show the idea.
		return da.allocateSplit(logger, unusedGPUs, somewhatAllocatedGPUs, request.Size)
	}
}

// Allocate performs allocation and returns its result.
func (DeviceAllocator) allocateSplit(log *zap.Logger, //nolint:dupl,lll // logic is separated to 2 functions to improve readability
	unusedGPUs, somewhatAllocatedGPUs *types.GPUsSet,
	allocationRequest int,
) (*types.AllocationResult, error) {
	logger := log.Named("WithSplit")

	var (
		leftToAllocate = allocationRequest
		result         = types.NewAllocationResult()
		err            error
	)

	// Try to allocate whole GPUs from unused GPUs.
	leftToAllocate, err = greedyAllocWholes(logger.Named(lkLogNameWUG),
		unusedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameWUG, err)
	}

	// Try to allocate whole GPUs from somewhat-used GPUs.
	leftToAllocate, err = greedyAllocWholes(logger.Named(lkLogNameWSUG),
		somewhatAllocatedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameWSUG, err)
	}

	// Try to allocate fractions from somewhat-used GPUs.
	leftToAllocate, err = greedyAllocFractions(logger.Named(lkLogNameFSUG),
		somewhatAllocatedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameFSUG, err)
	}

	// Try to allocate fractions from unused GPUs.
	leftToAllocate, err = greedyAllocFractions(logger.Named(lkLogNameFUG),
		unusedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameFUG, err)
	}

	if leftToAllocate > 0 {
		return nil, fmt.Errorf(errAllocLeftMsgFmt, types.ErrAllocation, leftToAllocate)
	}

	return result, nil
}

func (DeviceAllocator) allocateNoSplit(log *zap.Logger, //nolint:dupl,lll // logic is separated to 2 functions to improve readability
	unusedGPUs, somewhatAllocatedGPUs *types.GPUsSet,
	allocationRequest int,
) (*types.AllocationResult, error) {
	logger := log.Named("NoSplit")

	var (
		leftToAllocate = allocationRequest
		result         = types.NewAllocationResult()
		err            error
	)

	// Try to allocate whole GPUs from unused GPUs.
	leftToAllocate, err = greedyAllocWholesNoSplit(logger.Named(lkLogNameWUG),
		unusedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameWUG, err)
	}

	// Try to allocate whole GPUs from somewhat-used GPUs.
	leftToAllocate, err = greedyAllocWholesNoSplit(logger.Named(lkLogNameWSUG),
		somewhatAllocatedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameWSUG, err)
	}

	// Try to allocate fractions from somewhat-used GPUs.
	leftToAllocate, err = greedyAllocFractionsNoSplit(logger.Named(lkLogNameFSUG),
		somewhatAllocatedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameFSUG, err)
	}

	// Try to allocate fractions from unused GPUs.
	leftToAllocate, err = greedyAllocFractionsNoSplit(logger.Named(lkLogNameFUG),
		unusedGPUs, leftToAllocate, result)
	if err != nil {
		return nil, fmt.Errorf(errAllocMsgFmt, lkLogNameFUG, err)
	}

	if leftToAllocate > 0 {
		return nil, fmt.Errorf(errAllocLeftMsgFmt, types.ErrAllocation, leftToAllocate)
	}

	return result, nil
}

// greedyAllocWholesNoSplit allocates whole GPUs but does nothing
// if the allocation cannot fulfill the allocationRequest completely.
func greedyAllocWholesNoSplit(_log *zap.Logger,
	candidates *types.GPUsSet, allocationRequest int, result *types.AllocationResult,
) (int, error) {
	// Failsafe check.
	if allocationRequest == 0 {
		return allocationRequest, nil
	}

	logger := _log.Named("greedyAllocWholesNoSplit")
	leftToAllocate := allocationRequest

	if !candidates.IsAllocatable() {
		logger.Debug(logMsgGPUsSetNotAlloc, zap.Object(lkCandidateGPUs, candidates))

		return leftToAllocate, nil
	}

	// A minor optimization.
	// If the allocation request is smaller than the number of unallocated
	// MortalGPUs of any of the candidates,
	if leftToAllocate < candidates.GetSmallestFreeShares() {
		logger.Debug(
			logMsgTooSmallReq,
			zap.Int(lkLeftToAllocate, leftToAllocate),
			zap.Int(lkSmallestSharesCnt, candidates.GetSmallestFreeShares()))

		return leftToAllocate, nil
	}

	// An optimization to skip further steps if we cannot fulfill the request.
	if leftToAllocate > candidates.GetLargestFreeShares() {
		logger.Debug(
			logMsgTooBigReq,
			zap.Int(lkLeftToAllocate, leftToAllocate),
			zap.Int(lkLargestSharesCnt, candidates.GetLargestFreeShares()))

		return leftToAllocate, nil
	}

	gpus := candidates.Iterator()

	for gpus.Next() {
		idx, gpu := gpus.Item()

		freeShares := gpu.GetFreeShares()

		// Found a fit.
		if freeShares == leftToAllocate {
			if l := logger.Check(zap.DebugLevel, "Allocation fit is found"); l != nil {
				l.Write(
					zap.String(lkPhysicalGPUUUID, gpu.GetSharedDeviceInfo().UUID),
					zap.Int(lkGPUTotalShares, gpu.GetTotalShares()),
					zap.Int(lkGPUFreeShares, gpu.GetFreeShares()),
					zap.Int(lkLeftToAllocate, leftToAllocate),
					zap.Int(lkCandidateGPUIndex, idx),
				)
			}

			uuid, mortals, err := candidates.AllocateFromGPU(idx, leftToAllocate)
			if err != nil {
				logger.Error("Can not allocate a request to fill the GPU to be fully used",
					zap.Error(err),
					zap.Int(lkCandidateGPUIndex, idx),
					zap.Int(lkLeftToAllocate, leftToAllocate))

				return 0, fmt.Errorf(errAllocWholeGPUFmt, err, idx, leftToAllocate)
			}

			result.AppendMortalGPUs(mortals...)
			result.AppendPhysicalGPUs(uuid)

			leftToAllocate -= freeShares

			return leftToAllocate, nil
		}
	}

	// Not found, return the request value unchanged.
	return leftToAllocate, nil
}

// greedyAllocWholes allocates a GPU only if the GPU will be fully-allocated
// after the allocation.
func greedyAllocWholes(_log *zap.Logger,
	candidates *types.GPUsSet, allocationRequest int, result *types.AllocationResult,
) (int, error) {
	// Failsafe check.
	if allocationRequest == 0 {
		return allocationRequest, nil
	}

	logger := _log.Named("greedyAllocWholes")
	leftToAllocate := allocationRequest

	if !candidates.IsAllocatable() {
		logger.Debug(logMsgGPUsSetNotAlloc, zap.Object(lkCandidateGPUs, candidates))

		return leftToAllocate, nil
	}

	// A minor optimization.
	// If the allocation request is smaller than the number of unallocated
	// MortalGPUs of any of the candidates,
	if leftToAllocate < candidates.GetSmallestFreeShares() {
		logger.Debug(
			logMsgTooSmallReq,
			zap.Int(lkLeftToAllocate, leftToAllocate),
			zap.Int(lkSmallestSharesCnt, candidates.GetSmallestFreeShares()))

		return leftToAllocate, nil
	}

	candidates.SortDecreaseFreeShares()

	// Try to eat starting from the largest piece.
	gpus := candidates.Iterator()

	for gpus.Next() {
		idx, gpu := gpus.Item()

		// Already eaten before.
		if gpu == nil {
			continue
		}

		// Too large piece, will choke on it.
		freeShares := gpu.GetFreeShares()

		// The GPU has more unallocated MortalGPUs
		// than the request, no way to fill the GPU to 100% allocation.
		// Skip the GPU.
		// But go further in the loop as there is a chance that another
		// GPU with fewer MortalGPUs exists.
		if freeShares > leftToAllocate {
			if l := logger.Check(zap.DebugLevel, "GPU has more free shares than left to allocate, skipping it"); l != nil {
				l.Write(
					zap.String(lkPhysicalGPUUUID, gpu.GetSharedDeviceInfo().UUID),
					zap.Int(lkGPUTotalShares, gpu.GetTotalShares()),
					zap.Int(lkGPUFreeShares, gpu.GetFreeShares()),
					zap.Int(lkLeftToAllocate, leftToAllocate),
					zap.Int(lkCandidateGPUIndex, idx))
			}

			continue
		}

		// Allocate the whole GPU.
		uuid, mortals, err := candidates.AllocateFromGPU(idx, freeShares)
		if err != nil {
			logger.Error("Can not allocate a whole GPU",
				zap.Error(err),
				zap.Int(lkCandidateGPUIndex, idx),
				zap.Int(lkLeftToAllocate, leftToAllocate))

			return 0, fmt.Errorf(errAllocWholeGPUFmt,
				err, idx, leftToAllocate)
		}

		result.AppendMortalGPUs(mortals...)
		result.AppendPhysicalGPUs(uuid)

		leftToAllocate -= freeShares

		if leftToAllocate == 0 {
			logger.Debug("Success: Allocated just enough MortalGPUs",
				zap.Object(lkAllocationResult, result))

			break
		}
	}

	logger.Debug(logMsgAllocResult,
		zap.Int(lkLeftToAllocate, leftToAllocate),
		zap.Object(lkAllocationResult, result))

	return leftToAllocate, nil
}

// greedyAllocFractionsNoSplit allocates a GPU even if it will not be fully-allocated
// after that. Does not split a request to multiple physical GPUs.
// Tries to prefer a GPU which will be closer to "fully-allocated" after
// the allocation.
func greedyAllocFractionsNoSplit(_log *zap.Logger,
	candidates *types.GPUsSet, allocationRequest int, result *types.AllocationResult,
) (int, error) {
	// Failsafe check.
	if allocationRequest == 0 {
		return allocationRequest, nil
	}

	logger := _log.Named("greedyAllocFractionsNoSplit")
	leftToAllocate := allocationRequest

	if !candidates.IsAllocatable() {
		logger.Debug("Given GPUs set does not have allocatable Mortal GPUs",
			zap.Object(lkCandidateGPUs, candidates))

		return leftToAllocate, nil
	}

	if leftToAllocate > candidates.GetLargestFreeShares() {
		logger.Debug(
			"Request is larger than can be allocated without a split to several physical GPUs",
			zap.Object(lkCandidateGPUs, candidates),
			zap.Error(types.ErrFractionsAllocatorOverflow))

		return leftToAllocate, nil
	}

	candidates.SortDecreaseFreeShares()

	gpus := candidates.Iterator()

	for gpus.Next() {
		idx, gpu := gpus.Item()

		freeShares := gpu.GetFreeShares()

		// Fit found.
		if freeShares >= leftToAllocate {
			uuid, mortals, err := candidates.AllocateFromGPU(idx, leftToAllocate)
			if err != nil {
				logger.Error(logMsgAllocBug,
					zap.Error(err),
					zap.Int(lkCandidateGPUIndex, idx),
					zap.Int(lkLeftToAllocate, leftToAllocate))

				return 0, fmt.Errorf(errAllocFromGPUFmt, err, idx, leftToAllocate)
			}

			result.AppendMortalGPUs(mortals...)
			result.AppendPhysicalGPUs(uuid)

			return 0, nil
		}
	}

	// No fit found.
	return leftToAllocate, nil
}

// greedyAllocFractions allocates a GPU even if it will not be fully-allocated after that.
// Tries to prefer GPUs which will be closer to be fully-allocated.
// It is assumed that the allocationRequest is smaller than any of the
// free shares value on any of the candidates, i.e. no overflow.
func greedyAllocFractions(_log *zap.Logger,
	candidates *types.GPUsSet, _allocationRequest int, result *types.AllocationResult,
) (int, error) {
	// Failsafe check.
	if _allocationRequest == 0 {
		return _allocationRequest, nil
	}

	var (
		logger = _log.Named("greedyAllocFractions").With(
			zap.Int(lkAllocationRequest, _allocationRequest),
		)
		leftToAllocate = _allocationRequest
	)

	if !candidates.IsAllocatable() {
		logger.Debug("Given gpuSet does not have allocatable MortalGPUs",
			zap.Object(lkCandidateGPUs, candidates))

		return leftToAllocate, nil
	}

	if leftToAllocate > candidates.GetSmallestFreeShares() {
		logger.Error("Request overflow",
			zap.Object(lkCandidateGPUs, candidates),
			zap.Error(types.ErrFractionsAllocatorOverflow))

		return 0, fmt.Errorf("%w: request %d in %+v",
			types.ErrFractionsAllocatorOverflow, leftToAllocate, candidates)
	}

	candidates.SortDecreaseFreeShares()

	const unassigned = -1

	var (
		// Free shares left on the "best" GPU after this allocation happens.
		goodFreeLeftAfter = unassigned
		goodIdx           = unassigned
	)

	gpus := candidates.Iterator()

	for gpus.Next() {
		idx, gpu := gpus.Item()

		if gpu == nil {
			continue
		}

		freeShares := gpu.GetFreeShares()

		leftAfter := freeShares - leftToAllocate

		// Assign the first value.
		if goodFreeLeftAfter == unassigned {
			goodFreeLeftAfter = leftAfter
			goodIdx = idx
		}

		if leftAfter < goodFreeLeftAfter {
			goodFreeLeftAfter = leftAfter
			goodIdx = idx
		}
	}

	// Could not find a fit.
	if goodIdx == unassigned {
		logger.Debug("Could not find a GPU to fit the request",
			zap.Int(lkLeftToAllocate, leftToAllocate),
			zap.Object(lkCandidateGPUs, candidates))

		return leftToAllocate, nil
	}

	// Allocate.
	uuid, mortals, err := candidates.AllocateFromGPU(goodIdx, leftToAllocate)
	if err != nil {
		logger.Error(logMsgAllocBug,
			zap.Error(err),
			zap.Int(lkCandidateGPUIndex, goodIdx),
			zap.Int(lkLeftToAllocate, leftToAllocate))

		return 0, fmt.Errorf(errAllocFromGPUFmt, err, goodIdx, leftToAllocate)
	}

	result.AppendMortalGPUs(mortals...)
	result.AppendPhysicalGPUs(uuid)

	leftToAllocate -= len(mortals)

	logger.Debug(logMsgAllocResult,
		zap.Int(lkLeftToAllocate, leftToAllocate),
		zap.Object(lkAllocationResult, result))

	return leftToAllocate, nil
}
