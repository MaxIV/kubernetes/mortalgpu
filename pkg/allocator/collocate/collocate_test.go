package collocate

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type testCase struct {
	expectedResult *types.AllocationResult

	unusedGPUs types.GPUsSet

	allocationRequest int

	expectedLeftAllocations int
}

type TestAllocateWholeGPUs struct {
	suite.Suite
}

func (ts *TestAllocateWholeGPUs) TestNoAllocations() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		// No fit because no unused GPUs.
		{
			unusedGPUs:        types.NewGPUsSet(),
			allocationRequest: 100,

			expectedResult:          types.NewAllocationResultWithData([]string{}, []string{}),
			expectedLeftAllocations: 100,
		},
		// No fit because all the GPUs are larger than the request.
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
				},
			),
			allocationRequest: 2,

			expectedResult:          types.NewAllocationResultWithData([]string{}, []string{}),
			expectedLeftAllocations: 2,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := greedyAllocWholes(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func (ts *TestAllocateWholeGPUs) TestPartiallyAllocated() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					// This GPU should be chosen
					// as the largest one available which
					// the request could fully allocate.
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
				},
			),
			allocationRequest: 6,

			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-0-0",
					"mortal-gpu-0-1",
					"mortal-gpu-0-2",
					"mortal-gpu-0-3",
					"mortal-gpu-0-4",
				},
				[]string{"uuid-gpu-0"},
			),
			expectedLeftAllocations: 1,
		},
		// 2 GPUs should be selected
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					types.NewDeviceLoad(
						// This GPU should be chosen
						// as the largest one available which
						// the request could fully allocate.
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
				},
			),
			allocationRequest: 7,

			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-0-0",
					"mortal-gpu-0-1",
					"mortal-gpu-0-2",
					"mortal-gpu-0-3",
					"mortal-gpu-0-4",
				},
				[]string{"uuid-gpu-0"},
			),
			expectedLeftAllocations: 2,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := greedyAllocWholes(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func (ts *TestAllocateWholeGPUs) TestAllocateAll() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
				},
			),
			allocationRequest: 4,

			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-2-0",
					"mortal-gpu-2-1",
					"mortal-gpu-2-2",
					"mortal-gpu-2-3",
				},
				[]string{"uuid-gpu-2"},
			),
			expectedLeftAllocations: 0,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := greedyAllocWholes(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func (ts *TestAllocateWholeGPUs) TestContainsNills() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
					nil, // nil value in the list.
				},
			),

			allocationRequest: 4,

			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-2-0",
					"mortal-gpu-2-1",
					"mortal-gpu-2-2",
					"mortal-gpu-2-3",
				},
				[]string{"uuid-gpu-2"},
			),
			expectedLeftAllocations: 0,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := greedyAllocWholes(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func TestRunAllocateWholeGPUs(t *testing.T) {
	t.Parallel()
	suite.Run(t, &TestAllocateWholeGPUs{})
}

type TestAllocateGPUFractions struct {
	suite.Suite
}

func (ts *TestAllocateGPUFractions) TestAllocateGPUFractionsSuccess() {
	type args struct {
		candidates        types.GPUsSet
		allocationRequest int
	}

	tests := []*struct {
		name string
		args args

		expectedLeftAllocations int
		expectedResult          *types.AllocationResult
	}{
		{
			name: "Allocate GPU mortal-gpu-1",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
								"mortal-gpu-0-2",
								"mortal-gpu-0-3",
								"mortal-gpu-0-4",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 2,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 4,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-2-0",
								"mortal-gpu-2-1",
								"mortal-gpu-2-2",
								"mortal-gpu-2-3",
							}...,
						),
					},
				),
				allocationRequest: 1,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-1-0"},
				[]string{"uuid-gpu-1"},
			),
		},

		{
			name: "Allocate GPU mortal-gpu-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
								"mortal-gpu-0-2",
								"mortal-gpu-0-3",
								"mortal-gpu-0-4",
							}...,
						),

						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
								"mortal-gpu-1-3",
								"mortal-gpu-1-4",
								"mortal-gpu-1-5",
							}...,
						),

						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 2,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 4,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-2-0",
								"mortal-gpu-2-1",
								"mortal-gpu-2-2",
								"mortal-gpu-2-3",
								"mortal-gpu-2-4",
								"mortal-gpu-2-5",
							}...,
						),
					},
				),

				allocationRequest: 2,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-0-0", "mortal-gpu-0-1"},
				[]string{"uuid-gpu-0"},
			),
		},

		{
			name: "Allocate GPU mortal-gpu-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(

							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
							}...,
						),
					},
				),

				allocationRequest: 1,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-1-0"},
				[]string{"uuid-gpu-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result := types.NewAllocationResult()

		got, err := greedyAllocFractions(logger.Named(testCase.name),
			&testCase.args.candidates, testCase.args.allocationRequest, result)

		ts.Require().NoError(err)

		ts.Equal(testCase.expectedLeftAllocations, got, "Left allocations doesn't match")
		ts.Equal(testCase.expectedResult, result, "Unexpected result of allocation")
	}
}

func (ts *TestAllocateGPUFractions) TestAllocateGPUFractionsSuccessContainsNills() {
	type args struct {
		candidates        types.GPUsSet
		allocationRequest int
	}

	tests := []*struct {
		name string
		args args

		expectedLeftAllocations int
		expectedResult          *types.AllocationResult
	}{
		{
			name: "Allocate GPU mortal-gpu-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
							}...,
						),
					},
				),
				// },
				allocationRequest: 1,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-1-0"},
				[]string{"uuid-gpu-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result := types.NewAllocationResult()

		got, err := greedyAllocFractions(logger.Named(testCase.name),
			&testCase.args.candidates, testCase.args.allocationRequest, result)

		ts.Require().NoError(err)

		ts.Equal(testCase.expectedLeftAllocations, got, "Left allocations doesn't match")
		ts.Equal(testCase.expectedResult, result, "Unexpected result of allocation")
	}
}

func (ts *TestAllocateGPUFractions) TestAllocateGPUFractionsFailOverflow() {
	type args struct {
		candidates        types.GPUsSet
		allocationRequest int
	}

	tests := []*struct {
		name string
		args args

		expectedLeftAllocations int
		expectedResult          *types.AllocationResult
	}{
		{
			name: "Allocate GPU mortal-gpu-1",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
								"mortal-gpu-0-2",
								"mortal-gpu-0-3",
								"mortal-gpu-0-4",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 2,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 4,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-2-0",
								"mortal-gpu-2-1",
								"mortal-gpu-2-2",
								"mortal-gpu-2-3",
							}...,
						),
					},
				),
				allocationRequest: 4,
			},
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result := types.NewAllocationResult()

		_, err := greedyAllocFractions(logger.Named(testCase.name),
			&testCase.args.candidates, testCase.args.allocationRequest, result)

		ts.Require().Error(err)
		ts.ErrorIs(err, types.ErrFractionsAllocatorOverflow)
	}
}

func TestRunAllocateGPUFractions(t *testing.T) {
	t.Parallel()
	suite.Run(t, &TestAllocateGPUFractions{})
}

type TestAllocateAllSuite struct {
	suite.Suite
}

func (ts *TestAllocateAllSuite) TestAllocateWholeGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 5,
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
				},
				[]string{"gpu-uuid-1"},
			),
		},
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 5,
				availableMortalGPUs: []string{
					// Mix MortalGPUs to test that order does not matter.
					// All the 5 shares of gpu-uuid-1 are available.
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					// but their MortalGPU ids are not 0-4
					// as the allocator should choose the correct
					// GPU but does not care about its
					// MortalGPU indexes, i.e. uses the first from the list.
					// In this case, it should be as below
					// because if you take a look up at
					// availableMortalGPUs, you will see
					// that the MortalGPUs of gpu-uuid-1
					// are in this order in the availableMortalGPUs.
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
				},
				[]string{"gpu-uuid-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(false)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateOneAndFractionsFromUnusedGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPUAndAFraction",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6, // More than gpu-uuid-1.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					"mortalgpu-meta-0-0-gpu-uuid-0",
				},
				[]string{"gpu-uuid-1", "gpu-uuid-0"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(false)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateTwoAndFractionsFromUnusedGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateTwoGPUsAndAFraction",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-2",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 7,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 18, // More than any 2 GPUs together.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
					// All 7 shares of gpu-uuid-2 are available.
					"mortalgpu-meta-2-0-gpu-uuid-2",
					"mortalgpu-meta-2-1-gpu-uuid-2",
					"mortalgpu-meta-2-2-gpu-uuid-2",
					"mortalgpu-meta-2-3-gpu-uuid-2",
					"mortalgpu-meta-2-4-gpu-uuid-2",
					"mortalgpu-meta-2-5-gpu-uuid-2",
					"mortalgpu-meta-2-6-gpu-uuid-2",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",

					"mortalgpu-meta-2-0-gpu-uuid-2",
					"mortalgpu-meta-2-1-gpu-uuid-2",
					"mortalgpu-meta-2-2-gpu-uuid-2",
					"mortalgpu-meta-2-3-gpu-uuid-2",
					"mortalgpu-meta-2-4-gpu-uuid-2",
					"mortalgpu-meta-2-5-gpu-uuid-2",
					"mortalgpu-meta-2-6-gpu-uuid-2",

					"mortalgpu-meta-1-0-gpu-uuid-1",
				},
				[]string{"gpu-uuid-0", "gpu-uuid-2", "gpu-uuid-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(false)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateSomewhatAllocatedGPUsUntilFull() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 15,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6, // More than gpu-uuid-1.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					"mortalgpu-meta-0-0-gpu-uuid-0",
				},
				[]string{"gpu-uuid-1", "gpu-uuid-0"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(false)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func TestRunTestAllocateAll(t *testing.T) {
	suite.Run(t, &TestAllocateAllSuite{})
}

type TestFailNoSplitSuite struct {
	suite.Suite
}

func (ts *TestFailNoSplitSuite) TestFail() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args
	}{
		{
			name: "FailMixedGPUsState",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 11, // More than any GPU.
				availableMortalGPUs: []string{
					// 5/8 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
		},

		{
			name: "FailAllSomewhatAllocated",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 6,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 9, // More than any GPU.
				availableMortalGPUs: []string{
					// All the 5/8 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 8/10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
				},
			},
		},

		{
			name: "FailAllFree",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 9, // More than any GPU.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 8 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
				},
			},
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().Error(err)
		ts.Require().Nil(result)
	}
}

func (ts *TestFailNoSplitSuite) TestSuccess() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name           string
		args           args
		expectedResult *types.AllocationResult
	}{
		{
			name: "MixedGPUsState",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-1",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-0",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 8,
				availableMortalGPUs: []string{
					// 5/8 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-0-0-gpu-uuid-1",
					"mortalgpu-meta-0-1-gpu-uuid-1",
					"mortalgpu-meta-0-2-gpu-uuid-1",
					"mortalgpu-meta-0-3-gpu-uuid-1",
					"mortalgpu-meta-0-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-1-0-gpu-uuid-0",
					"mortalgpu-meta-1-1-gpu-uuid-0",
					"mortalgpu-meta-1-2-gpu-uuid-0",
					"mortalgpu-meta-1-3-gpu-uuid-0",
					"mortalgpu-meta-1-4-gpu-uuid-0",
					"mortalgpu-meta-1-5-gpu-uuid-0",
					"mortalgpu-meta-1-6-gpu-uuid-0",
					"mortalgpu-meta-1-7-gpu-uuid-0",
					"mortalgpu-meta-1-8-gpu-uuid-0",
					"mortalgpu-meta-1-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData([]string{
				"mortalgpu-meta-1-0-gpu-uuid-0",
				"mortalgpu-meta-1-1-gpu-uuid-0",
				"mortalgpu-meta-1-2-gpu-uuid-0",
				"mortalgpu-meta-1-3-gpu-uuid-0",
				"mortalgpu-meta-1-4-gpu-uuid-0",
				"mortalgpu-meta-1-5-gpu-uuid-0",
				"mortalgpu-meta-1-6-gpu-uuid-0",
				"mortalgpu-meta-1-7-gpu-uuid-0",
			}, []string{"gpu-uuid-0"}),
		},

		{
			name: "AllSomewhatAllocated",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 6,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 7,
				availableMortalGPUs: []string{
					// All the 5/8 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 8/10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData([]string{
				"mortalgpu-meta-0-0-gpu-uuid-0",
				"mortalgpu-meta-0-1-gpu-uuid-0",
				"mortalgpu-meta-0-2-gpu-uuid-0",
				"mortalgpu-meta-0-3-gpu-uuid-0",
				"mortalgpu-meta-0-4-gpu-uuid-0",
				"mortalgpu-meta-0-5-gpu-uuid-0",
				"mortalgpu-meta-0-6-gpu-uuid-0",
			}, []string{"gpu-uuid-0"}),
		},

		{
			name: "AllFree",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 8,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 7,
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					// All the 8 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					"mortalgpu-meta-1-5-gpu-uuid-1",
					"mortalgpu-meta-1-6-gpu-uuid-1",
					"mortalgpu-meta-1-7-gpu-uuid-1",
				},
			},
			expectedResult: types.NewAllocationResultWithData([]string{
				"mortalgpu-meta-1-0-gpu-uuid-1",
				"mortalgpu-meta-1-1-gpu-uuid-1",
				"mortalgpu-meta-1-2-gpu-uuid-1",
				"mortalgpu-meta-1-3-gpu-uuid-1",
				"mortalgpu-meta-1-4-gpu-uuid-1",
				"mortalgpu-meta-1-5-gpu-uuid-1",
				"mortalgpu-meta-1-6-gpu-uuid-1",
			}, []string{"gpu-uuid-1"}),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)
		ts.Equal(testCase.expectedResult, result)
	}
}

func TestRunTestFailNoSplitSuite(t *testing.T) {
	suite.Run(t, &TestFailNoSplitSuite{})
}
