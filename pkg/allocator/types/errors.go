package types

import "errors"

// ErrFractionsAllocatorOverflow a fraction allocator received a request which is larger
// than at least one of candidates' free shares.
var ErrFractionsAllocatorOverflow = errors.New("fractions allocator was given a request which is overflowing")

// ErrAllocation can not fulfill an allocation.
var ErrAllocation = errors.New("can not allocate GPUs for the request")
