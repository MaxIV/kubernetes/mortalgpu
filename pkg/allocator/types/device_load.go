package types

import (
	"encoding/json"
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// DeviceLoad - available MortalGPUs (GPU fractions).
type DeviceLoad struct {
	// sharedDevice physical device information.
	sharedDevice *sharecfg.SharedDevice
	// mortalGPUs - list of available (not allocated) GPU fractions.
	mortalGPUs []string
}

// NewDeviceLoad creates a new DeviceLoad with a copy of the provided data.
func NewDeviceLoad(sharedDevice *sharecfg.SharedDevice, mortalGPUs ...string) *DeviceLoad {
	res := DeviceLoad{
		mortalGPUs:   make([]string, 0, len(mortalGPUs)),
		sharedDevice: sharedDevice.Copy(),
	}

	res.mortalGPUs = append(res.mortalGPUs, mortalGPUs...)

	return &res
}

// GetFreeShares returns free shares of this DeviceLoad.
func (l *DeviceLoad) GetFreeShares() int {
	return len(l.mortalGPUs)
}

// GetTotalShares returns total shares of this DeviceLoad.
func (l *DeviceLoad) GetTotalShares() int {
	return int(l.sharedDevice.Shares.SharesCount)
}

// GetSharedDeviceInfo returns pointer to a shared device.
func (l *DeviceLoad) GetSharedDeviceInfo() *sharecfg.SharedDevice {
	return l.sharedDevice
}

// MarshalLogObject implements zap.ObjectLogMarshaler.
func (l *DeviceLoad) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	if err := enc.AddObject(lkSharedDevice, l.sharedDevice); err != nil {
		return fmt.Errorf("can not Marshal sharedDevice field: %w", err)
	}

	zap.Strings(lkMortalGpus, l.mortalGPUs).AddTo(enc)

	return nil
}

var _ json.Marshaler = (*DeviceLoad)(nil)

// MarshalJSON implements json.Marshaler.
func (l *DeviceLoad) MarshalJSON() ([]byte, error) {
	type tmp struct {
		SharedDevice *sharecfg.SharedDevice `json:"shared_device,omitempty"`
		MortalGPUs   []string               `json:"mortal_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
	}

	return json.Marshal(&tmp{ //nolint:musttag // I feel that musttag has a false positive here.
		SharedDevice: l.sharedDevice,
		MortalGPUs:   l.mortalGPUs,
	})
}

var _ json.Unmarshaler = (*DeviceLoad)(nil)

func (l *DeviceLoad) UnmarshalJSON(data []byte) error {
	type tmp struct {
		SharedDevice *sharecfg.SharedDevice `json:"shared_device,omitempty"`
		MortalGPUs   []string               `json:"mortal_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
	}

	parsed := &tmp{}

	if err := json.Unmarshal(data, parsed); err != nil { //nolint:musttag // I feel that musttag has a false positive here.
		return err
	}

	l.mortalGPUs = parsed.MortalGPUs
	l.sharedDevice = parsed.SharedDevice.Copy()

	return nil
}
