package types

// Just a dictionary of keys for logging.
// Starts from "lk" like "logging key".
const (
	lkAllocationRequest = "allocation_request"
	lkLeftToAllocate    = "left_to_allocate"

	lkAllocationResult = "allocation_result"

	lkMortalGpus = "mortal_gpus"

	lkPhysicalGPUUUIDs = "physical_gpu_uuids"
	lkPhysicalGPUUUID  = "physical_gpu_uuid"

	lkSharedDevice = "shared_device"

	lkCandidateGPUs     = "gpu_candidates"
	lkCandidateGPUIndex = "gpu_candidate_index"

	lkGPUTotalShares = "gpu_total_shares"
	lkGPUFreeShares  = "gpu_free_shares"

	lkSharedDevices = "shared_devices"

	lkMortalGpu = "mortal_gpu"
)
