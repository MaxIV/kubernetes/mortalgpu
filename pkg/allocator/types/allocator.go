package types

import (
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// AllocationRequest state to be passed to an Allocator.Allocate call.
type AllocationRequest struct {
	// PhysicalGPUs physical GPUs sharing configuration and state.
	PhysicalGPUs []*sharecfg.SharedDevice
	// AvailableMortalGPUs available devices for allocation.
	AvailableMortalGPUs []string
	// Size number of requested devices.
	Size int
}

// Allocator interface for a pluggable allocator.
type Allocator interface {
	// Allocate performs allocation.
	Allocate(logger *zap.Logger, state *AllocationRequest) (*AllocationResult, error)
}
