package types

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type TestGPUsSetIter struct {
	suite.Suite
}

func (ts *TestGPUsSetIter) TestGoodRun() {
	type testCase struct {
		gs GPUsSet

		expectedIterCount  int
		expectedMortalGPUs []string
		expectedUUIDs      []string
		expectedIndexes    []int
	}

	tests := []*testCase{
		{
			gs: NewGPUsSetWithData([]*DeviceLoad{
				{
					mortalGPUs: []string{"m1", "m2", "m3"},
					sharedDevice: &sharecfg.SharedDevice{
						UUID:   "uuid1",
						Index:  1,
						Shares: sharecfg.SharesInfo{},
					},
				},
				{
					mortalGPUs: []string{"m4", "m5", "m6"},
					sharedDevice: &sharecfg.SharedDevice{
						UUID:   "uuid2",
						Index:  2,
						Shares: sharecfg.SharesInfo{},
					},
				},
				{
					mortalGPUs: []string{"m7", "m8", "m9"},
					sharedDevice: &sharecfg.SharedDevice{
						UUID:   "uuid3",
						Index:  3,
						Shares: sharecfg.SharesInfo{},
					},
				},
			}),
			expectedIterCount:  3,
			expectedMortalGPUs: []string{"m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9"},
			expectedUUIDs:      []string{"uuid1", "uuid2", "uuid3"},
			expectedIndexes:    []int{1, 2, 3},
		},
	}

	for _, test := range tests {
		iterator := test.gs.Iterator()

		var (
			iterCount  int
			mortalGPUs []string
			uuids      []string
			indexes    []int
		)

		for iterator.Next() {
			idx, dl := iterator.Item()

			ts.Require().Equal(iterCount, idx)

			iterCount++

			mortalGPUs = append(mortalGPUs, dl.mortalGPUs...)
			uuids = append(uuids, dl.sharedDevice.UUID)
			indexes = append(indexes, dl.sharedDevice.Index)
		}

		ts.Equal(test.expectedIterCount, iterCount)
		ts.Equal(test.expectedIndexes, indexes)
		ts.Equal(test.expectedMortalGPUs, mortalGPUs)
		ts.Equal(test.expectedUUIDs, uuids)
	}
}

func TestRunTestGPUsSetIter(t *testing.T) {
	suite.Run(t, &TestGPUsSetIter{})
}
