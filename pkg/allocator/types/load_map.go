package types

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"sort"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	mortalnames "gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mortal-names"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

// Placeholder to show that some "shares" value is not computed.
const gPUsSetUnassignedSize = -1

var (
	// ErrIndexOutOfRange given GPU index is out of range, can not
	// perform allocation.
	ErrIndexOutOfRange = errors.New("index is our of range")
	// ErrGPUHadBeenAllocated given GPU had been allocated earlier.
	ErrGPUHadBeenAllocated = errors.New("this GPU had been allocated before")
	// ErrAllocationRequestTooBig allocation request is larger than available MortalGPUs
	// on a physical GPU.
	// This should never happen, if it does, it is a bug.
	ErrAllocationRequestTooBig = errors.New("given allocation request is larger than a GPU")
)

// GPUsSet set of allocatable GPUs.
type GPUsSet struct {
	// gpus gpus' loads.
	gpus []*DeviceLoad

	// smallestFreeShares - smallest allocatable GPU,
	// least number of available MortalGPUs.
	smallestFreeShares int
	// largestFreeShares - largest allocatable GPU,
	// greatest number of available MortalGPUs.
	largestFreeShares int
}

// NewGPUsSet creates a new empty GPUsSet.
func NewGPUsSet() GPUsSet {
	return GPUsSet{
		smallestFreeShares: gPUsSetUnassignedSize,
		largestFreeShares:  gPUsSetUnassignedSize,
		gpus:               make([]*DeviceLoad, 0),
	}
}

// NewGPUsSetWithData creates a new GPUsSet with pre-filled data.
func NewGPUsSetWithData(devs []*DeviceLoad) GPUsSet {
	res := &GPUsSet{
		smallestFreeShares: gPUsSetUnassignedSize,
		largestFreeShares:  gPUsSetUnassignedSize,
		gpus:               make([]*DeviceLoad, 0, len(devs)),
	}

	for _, d := range devs {
		res.addDevice(d)
	}

	return *res
}

// AllocateFromGPU gets requested number of MortalGPUs from the
// physical GPU pointed by gpuIdx.
// The returned slice of MortalGPUs shares the same memory as .gpus
// but not available from the gpuSet.
// Modifies the GPUsSet, so needs a special attention when the same
// GPUsSet is used in iterator, i.e. the AllocateFromGPU must be
// the last operation in the iterator loop step.
func (gs *GPUsSet) AllocateFromGPU(gpuIdx, allocRequest int) (gpuUUID string, mortalGPUs []string, err error) {
	if gpuIdx >= len(gs.gpus) {
		return "", nil, fmt.Errorf("%w: index=%d, total gpus=%d", ErrIndexOutOfRange, gpuIdx, len(gs.gpus))
	}

	gpu := gs.gpus[gpuIdx]
	if gpu == nil {
		return "", nil, fmt.Errorf("%w: GPU at index %d", ErrGPUHadBeenAllocated, gpuIdx)
	}

	freeShares := gpu.GetFreeShares()

	if allocRequest > freeShares {
		return "", nil, fmt.Errorf("%w: request size=%d, gpu free shares=%d",
			ErrAllocationRequestTooBig, allocRequest, freeShares)
	}

	gpuUUID = gpu.sharedDevice.UUID

	if freeShares == allocRequest {
		mortalGPUs = gpu.mortalGPUs
		gs.gpus[gpuIdx] = nil
	} else {
		mortalGPUs = gpu.mortalGPUs[:allocRequest]
		// Remove allocated.
		gpu.mortalGPUs = gpu.mortalGPUs[allocRequest:]
	}

	// Recompute smallestFreeShares and largestFreeShares if needed.
	if freeShares == gs.largestFreeShares || freeShares == gs.smallestFreeShares {
		var (
			tLarge = gPUsSetUnassignedSize
			tSmall = gPUsSetUnassignedSize
		)

		for _, g := range gs.gpus {
			if g == nil {
				continue
			}

			gpuFreeShares := g.GetFreeShares()

			if tLarge == gPUsSetUnassignedSize {
				tLarge = gpuFreeShares
			}

			if tSmall == gPUsSetUnassignedSize {
				tSmall = gpuFreeShares
			}

			if tSmall > gpuFreeShares {
				tSmall = gpuFreeShares
			}

			if tLarge < gpuFreeShares {
				tLarge = gpuFreeShares
			}
		}

		gs.smallestFreeShares = tSmall
		gs.largestFreeShares = tLarge
	}

	return gpuUUID, mortalGPUs, nil
}

// IsAllocatable checks if this set has any GPUs which can be allocated.
func (gs *GPUsSet) IsAllocatable() bool {
	return gs.largestFreeShares > 0
}

// GetSmallestFreeShares returns a number representing smallest free shares
// value across all GPUs in the GPUsSet.
func (gs *GPUsSet) GetSmallestFreeShares() int {
	return gs.smallestFreeShares
}

// GetSmallestFreeShares returns a number representing largest free shares
// value across all GPUs in the GPUsSet.
func (gs *GPUsSet) GetLargestFreeShares() int {
	return gs.largestFreeShares
}

func (gs *GPUsSet) addDevice(dl *DeviceLoad) {
	// Skip nil values.
	if dl == nil {
		return
	}

	freeShares := dl.GetFreeShares()

	gs.gpus = append(gs.gpus, dl)

	if gs.smallestFreeShares == gPUsSetUnassignedSize {
		gs.smallestFreeShares = freeShares
	}

	if gs.largestFreeShares == gPUsSetUnassignedSize {
		gs.largestFreeShares = freeShares
	}

	if gs.largestFreeShares < freeShares {
		gs.largestFreeShares = freeShares
	}

	if gs.smallestFreeShares > freeShares {
		gs.smallestFreeShares = freeShares
	}
}

// SortDecreaseFreeShares sorts GPUs based on the decreasing of the number of
// free shares, so that the lower index the more free shares a GPU has.
func (gs *GPUsSet) SortDecreaseFreeShares() {
	compareF := func(idxI, idxJ int) bool {
		// Special care for nils => last in the sorted slice.
		if gs.gpus[idxI] == nil {
			return false
		}

		if gs.gpus[idxJ] == nil {
			return false
		}

		return gs.gpus[idxI].GetFreeShares() > gs.gpus[idxJ].GetFreeShares()
	}

	// Sort by decrease of free shares to "eat" starting from the largest.
	if !sort.SliceIsSorted(gs.gpus, compareF) {
		sort.Slice(gs.gpus, compareF)
	}
}

// SortIncreaseUtilizationRate sorts by increase of allocated shares to total shares rate.
func (gs *GPUsSet) SortIncreaseUtilizationRate() {
	// This function might be not very optimal as it has a lot of calls
	// and float64 operations but assuming that the sort is perfomed
	// for physical GPUs of one physical server, there should not be more
	// than, probably, 16-32 elements (GPUs attached to a server) in the slice anyway.
	compareF := func(idxI, idxJ int) bool {
		gpuI := gs.gpus[idxI]
		gpuJ := gs.gpus[idxJ]

		// Special care for nils => last in the sorted slice.
		if gpuI == nil {
			return false
		}

		if gpuJ == nil {
			return false
		}

		pIFree := gpuI.GetFreeShares()
		pJFree := gpuJ.GetFreeShares()
		pITotal := gpuI.GetTotalShares()
		pJTotal := gpuJ.GetTotalShares()

		pIUsed := float64(pITotal-pIFree) / float64(pITotal)
		pJUsed := float64(pJTotal-pJFree) / float64(pJTotal)

		// If the used rate is the same, then
		// we assume that the GPU with larger total
		// capacity has lower utilization.
		if pIUsed == pJUsed {
			return pITotal > pJTotal
		}

		// Floating point comparison within 0.xx % toleration.
		const epsilon = 0.0001

		diff := math.Abs(pIUsed - pJUsed)
		if diff < epsilon {
			// Utilizations are equal within the tolerance.
			return pITotal > pJTotal
		} else { //nolint:revive // if-else provides better explanation of intent.
			// Otherwise, use direct comparison between the utilization rates.
			return pIUsed < pJUsed
		}
	}

	// Sort by decrease of free shares to "eat" starting from the largest.
	if !sort.SliceIsSorted(gs.gpus, compareF) {
		sort.Slice(gs.gpus, compareF)
	}
}

// SortIncreaseCapacity sorts by increasing of total number of MortalGPUs.
func (gs *GPUsSet) SortIncreaseCapacity() {
	compareF := func(idxI, idxJ int) bool {
		return gs.gpus[idxI].GetTotalShares() < gs.gpus[idxJ].GetTotalShares()
	}

	sort.Slice(gs.gpus, compareF)
}

// Iterator creates an iterator over the GPUsSet data.
func (gs *GPUsSet) Iterator() *GPUsSetIter {
	return newGPUsSetIter(gs)
}

// MarshalLogObject implements zap.MarshalLogObject.
func (gs *GPUsSet) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddInt("smallest_free", gs.smallestFreeShares)
	enc.AddInt("largest_free", gs.largestFreeShares)
	zap.Objects("physical_gpus", gs.gpus).AddTo(enc)

	return nil
}

var _ json.Marshaler = (*GPUsSet)(nil)

// MarshalJSON implements JSON Marshaler.
func (gs *GPUsSet) MarshalJSON() ([]byte, error) {
	type tGpusSet struct {
		GPUs []*DeviceLoad `json:"gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
		// smallestFreeShares - smallest allocatable GPU,
		// least number of available MortalGPUs.
		SmallestFreeShares int `json:"smallest_free_shares,omitempty"`
		// largestFreeShares - largest allocatable GPU,
		// greatest number of available MortalGPUs.
		LargestFreeShares int `json:"largest_free_shares,omitempty"`
	}

	tmp := tGpusSet{
		GPUs:               gs.gpus,
		SmallestFreeShares: gs.smallestFreeShares,
		LargestFreeShares:  gs.largestFreeShares,
	}

	return json.Marshal(&tmp)
}

var _ json.Unmarshaler = (*GPUsSet)(nil)

// UnmarshalJSON implements json.Unmarshaler.
func (gs *GPUsSet) UnmarshalJSON(data []byte) error {
	type tGpusSet struct {
		GPUs []*DeviceLoad `json:"gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
		// smallestFreeShares - smallest allocatable GPU,
		// least number of available MortalGPUs.
		SmallestFreeShares int `json:"smallest_free_shares,omitempty"`
		// largestFreeShares - largest allocatable GPU,
		// greatest number of available MortalGPUs.
		LargestFreeShares int `json:"largest_free_shares,omitempty"`
	}

	tmp := tGpusSet{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	gs.gpus = tmp.GPUs
	gs.largestFreeShares = tmp.LargestFreeShares
	gs.smallestFreeShares = tmp.SmallestFreeShares

	return nil
}

// GPUsSetIter iterates over GPUsSet DeviceLoad_s.
type GPUsSetIter struct {
	gs *GPUsSet

	idx int
}

func newGPUsSetIter(gs *GPUsSet) *GPUsSetIter {
	return &GPUsSetIter{
		gs:  gs,
		idx: -1, //nolint:mnd // -1 is used so that the first Next() call makes it "0".
	}
}

// Next - next DeviceLoad, returns false on stop.
func (gsi *GPUsSetIter) Next() bool {
	gsi.idx++

	// Stop iteration.
	if gsi.idx == len(gsi.gs.gpus) {
		return false
	}

	// Skip nil values, i.e. completely allocated GPUs.
	if gsi.gs.gpus[gsi.idx] != nil {
		return true
	}

	// Try next and further elements.
	for i := gsi.idx + 1; i < len(gsi.gs.gpus); i++ {
		if gsi.gs.gpus[i] != nil {
			gsi.idx = i

			return true
		}
	}

	return false
}

// Item returns current element.
func (gsi *GPUsSetIter) Item() (int, *DeviceLoad) {
	return gsi.idx, gsi.gs.gpus[gsi.idx]
}

// LoadMap contains current status of all available GPUs.
type LoadMap struct {
	// unusedGPUs - completely unused (without any allocation) GPUs.
	unusedGPUs GPUsSet
	// allocatableGPUs - GPUs with some portions of them allocated.
	allocatableGPUs GPUsSet
}

var _ json.Marshaler = (*LoadMap)(nil)

// MarshalJSON implements json.Marshaler.
func (lm *LoadMap) MarshalJSON() ([]byte, error) {
	type tmp struct {
		UnusedGPUs      GPUsSet `json:"unused_gpus,omitempty"`      //nolint:tagliatelle // "gpus" is visually better.
		AllocatableGPUs GPUsSet `json:"allocatable_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
	}

	v := tmp{
		UnusedGPUs:      lm.unusedGPUs,
		AllocatableGPUs: lm.allocatableGPUs,
	}

	return json.Marshal(&v)
}

var _ json.Unmarshaler = (*LoadMap)(nil)

// UnmarshalJSON implements json.Unmarshaler.
func (lm *LoadMap) UnmarshalJSON(data []byte) error {
	type tmp struct {
		UnusedGPUs      GPUsSet `json:"unused_gpus,omitempty"`      //nolint:tagliatelle // "gpus" is visually better.
		AllocatableGPUs GPUsSet `json:"allocatable_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually better.
	}

	parsed := tmp{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		return err
	}

	lm.allocatableGPUs = parsed.AllocatableGPUs
	lm.unusedGPUs = parsed.UnusedGPUs

	return nil
}

// MarshalLogObject implements zap.MarshalLogObject.
func (lm *LoadMap) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	if err := enc.AddObject("unused_gpus", &lm.unusedGPUs); err != nil {
		return fmt.Errorf("can not Marshal unused_gpus: %w", err)
	}

	if err := enc.AddObject("allocatable_gpus", &lm.allocatableGPUs); err != nil {
		return fmt.Errorf("can not Marshal allocatable_gpus: %w", err)
	}

	return nil
}

// NewLoadMap builds a map of real device id to meta device id.
func NewLoadMap(log *zap.Logger, availableDevIDs []string, sharedDevices []*sharecfg.SharedDevice) (*LoadMap, error) {
	logger := log.Named("initLoadMaps")

	// Prepare GPU lookup hash.
	sharedDevicesM := make(map[string]*sharecfg.SharedDevice, len(sharedDevices))
	for _, d := range sharedDevices {
		sharedDevicesM[d.UUID] = d
	}

	if l := logger.Check(zap.DebugLevel, "initLoadMap called"); l != nil {
		l.Write(
			zap.Strings(lkMortalGpus, availableDevIDs),
			zap.Objects(lkSharedDevices, sharedDevices),
		)
	}

	// Stores state for all the shared physical GPUs.
	// Key is a physical GPU index as given by NVML.
	sharedDevicesLoadMap := make(map[int]*DeviceLoad, len(sharedDevices))

	for _, availableDevID := range availableDevIDs {
		mortalGPUDevice, err := mortalnames.ParseDeviceInfo(availableDevID)
		if err != nil {
			logger.Error("Can not parse MortalGPU device ID",
				zap.String("mortal_gpu_device_id", availableDevID),
				zap.Error(err))

			return nil, fmt.Errorf("initLoadMaps: %w", err)
		}

		// Create new records for physical GPUs if not present.
		physDev, ok := sharedDevicesLoadMap[mortalGPUDevice.GPUIndex]
		if !ok {
			gpu, ok := sharedDevicesM[mortalGPUDevice.GPUUUID]
			if !ok {
				logger.Warn(
					"Can not find a shared GPU in SharedDevices by UUID, ignoring the MortalGPU",
					zap.String(lkMortalGpu, availableDevID),
					zap.Objects(lkSharedDevices, sharedDevices),
				)

				// Skip the loop step.
				continue
			}

			physDev = &DeviceLoad{
				mortalGPUs:   nil,
				sharedDevice: gpu,
			}

			sharedDevicesLoadMap[mortalGPUDevice.GPUIndex] = physDev
		}

		physDev.mortalGPUs = append(physDev.mortalGPUs, availableDevID)
	}

	// Split the populated GPUs info to completely empty GPUs and somewhat used GPUs.
	result := LoadMap{
		unusedGPUs:      NewGPUsSet(),
		allocatableGPUs: NewGPUsSet(),
	}

	for _, info := range sharedDevicesLoadMap {
		freeShares := info.GetFreeShares()

		// No need to have a GPU without allocatable resources.
		if freeShares == 0 {
			continue
		}

		// Populate completely unused GPUs.
		if freeShares == info.GetTotalShares() {
			result.unusedGPUs.addDevice(info)
		} else {
			// Somewhat used GPUs.
			result.allocatableGPUs.addDevice(info)
		}
	}

	if l := logger.Check(zap.DebugLevel, "Computed load maps"); l != nil {
		l.Write(zap.Object("load_maps", &result))
	}

	return &result, nil
}

// GetUnusedGPUs returns pointer to unused GPUs GPUsSet from
// the LoadMap.
// Allows modification of the LoadMap's data.
func (lm *LoadMap) GetUnusedGPUs() *GPUsSet {
	return &lm.unusedGPUs
}

// GetAllocatableGPUs returns pointer to allocatable (have free and allocated share)
// GPUs GPUsSet from the LoadMap.
// Allows modification of the LoadMap's data.
func (lm *LoadMap) GetAllocatableGPUs() *GPUsSet {
	return &lm.allocatableGPUs
}
