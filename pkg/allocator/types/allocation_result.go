package types

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// AllocationResult result of MortalGPU allocations.
type AllocationResult struct {
	mortalGPUs          []string
	physicalDeviceUUIDs []string
}

// NewAllocationResult creates a new AllocationResult.
func NewAllocationResult() *AllocationResult {
	return &AllocationResult{
		mortalGPUs:          make([]string, 0),
		physicalDeviceUUIDs: make([]string, 0),
	}
}

// NewAllocationResultWithData creates a new AllocationResult with copy of
// the provided data.
func NewAllocationResultWithData(mortalGPUs, physicalDeviceUUIDs []string) *AllocationResult {
	res := AllocationResult{
		mortalGPUs:          make([]string, 0, len(mortalGPUs)),
		physicalDeviceUUIDs: make([]string, 0, len(physicalDeviceUUIDs)),
	}

	res.mortalGPUs = append(res.mortalGPUs, mortalGPUs...)
	res.physicalDeviceUUIDs = append(res.physicalDeviceUUIDs, physicalDeviceUUIDs...)

	return &res
}

// GetMortalGPUs returns allocated MortalGPU names.
func (al *AllocationResult) GetMortalGPUs() []string {
	return al.mortalGPUs
}

// GetPhysicalGPUsUUIDs returns allocated physical GPU UUIDs.
func (al *AllocationResult) GetPhysicalGPUsUUIDs() []string {
	return al.physicalDeviceUUIDs
}

// AppendMortalGPUs adds MortalGPUs to the result.
func (al *AllocationResult) AppendMortalGPUs(mortalGPUs ...string) {
	al.mortalGPUs = append(al.mortalGPUs, mortalGPUs...)
}

// AppendPhysicalGPUs adds physical GPUs to the result.
func (al *AllocationResult) AppendPhysicalGPUs(gpuUUIDs ...string) {
	al.physicalDeviceUUIDs = append(al.physicalDeviceUUIDs, gpuUUIDs...)
}

// MarshalLogObject implements zap.ObjectLogMarshaler.
func (al *AllocationResult) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	zap.Strings(lkMortalGpus, al.mortalGPUs).AddTo(enc)
	zap.Strings(lkPhysicalGPUUUIDs, al.physicalDeviceUUIDs).AddTo(enc)

	return nil
}
