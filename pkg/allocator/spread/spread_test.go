package spread

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type testCase struct {
	expectedResult *types.AllocationResult

	unusedGPUs types.GPUsSet

	allocationRequest int

	expectedLeftAllocations int
}

type TestAllocateFromUnusedGPUs struct {
	suite.Suite
}

func (ts *TestAllocateFromUnusedGPUs) TestNoAllocations() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		// No fit because no unused GPUs.
		{
			unusedGPUs:        types.NewGPUsSet(),
			allocationRequest: 100,

			expectedResult:          types.NewAllocationResultWithData([]string{}, []string{}),
			expectedLeftAllocations: 100,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := allocateLeastUsed(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func (ts *TestAllocateFromUnusedGPUs) TestContainsNills() {
	logger := zap.Must(zap.NewDevelopment())

	cases := []*testCase{
		{
			unusedGPUs: types.NewGPUsSetWithData(
				[]*types.DeviceLoad{
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-0",
							Index: 0,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 5,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-0-0",
							"mortal-gpu-0-1",
							"mortal-gpu-0-2",
							"mortal-gpu-0-3",
							"mortal-gpu-0-4",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-1",
							Index: 1,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 3,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-1-0",
							"mortal-gpu-1-1",
							"mortal-gpu-1-2",
						}...,
					),
					types.NewDeviceLoad(
						&sharecfg.SharedDevice{
							UUID:  "uuid-gpu-2",
							Index: 2,
							Shares: sharecfg.SharesInfo{
								ShareSize:   100 * 1024 * 1024, // 100MB.
								SharesCount: 4,                 // the same as the metagpus count.
							},
						},
						[]string{
							"mortal-gpu-2-0",
							"mortal-gpu-2-1",
							"mortal-gpu-2-2",
							"mortal-gpu-2-3",
						}...,
					),
					nil, // nil value in the list.
				},
			),

			allocationRequest: 4,

			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-0-0",
					"mortal-gpu-0-1",
					"mortal-gpu-0-2",
					"mortal-gpu-0-3",
				},
				[]string{"uuid-gpu-0"},
			),
			expectedLeftAllocations: 0,
		},
	}

	for tcI, testCase := range cases {
		result := types.NewAllocationResult()

		left, err := allocateLeastUsed(logger, &testCase.unusedGPUs, testCase.allocationRequest, result)

		ts.Require().NoError(err)
		ts.Equalf(testCase.expectedLeftAllocations, left, "Left allocations doesn't match, case %d", tcI)
		ts.Equalf(testCase.expectedResult, result, "Allocation result does not match, case %d", tcI)
	}
}

func TestRunAllocateLeastUsed(t *testing.T) {
	suite.Run(t, &TestAllocateFromUnusedGPUs{})
}

type TestAllocateFromGPUShares struct {
	suite.Suite
}

func (ts *TestAllocateFromGPUShares) TestAllocateFromGPUSharesSuccess() {
	type args struct {
		candidates        types.GPUsSet
		allocationRequest int
	}

	tests := []*struct {
		name string
		args args

		expectedLeftAllocations int
		expectedResult          *types.AllocationResult
	}{
		{
			name: "Allocate GPU mortal-gpu-1-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 10,                // 50% utilization
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
								"mortal-gpu-0-2",
								"mortal-gpu-0-3",
								"mortal-gpu-0-4",
							}...,
						),
						// This GPU should be chosen as the least utilized.
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 10,                // 33.3% utilization
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
								"mortal-gpu-1-3",
								"mortal-gpu-1-4",
								"mortal-gpu-1-5",
								"mortal-gpu-1-6",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 2,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // 80% utilization
								},
							},
							[]string{
								"mortal-gpu-2-0",
							}...,
						),
					},
				),
				allocationRequest: 1,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-1-0"},
				[]string{"uuid-gpu-1"},
			),
		},

		{
			name: "Allocate GPU mortal-gpu-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 6,                 // 5/6 of the GPU is free.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
								"mortal-gpu-0-2",
								"mortal-gpu-0-3",
								"mortal-gpu-0-4",
							}...,
						),

						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 100,               // Almost fully-used.
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
								"mortal-gpu-1-3",
								"mortal-gpu-1-4",
								"mortal-gpu-1-5",
							}...,
						),

						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 2,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 200,               // Also almost fully-used.
								},
							},
							[]string{
								"mortal-gpu-2-0",
								"mortal-gpu-2-1",
								"mortal-gpu-2-2",
								"mortal-gpu-2-3",
								"mortal-gpu-2-4",
								"mortal-gpu-2-5",
							}...,
						),
					},
				),

				allocationRequest: 2,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-0-0", "mortal-gpu-0-1"},
				[]string{"uuid-gpu-0"},
			),
		},

		{
			name: "Allocate GPU GPUs 0 and 3",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(

							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // Almost free, 33.3% used.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 10,                // 66.6% used
								},
							},
							[]string{
								"mortal-gpu-1-0",
								"mortal-gpu-1-1",
								"mortal-gpu-1-2",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-2",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 12,                // 10/12 used
								},
							},
							[]string{
								"mortal-gpu-2-0",
								"mortal-gpu-2-1",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-3",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 12,                // 2/12 used.
								},
							},
							[]string{
								"mortal-gpu-3-0",
								"mortal-gpu-3-1",
								"mortal-gpu-3-2",
								"mortal-gpu-3-3",
								"mortal-gpu-3-4",
								"mortal-gpu-3-5",
								"mortal-gpu-3-6",
								"mortal-gpu-3-7",
								"mortal-gpu-3-8",
								"mortal-gpu-3-9",
							}...,
						),
					},
				),

				allocationRequest: 10,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortal-gpu-0-0",
					"mortal-gpu-0-1",
					"mortal-gpu-3-0",
					"mortal-gpu-3-1",
					"mortal-gpu-3-2",
					"mortal-gpu-3-3",
					"mortal-gpu-3-4",
					"mortal-gpu-3-5",
					"mortal-gpu-3-6",
					"mortal-gpu-3-7",
				},
				[]string{"uuid-gpu-0", "uuid-gpu-3"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result := types.NewAllocationResult()

		got, err := allocateLeastUsed(logger.Named(testCase.name),
			&testCase.args.candidates, testCase.args.allocationRequest, result)

		ts.Require().NoError(err)

		ts.Equal(testCase.expectedLeftAllocations, got, "Left allocations doesn't match")
		ts.Equal(testCase.expectedResult, result, "Unexpected result of allocation")
	}
}

func (ts *TestAllocateFromGPUShares) TestAllocateFromGPUSharesSuccessContainsNills() {
	type args struct {
		candidates        types.GPUsSet
		allocationRequest int
	}

	tests := []*struct {
		name string
		args args

		expectedLeftAllocations int
		expectedResult          *types.AllocationResult
	}{
		{
			name: "Allocate GPU mortal-gpu-0",
			args: args{
				candidates: types.NewGPUsSetWithData(
					[]*types.DeviceLoad{
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-0",
								Index: 0,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 5,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-0-0",
								"mortal-gpu-0-1",
							}...,
						),
						types.NewDeviceLoad(
							&sharecfg.SharedDevice{
								UUID:  "uuid-gpu-1",
								Index: 1,
								Shares: sharecfg.SharesInfo{
									ShareSize:   100 * 1024 * 1024, // 100MB.
									SharesCount: 3,                 // the same as the metagpus count.
								},
							},
							[]string{
								"mortal-gpu-1-0",
							}...,
						),
					},
				),
				// },
				allocationRequest: 1,
			},
			expectedLeftAllocations: 0,
			expectedResult: types.NewAllocationResultWithData(
				[]string{"mortal-gpu-1-0"},
				[]string{"uuid-gpu-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result := types.NewAllocationResult()

		got, err := allocateLeastUsed(logger.Named(testCase.name),
			&testCase.args.candidates, testCase.args.allocationRequest, result)

		ts.Require().NoError(err)

		ts.Equal(testCase.expectedLeftAllocations, got, "Left allocations doesn't match")
		ts.Equal(testCase.expectedResult, result, "Unexpected result of allocation")
	}
}

type TestAllocateAllSuite struct {
	suite.Suite
}

func (ts *TestAllocateAllSuite) TestAllocateWholeGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 5,
				availableMortalGPUs: []string{
					// All the 5/5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10/10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
				},
				[]string{"gpu-uuid-0"},
			),
		},
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6,
				availableMortalGPUs: []string{
					// Mix MortalGPUs to test that order does not matter.
					// All the 5 shares of gpu-uuid-1 are available.
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// 6/10 shares of gpu-uuid-0 should be allocated.
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
				},
				[]string{"gpu-uuid-0"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, false)

		result, err := allocator.Allocate(logger, &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateOneFromUnusedGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPUAndAFraction",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6, // More than gpu-uuid-1.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					// All 5 shares of gpu-uuid-1 should be allocated.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
				},
				[]string{"gpu-uuid-0"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, false)

		result, err := allocator.Allocate(logger, &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateTwoAndFractionsFromUnusedGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateTwoGPUsAndAFraction",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 10,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-2",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 7,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 18, // More than any 2 GPUs together.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All the 10 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
					// All 7 shares of gpu-uuid-2 are available.
					"mortalgpu-meta-2-0-gpu-uuid-2",
					"mortalgpu-meta-2-1-gpu-uuid-2",
					"mortalgpu-meta-2-2-gpu-uuid-2",
					"mortalgpu-meta-2-3-gpu-uuid-2",
					"mortalgpu-meta-2-4-gpu-uuid-2",
					"mortalgpu-meta-2-5-gpu-uuid-2",
					"mortalgpu-meta-2-6-gpu-uuid-2",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",

					"mortalgpu-meta-2-0-gpu-uuid-2",
					"mortalgpu-meta-2-1-gpu-uuid-2",
					"mortalgpu-meta-2-2-gpu-uuid-2",
					"mortalgpu-meta-2-3-gpu-uuid-2",
					"mortalgpu-meta-2-4-gpu-uuid-2",
					"mortalgpu-meta-2-5-gpu-uuid-2",
					"mortalgpu-meta-2-6-gpu-uuid-2",

					"mortalgpu-meta-1-0-gpu-uuid-1",
				},
				[]string{"gpu-uuid-0", "gpu-uuid-2", "gpu-uuid-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, false)

		result, err := allocator.Allocate(logger, &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func (ts *TestAllocateAllSuite) TestAllocateMixedGPUs() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name string
		args args

		expectedResult *types.AllocationResult
	}{
		{
			name: "AllocateOneGPU",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 4, // Less than gpu-uuid-1.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// 10/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
					"mortalgpu-meta-0-6-gpu-uuid-0",
					"mortalgpu-meta-0-7-gpu-uuid-0",
					"mortalgpu-meta-0-8-gpu-uuid-0",
					"mortalgpu-meta-0-9-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
				},
				[]string{"gpu-uuid-1"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, false)

		result, err := allocator.Allocate(logger, &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)

		ts.Equal(testCase.expectedResult, result)
	}
}

func TestRunTestAllocateAll(t *testing.T) {
	suite.Run(t, &TestAllocateAllSuite{})
}

type TestFailMultiGPUsSuite struct {
	suite.Suite
}

func (ts *TestFailMultiGPUsSuite) TestFailAllocation() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		args args
	}{
		// Mixed used and unused GPUs.
		{
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6, // More that any of the free shares of GPUs.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// 3/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
				},
			},
		},
		// All GPUs are somewhat used
		{
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6, // More that any of the free shares of GPUs.
				availableMortalGPUs: []string{
					// 4/5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					// 3/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
				},
			},
		},
		// All GPUs are completely free
		{
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 6,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 7, // More that any of the free shares of GPUs.
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All 6 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
				},
			},
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, true)

		result, err := allocator.Allocate(logger, &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().Error(err)
		ts.Require().Nil(result)
	}
}

func (ts *TestFailMultiGPUsSuite) TestSuccess() {
	type args struct {
		sharedGPUs          []*sharecfg.SharedDevice
		availableMortalGPUs []string
		allocationRequest   int
	}

	tests := []*struct {
		name           string
		args           args
		expectedResult *types.AllocationResult
	}{
		// Mixed used and unused GPUs.
		{
			name: "MixedUsedAndUnusedGPUs",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 5,
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// 3/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
				}, []string{
					"gpu-uuid-1",
				},
			),
		},
		// All GPUs are somewhat used
		{
			name: "AllGPUsAreNotCompletelyUnused-AllocateLargestGPU-ForSmallRequest",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 3,
				availableMortalGPUs: []string{
					// 4/5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					// 3/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
				},
				[]string{"gpu-uuid-1"},
			),
		},
		// All GPUs are somewhat used
		{
			name: "AllGPUsAreNotCompletelyUnused-AllocateLargestGPU-ForLargeRequest",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 12,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 4,
				availableMortalGPUs: []string{
					// 4/5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					// 3/12 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
				},
				[]string{"gpu-uuid-1"},
			),
		},
		// All GPUs are completely free
		{
			name: "AllGPUsAreFree",
			args: args{
				// 2 GPUs with different number of shares
				// with the share size of 100MB.
				sharedGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "gpu-uuid-0",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							SharesCount: 6,
							ShareSize:   100 * sharecfg.MB,
						},
					},
					{
						UUID:  "gpu-uuid-1",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							SharesCount: 5,
							ShareSize:   100 * sharecfg.MB,
						},
					},
				},
				allocationRequest: 6,
				availableMortalGPUs: []string{
					// All the 5 shares of gpu-uuid-1 are available.
					"mortalgpu-meta-1-0-gpu-uuid-1",
					"mortalgpu-meta-1-1-gpu-uuid-1",
					"mortalgpu-meta-1-2-gpu-uuid-1",
					"mortalgpu-meta-1-3-gpu-uuid-1",
					"mortalgpu-meta-1-4-gpu-uuid-1",
					// All 6 shares of gpu-uuid-0 are available.
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
				},
			},
			expectedResult: types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-0-0-gpu-uuid-0",
					"mortalgpu-meta-0-1-gpu-uuid-0",
					"mortalgpu-meta-0-2-gpu-uuid-0",
					"mortalgpu-meta-0-3-gpu-uuid-0",
					"mortalgpu-meta-0-4-gpu-uuid-0",
					"mortalgpu-meta-0-5-gpu-uuid-0",
				},
				[]string{"gpu-uuid-0"},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		allocator := NewDeviceAllocator(true, true)

		result, err := allocator.Allocate(logger.Named(testCase.name), &types.AllocationRequest{
			PhysicalGPUs:        testCase.args.sharedGPUs,
			AvailableMortalGPUs: testCase.args.availableMortalGPUs,
			Size:                testCase.args.allocationRequest,
		})

		ts.Require().NoError(err)
		ts.Require().NotNil(result)
		ts.Require().Equal(testCase.expectedResult, result)
	}
}

func TestRunTestFailMultiGPUSuite(t *testing.T) {
	suite.Run(t, &TestFailMultiGPUsSuite{})
}
