// Allocator which tries to spread workloads to have even load of physical GPUs
// based on proportion of allocated to total MortalGPUs.
package spread

import (
	"fmt"

	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
)

// Just a dictionary of keys for logging.
// Starts from "lk" like "logging key".
const (
	lkAllocationRequest = "allocation_request"
	lkLeftToAllocate    = "left_to_allocate"

	lkCandidateGPUs     = "gpu_candidates"
	lkCandidateGPUIndex = "gpu_candidate_index"

	lkMaxAvailableShares = "gpu_set_max_available_shares"
)

// DeviceAllocator performs allocation of MortalGPUs.
type DeviceAllocator struct {
	allowCollocation bool
	failMultiGPUs    bool
}

var _ types.Allocator = (*DeviceAllocator)(nil) // type check.

// NewDeviceAllocator creates a new DeviceAllocator.
// If allowCollocation is false, then the allocator will never collocate containers
// with other containers.
func NewDeviceAllocator(allowCollocation, failMultiGPUs bool) *DeviceAllocator {
	return &DeviceAllocator{allowCollocation: allowCollocation, failMultiGPUs: failMultiGPUs}
}

// Allocate performs allocation and returns its result.
func (da *DeviceAllocator) Allocate(log *zap.Logger,
	request *types.AllocationRequest,
) (*types.AllocationResult, error) {
	logger := log.Named("Allocate")

	loadMaps, err := types.NewLoadMap(logger, request.AvailableMortalGPUs, request.PhysicalGPUs)
	if err != nil {
		return nil, fmt.Errorf("can not compute current GPUs usage: %w", err)
	}

	var (
		leftToAllocate = request.Size
		result         = types.NewAllocationResult()
	)

	// Try to allocate from unused GPUs.

	// If the allocator cannot allocate
	// all the requests on the same GPU and the
	// request's properties require it to be allocated on the same
	// GPU, then fail allocation as soon as possible.
	var (
		unusedGPUs       = loadMaps.GetUnusedGPUs()
		unusedGPUsLogger = logger.Named("UnusedGPUs")
	)

	if da.failMultiGPUs && unusedGPUs.GetLargestFreeShares() < leftToAllocate {
		unusedGPUsLogger.Error(
			"Unused GPUs: Can not allocate a request on the same physical GPU and multiple GPUs are not allowed",
			zap.Int(lkAllocationRequest, leftToAllocate),
			zap.Int(lkMaxAvailableShares, unusedGPUs.GetLargestFreeShares()))
	} else {
		leftToAllocate, err = allocateLeastUsed(unusedGPUsLogger,
			unusedGPUs, leftToAllocate, result)
		if err != nil {
			return nil, fmt.Errorf("can not allocate whole GPUs from unused GPUs: %w", err)
		}
	}

	// Try to allocate whole GPUs from somewhat-used GPUs.
	var (
		somewhatUsedGPUs       = loadMaps.GetAllocatableGPUs()
		somewhatUsedGPUsLogger = logger.Named("SomewhatUsedGPUs")
	)

	if da.failMultiGPUs && somewhatUsedGPUs.GetLargestFreeShares() < leftToAllocate {
		somewhatUsedGPUsLogger.Error(
			"Allocatable GPUs: Can not allocate a request on the same physical GPU and multiple GPUs are not allowed",
			zap.Int(lkAllocationRequest, leftToAllocate),
			zap.Int(lkMaxAvailableShares, somewhatUsedGPUs.GetLargestFreeShares()))
	} else {
		if da.allowCollocation {
			leftToAllocate, err = allocateLeastUsed(somewhatUsedGPUsLogger,
				somewhatUsedGPUs, leftToAllocate, result)
			if err != nil {
				return nil, fmt.Errorf("can not allocate whole GPUs from somewhat used GPUs: %w", err)
			}
		}
	}

	if leftToAllocate > 0 {
		return nil, fmt.Errorf("%w: left to allocate %d shares", types.ErrAllocation, leftToAllocate)
	}

	return result, nil
}

// allocateLeastUsed tries to allocate from the least used (allocated) GPUs first.
func allocateLeastUsed(log *zap.Logger,
	gpus *types.GPUsSet, allocationRequest int,
	result *types.AllocationResult,
) (int, error) {
	logger := log.Named("allocateLeastUsed")

	if allocationRequest <= 0 {
		logger.Debug(
			"Allocation request is less or equal to zero, no need to allocate, skip",
			zap.Int(lkAllocationRequest, allocationRequest))

		return allocationRequest, nil
	}

	if l := logger.Check(zap.DebugLevel, "Allocation request"); l != nil {
		l.Write(
			zap.Object(lkCandidateGPUs, gpus),
			zap.Int(lkAllocationRequest, allocationRequest),
		)
	}

	leftToAllocate := allocationRequest

	gpus.SortIncreaseUtilizationRate()

	gpusIter := gpus.Iterator()

	for gpusIter.Next() {
		idx, gpu := gpusIter.Item()

		canAllocate := min(leftToAllocate, gpu.GetFreeShares())

		gpuUUID, mortalGPUs, err := gpus.AllocateFromGPU(idx, canAllocate)
		if err != nil {
			return 0, fmt.Errorf("allocateLeastUsed failed: %w", err)
		}

		result.AppendMortalGPUs(mortalGPUs...)
		result.AppendPhysicalGPUs(gpuUUID)

		leftToAllocate -= canAllocate

		if leftToAllocate == 0 {
			break
		}
	}

	return leftToAllocate, nil
}
