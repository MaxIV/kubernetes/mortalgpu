# External Allocator

External allocator plugin allows an administrator to implement any policy they want.

The configuration of the allocator is documented in [Helm Values](../../../chart/values.yaml).

Any executable conforming to API below can be an external allocator.

The executable is called by MortalGPU daemon and is passed a JSON as stdin:

```json
{
  "load_map": {
    "unused_gpus": { // GPUs which do not have any container using them
      "gpus": [
        {
          "shared_device": {
            "uuid": "GPU_UUID_2", // NVIDIA GPU UUID
            "index": 1, // NVIDIA GPU index
            "shares": {
              "ShareSize": 50000, // Size of one MortalGPU in RAM bytes.
              "SharesCount": 5 // Total shares this physical GPU is split to.
            }
          },
          "mortal_gpus": [ // Shares which can be used for allocation
            "mortalgpu-meta-1-0-GPU_UUID_2",
            "mortalgpu-meta-1-1-GPU_UUID_2",
            "mortalgpu-meta-1-2-GPU_UUID_2",
            "mortalgpu-meta-1-3-GPU_UUID_2",
            "mortalgpu-meta-1-4-GPU_UUID_2"
          ]
        }
      ],
      // If there are several .gpus, then shows the smallest number of unused MortalGPU shares of them all
      "smallest_free_shares": 5,
      // If there are several .gpus, then shows the largest number of unused MortalGPU shares of them all
      "largest_free_shares": 5
    },
    "allocatable_gpus": { // GPUs which have "some" containers using them. The fields are the same as in "unused_gpus"
      "gpus": [
        {
          "shared_device": {
            "uuid": "GPU_UUID_1",
            "shares": {
              "ShareSize": 100000,
              "SharesCount": 10
            }
          },
          "mortal_gpus": [
            "mortalgpu-meta-0-0-GPU_UUID_1",
            "mortalgpu-meta-0-1-GPU_UUID_1",
            "mortalgpu-meta-0-2-GPU_UUID_1"
          ]
        }
      ],
      "smallest_free_shares": 3,
      "largest_free_shares": 3
    }
  },
  "requested_size": 5, // Number of MortalGPUs requested by a container.
  "allow_collocation": true // Passed from MortalGPU allocator configuration.
}
```

The executable is expected to made a decision, print to stdout the result and exit with code 0.

The result format is:

```json
{
    "mortal_gpus": [ // MortalGPUs to be allocated by a container.
        "mortalgpu-meta-1-0-GPU_UUID_2",
        "mortalgpu-meta-1-1-GPU_UUID_2",
        "mortalgpu-meta-1-2-GPU_UUID_2",
        "mortalgpu-meta-1-3-GPU_UUID_2",
        "mortalgpu-meta-1-4-GPU_UUID_2"
    ],
    "physical_gpus": [ // NVIDIA GPU UUIDs to be passed to the container
        "GPU_UUID_2"
    ],
    "error": null // If not null, then a string with error message describing why the allocation failed.
}
```

Directory [examples](examples/) contains a simple external allocator to demonstrate how it is 
supposed to work.