package external

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// EnvVar environment variable for an external executable.
type EnvVar struct {
	// Name environment variable name.
	Name string `json:"name,omitempty" yaml:"name,omitempty"`
	// Value environment variable value.
	Value string `json:"value,omitempty" yaml:"value,omitempty"`
}

// MarshalLogObject implements zap.ObjectMarshaler.
func (ev *EnvVar) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("name", ev.Name)
	enc.AddString("value", ev.Value)

	return nil
}

// ExecConfig configuration running an external application.
type ExecConfig struct {
	// Path path to the executable.
	Path string `json:"path,omitempty" yaml:"path,omitempty"`

	// Args arguments to call an executable.
	Args []string `json:"args,omitempty" yaml:"args,omitempty"`

	// Env environment variables to pass to an external executable.
	EnvVars []*EnvVar `json:"envVars,omitempty" yaml:"envVars,omitempty"`
}

// MarshalLogObject implements zap.ObjectMarshaler.
func (ec *ExecConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("path", ec.Path)

	zap.Strings("args", ec.Args).AddTo(enc)

	zap.Objects("env_vars", ec.EnvVars).AddTo(enc)

	return nil
}

// AllocateRequest MortalGPU allocation request passed to an external executable.
type AllocateRequest struct {
	// LoadMap current GPUs load map.
	LoadMap *types.LoadMap `json:"load_map,omitempty"` //nolint:tagliatelle // snake case is preferred.
	// RequestedSize number of requested MortalGPUs.
	RequestedSize int `json:"requested_size,omitempty"` //nolint:tagliatelle // snake case is preferred.

	// AllowCollocation a flag showing to an external allocator
	// whether scheduling multiple containers
	// on the same physical GPU is okay or not.
	AllowCollocation bool `json:"allow_collocation,omitempty"` //nolint:tagliatelle // snake case is preferred.
	// NoSplit mirrors the corresponding flag from Sharing configuration.
	NoSplit bool `json:"no_split,omitempty"` //nolint:tagliatelle,lll // snake case is preferred.
}

// MarshalLogObject implements zap.ObjectMarshaler.
func (req *AllocateRequest) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddInt("requested_size", req.RequestedSize)
	enc.AddBool("allow_collocation", req.AllowCollocation)
	enc.AddBool("no_split", req.NoSplit)

	if err := enc.AddObject("load_map", req.LoadMap); err != nil {
		return fmt.Errorf("can not encore LoadMap: %w", err)
	}

	return nil
}

// AllocateResponse response which must be printed to stdout by an external allocator
// as JSON.
type AllocateResponse struct {
	// Error indicates error.
	Error *string `json:"error,omitempty"`
	// MortalGPUs device names to allocate.
	MortalGPUs []string `json:"mortal_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually nice.
	// PhysicalGPUs physical GPU UUIDs to propagate to a container.
	PhysicalGPUs []string `json:"physical_gpus,omitempty"` //nolint:tagliatelle // "gpus" is visually nice.
}

func (resp *AllocateResponse) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	zap.Stringp("error", resp.Error).AddTo(enc)
	zap.Strings("mortal_gpus", resp.MortalGPUs).AddTo(enc)
	zap.Strings("physical_gpu_uuids", resp.PhysicalGPUs).AddTo(enc)

	return nil
}

// DeviceAllocator calls an external application to perform user-specified allocation.
type DeviceAllocator struct {
	config *ExecConfig

	allowCollocation bool
	noSplit          bool
}

var _ types.Allocator = (*DeviceAllocator)(nil) // type check.

// NewDeviceAllocator creates a new DeviceAllocator.
// If allowCollocation is false, then the allocator will never collocate containers
// with other containers.
// if there is no possibility to give a container an unued GPU,
// the allocation will fail.
func NewDeviceAllocator(
	allowCollocation, noSplit bool,
	config *ExecConfig,
) *DeviceAllocator {
	return &DeviceAllocator{
		config:           config,
		allowCollocation: allowCollocation,
		noSplit:          noSplit,
	}
}

// Allocate performs allocation and returns its result.
func (da *DeviceAllocator) Allocate(log *zap.Logger,
	request *types.AllocationRequest,
) (*types.AllocationResult, error) {
	logger := log.Named("ExternalAllocator").Named("Allocate")

	loadMaps, err := types.NewLoadMap(logger, request.AvailableMortalGPUs, request.PhysicalGPUs)
	if err != nil {
		return nil, fmt.Errorf("can not compute current GPUs usage: %w", err)
	}

	//nolint:gosec // G204 - the running executable is supposed to be defined
	// in the MortalGPU's configuration, so it is controlled by the application's adminitrator.
	// In this case, using a variable as the name+path of the executable
	// is secure in terms of that no user input can affect it.
	proc := exec.Command(da.config.Path, da.config.Args...)

	// Add the MortalGPU's environment + user-defined environment.
	osEnv := os.Environ()

	proc.Env = make([]string, 0, len(osEnv)+len(da.config.EnvVars))

	proc.Env = append(proc.Env, osEnv...)

	for i := 0; i < len(da.config.EnvVars); i++ {
		p := da.config.EnvVars[i]

		proc.Env = append(proc.Env, p.Name+"="+p.Value)
	}

	// Build a request.
	procRequest := &AllocateRequest{
		LoadMap:          loadMaps,
		RequestedSize:    request.Size,
		AllowCollocation: da.allowCollocation,
		NoSplit:          da.noSplit,
	}

	logger.Debug("Allocation request", zap.Object("request", procRequest))

	pStdIn, err := proc.StdinPipe()
	if err != nil {
		return nil, fmt.Errorf("can not attach to an external allocator stdin: %w", err)
	}

	if err = json.NewEncoder(pStdIn).Encode(procRequest); err != nil {
		return nil, fmt.Errorf("can not send AllocationRequest to an external allocator: %w", err)
	}

	// Run and wait.
	procOut, err := proc.Output()
	if err != nil {
		return nil, fmt.Errorf("can not start an external DeviceAllocator: %w", err)
	}

	// Read result.
	procResult := &AllocateResponse{}

	if err = json.Unmarshal(procOut, procResult); err != nil {
		return nil, fmt.Errorf("can not read AllocationResponse from an external allocator: %w", err)
	}

	logger.Debug("Allocation response", zap.Object("response", procResult))

	if procResult.Error != nil {
		return nil, fmt.Errorf("external allocator error: %w", err)
	}

	result := types.NewAllocationResultWithData(procResult.MortalGPUs, procResult.PhysicalGPUs)

	return result, nil
}
