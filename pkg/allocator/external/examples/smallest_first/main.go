package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/external"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
)

func sendResponse(resp *external.AllocateResponse) {
	if err := json.NewEncoder(os.Stdout).Encode(resp); err != nil {
		log.Fatalf("can not encode response: %s\n", err) //nolint:revive // deep-exit: It is an example.
	}
}

var bb = bytes.NewBufferString(`
{
  "load_map": {
    "unused_gpus": {
      "gpus": [
        {
          "shared_device": {
            "uuid": "GPU_UUID_2",
            "index": 1,
            "shares": {
              "ShareSize": 50000,
              "SharesCount": 5
            }
          },
          "mortal_gpus": [
            "mortalgpu-meta-1-0-GPU_UUID_2",
            "mortalgpu-meta-1-1-GPU_UUID_2",
            "mortalgpu-meta-1-2-GPU_UUID_2",
            "mortalgpu-meta-1-3-GPU_UUID_2",
            "mortalgpu-meta-1-4-GPU_UUID_2"
          ]
        }
      ],
      "smallest_free_shares": 5,
      "largest_free_shares": 5
    },
    "allocatable_gpus": {
      "gpus": [
        {
          "shared_device": {
            "uuid": "GPU_UUID_1",
            "shares": {
              "ShareSize": 100000,
              "SharesCount": 10
            }
          },
          "mortal_gpus": [
            "mortalgpu-meta-0-0-GPU_UUID_1",
            "mortalgpu-meta-0-1-GPU_UUID_1",
            "mortalgpu-meta-0-2-GPU_UUID_1"
          ]
        }
      ],
      "smallest_free_shares": 3,
      "largest_free_shares": 3
    }
  },
  "requested_size": 5,
  "allow_collocation": true
}
`)

func main() { //nolint:revive // function-length it is an example.
	var (
		req  external.AllocateRequest
		resp external.AllocateResponse
	)

	if err := json.NewDecoder(bb).Decode(&req); err != nil {
		s := fmt.Sprintf("malformed request: %s", err)
		resp.Error = &s
		sendResponse(&resp)
		os.Exit(0)
	}

	// Simple allocator.
	leftToAllocate := req.RequestedSize

	// Allocate empty GPUs and then somewhat-used.

	emptyGPUs := req.LoadMap.GetUnusedGPUs()
	emptyGPUs.SortIncreaseCapacity()

	emptyIter := emptyGPUs.Iterator()

	for emptyIter.Next() {
		idx, candidate := emptyIter.Item()

		canAllocate := min(candidate.GetFreeShares(), leftToAllocate)

		gpuUUID, mortals, err := emptyGPUs.AllocateFromGPU(idx, canAllocate)
		if err != nil {
			s := fmt.Sprintf("can not allocate an empty gpu %d: %s", idx, err)
			resp.Error = &s
			sendResponse(&resp)

			return
		}

		resp.MortalGPUs = append(resp.MortalGPUs, mortals...)
		resp.PhysicalGPUs = append(resp.PhysicalGPUs, gpuUUID)

		leftToAllocate -= canAllocate

		if leftToAllocate == 0 {
			break
		}
	}

	if leftToAllocate == 0 {
		sendResponse(&resp)

		return
	}

	// Try somewhat allocated GPUs.
	usedGPUs := req.LoadMap.GetAllocatableGPUs()
	usedGPUs.SortIncreaseCapacity()

	usedIter := usedGPUs.Iterator()

	for usedIter.Next() {
		idx, candidate := usedIter.Item()

		canAllocate := min(candidate.GetFreeShares(), leftToAllocate)

		gpuUUID, mortals, err := usedGPUs.AllocateFromGPU(idx, canAllocate)
		if err != nil {
			s := fmt.Sprintf("can not allocate a collocated gpu %d: %s", idx, err)
			resp.Error = &s
			sendResponse(&resp)

			return
		}

		resp.MortalGPUs = append(resp.MortalGPUs, mortals...)
		resp.PhysicalGPUs = append(resp.PhysicalGPUs, gpuUUID)

		leftToAllocate -= canAllocate

		if leftToAllocate == 0 {
			break
		}
	}

	if leftToAllocate == 0 {
		sendResponse(&resp)

		return
	}

	s := fmt.Sprintf("can not allocate request: %s", types.ErrAllocation)
	resp.Error = &s
	sendResponse(&resp)
}
