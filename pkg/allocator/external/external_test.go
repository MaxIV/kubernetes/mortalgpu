package external_test

import (
	"os/exec"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/external"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
	"go.uber.org/zap"
)

type TestExternalAllocatorSuite struct {
	suite.Suite
}

func (ts *TestExternalAllocatorSuite) SetupSuite() {
	cmd := exec.Command("go", []string{
		"build",
		"-C", "examples/smallest_first/",
		"-o", "allocator",
	}...)

	if err := cmd.Run(); err != nil {
		ts.FailNowf("Can not build allocator example", "error: %s", err)
	}
}

func (ts *TestExternalAllocatorSuite) TestSuccessRun() {
	allocator := external.NewDeviceAllocator(true, false, &external.ExecConfig{
		Path: "examples/smallest_first/allocator",
	})

	type testCase struct {
		request types.AllocationRequest
		result  types.AllocationResult
	}

	var tests = []*testCase{
		{
			request: types.AllocationRequest{
				PhysicalGPUs: []*sharecfg.SharedDevice{
					{
						UUID:  "GPU_UUID_1",
						Index: 0,
						Shares: sharecfg.SharesInfo{
							ShareSize:   100000,
							SharesCount: 10,
						},
					},
					{
						UUID:  "GPU_UUID_2",
						Index: 1,
						Shares: sharecfg.SharesInfo{
							ShareSize:   50000,
							SharesCount: 5,
						},
					},
				},
				AvailableMortalGPUs: []string{
					"mortalgpu-meta-0-0-GPU_UUID_1",
					"mortalgpu-meta-0-1-GPU_UUID_1",
					"mortalgpu-meta-0-2-GPU_UUID_1",
					"mortalgpu-meta-1-0-GPU_UUID_2",
					"mortalgpu-meta-1-1-GPU_UUID_2",
					"mortalgpu-meta-1-2-GPU_UUID_2",
					"mortalgpu-meta-1-3-GPU_UUID_2",
					"mortalgpu-meta-1-4-GPU_UUID_2",
				},
				Size: 5,
			},

			result: *types.NewAllocationResultWithData(
				[]string{
					"mortalgpu-meta-1-0-GPU_UUID_2",
					"mortalgpu-meta-1-1-GPU_UUID_2",
					"mortalgpu-meta-1-2-GPU_UUID_2",
					"mortalgpu-meta-1-3-GPU_UUID_2",
					"mortalgpu-meta-1-4-GPU_UUID_2",
				},
				[]string{
					"GPU_UUID_2",
				},
			),
		},
	}

	logger := zap.Must(zap.NewDevelopment())

	for _, testCase := range tests {
		result, err := allocator.Allocate(logger, &testCase.request)

		ts.Require().NoError(err)
		ts.Equal(testCase.result, *result)
	}
}

func TestExternalAllocator(t *testing.T) {
	suite.Run(t, &TestExternalAllocatorSuite{})
}
