package mortalnames

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

// DeviceIDInfo MortalGPU device information.
type DeviceIDInfo struct {
	// GPUUUID physical GPU UUID (or MIG device UUID).
	GPUUUID string
	// GPUIndex physical GPU index from NVIDIA API.
	GPUIndex int
	// MortalGPUIndex index of a MortalGPU share.
	MortalGPUIndex int
}

// 3 capture groups + 1 "whole" regex match.
const mortalDeviceIDParts = 4

// each meta gpu will start from 'mortalgpu-meta-[index-number]-[sequence-number]'.
var reMortalDeviceInfo = regexp.MustCompile(`mortalgpu-meta-(\d+)-(\d+)-(\S+)`)

// reMortalDeviceId to parse GPU UUID.
var reMortalDeviceId = regexp.MustCompile(`mortalgpu-meta-\d+-\d+-(\S+)`)

// reMortalDeviceIDIndex extracts only a physical GPU index.
var reMortalDeviceIDIndex = regexp.MustCompile(`mortalgpu-meta-(\d+)`)

// ErrIncorrectMortalDeviceID the parser could not parse given ID string.
var ErrIncorrectMortalDeviceID = errors.New(
	"metaDeviceIdToDeviceIndex: Incorrect MetaDeviceID, cannot extract device information")

// ExtractGPUDeviceIndex same as ParseDeviceID but only gets a physical GPU
// device index for the sake of having less operations to perform.
func ExtractGPUDeviceIndex(mortalDevice string) (int, error) {
	match := reMortalDeviceIDIndex.FindStringSubmatch(mortalDevice)
	if len(match) != 2 { //nomnd: 1 capture group + 1 "whole" match
		return -1, fmt.Errorf("%w: value is %q", ErrIncorrectMortalDeviceID, mortalDevice)
	}

	res, err := strconv.Atoi(match[1])
	if err != nil {
		return -1, fmt.Errorf("can not parse GPU Index from MortalGPU ID: %w", err)
	}

	return res, nil
}

// ParseDeviceUUID parses MortalGPU name to extract physical GPU UUID.
func ParseDeviceUUID(mortalDevice string) (string, error) {
	match := reMortalDeviceId.FindStringSubmatch(mortalDevice)
	if len(match) != 2 { //nomnd: 1 capture group + 1 "whole" match
		return "", fmt.Errorf("%w: value is %q", ErrIncorrectMortalDeviceID, mortalDevice)
	}

	return match[1], nil
}

// ParseDeviceInfo parses MortalGPU device name to extra
func ParseDeviceInfo(mortalDevice string) (*DeviceIDInfo, error) {
	match := reMortalDeviceInfo.FindStringSubmatch(mortalDevice)
	if len(match) != mortalDeviceIDParts {
		return nil, fmt.Errorf("%w: value is %q", ErrIncorrectMortalDeviceID, mortalDevice)
	}

	gpuIndex, err := strconv.Atoi(match[1])
	if err != nil {
		return nil, fmt.Errorf("can not parse GPU Index from MortalGPU ID: %w", err)
	}

	mortalGPUIndex, err := strconv.Atoi(match[2])
	if err != nil {
		return nil, fmt.Errorf("can not parse MortalGPU Index from its ID: %w", err)
	}

	gpuUUID := match[3]

	return &DeviceIDInfo{
		GPUUUID:        gpuUUID,
		GPUIndex:       gpuIndex,
		MortalGPUIndex: mortalGPUIndex,
	}, nil
}

// FormatDeviceID formats DeviceIDInfo to build a MortalGPU device name.
func FormatDeviceID(di *DeviceIDInfo) string {
	return fmt.Sprintf("mortalgpu-meta-%d-%d-%s", di.GPUIndex, di.MortalGPUIndex, di.GPUUUID)
}
