# MortalGPU

[![Container Image](https://img.shields.io/badge/Container%20Image-quay.io-green.svg)](https://quay.io/repository/maxiv/mortalgpu?tab=tags) [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/mortalgpu)](https://artifacthub.io/packages/search?repo=mortalgpu)

MortalGPU is Kubernetes device plugin implementing the concept of sharing Nvidia GPUs memory between workloads.

Apart from generic GPU support for batch workloads (such as AI/ML or scientific software), the development has a strong focus on the workloads run by mortals (such as interactive Jupyter Notebooks).

From user perspective, MortalGPU provides a way to share the GPU between workloads with a capability for **m**emory **o**ve**r**commi**t**, while maintaining VRAM **a**llocation **l**imit per GPU workload - the approach used for sharing RAM on Kubernetes.

It also provides visibility on the Pod level for both mortal users and admins.

## Project origins

To achieve flexible GPU sharing for [Jupyterhub installation](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyterhub-kubernetes) at [MAX IV Laboratory](https://www.maxiv.lu.se) we start evaluating the [MetaGPU Device Plugin](https://github.com/cnvrg/metagpu) that implements the promising idea of dividing the GPU into several meta-GPUs from the Kubernetes scheduling perspective.

MetaGPU was lacking the features we needed (like controlled RAM-like overcommit or MIG support), so we started to implement them to contribute upstream. But as pull requests were not handled for a long time and there were more and more features we wanted to add or rewrite, this process ended up in forking the project and developing MortalGPU as a separate, more feature-rich product.

## MortalGPU vs MetaGPU

While keeping the main operational logic of MetaGPU, MortalGPU adds features and changes the implementation of the logic all over the codebase. The most significant highlights are:

  - Support for memory overcommit with allocation limit, to allow working with GPU RAM the same way Kubernetes works with RAM: using requests for scheduling and limits for enforcing the workload upper usage limit.
  - [Multi-Instance GPU (MIG)](https://www.nvidia.com/en-us/technologies/multi-instance-gpu/) support, to allow sharing of the MIG partition of A100 or H100 GPUs between workloads.
  - Full-featured Helm Chart for distinct maintainability.
  - Hardening the security of the solution, dropping the need to execute commands inside the Pods in particular.
  - Improvement to client tools to address workload automation cases, including JSON support and memory limits discovery interfaces
  - Redefining the logging for better observability and troubleshooting.
  - Expanding Prometheus metrics with allocation stats.
  - Support for Cgroups v2 and all common container runtimes.
  - Sharing configuration using mortals-defined model names and memory chunks.
  - Flexible support for multiple GPUs per node via configurable allocation algorithms.
  - Support for interoperation with Kubernetes `CPUManager`
  - Security refactoring of gRPC API of MortalGPU

It is expected that MortalGPU project continue to diverge more from MetaGPU during further development.

## How MortalGPU works?

Several MortalGPU components are working together to provide GPUs sharing experience for end-user workloads. 

Implementation is based on [Nvidia Container Toolkit](https://github.com/NVIDIA/nvidia-container-toolkit)
and [go-nvml](https://github.com/NVIDIA/go-nvml) library.

### Device Plugin

MortalGPU looks for available GPUs and represents 1 GPU device (physical or MIG partition) with a configurable amount of meta-devices.
It registers as the device plugin in Kubernetes managing [extended resources](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#managing-extended-resources) in meta-devices capacity (e.g. `320` of `mortalgpu/v100`).

These meta-devices are used by Kubernetes scheduling, allowing (from a scheduling point of view) to allocate a fraction of GPU to the workload. MortalGPU then ensures that workload is only given access to the physical device, shares belong to, configuring the container runtime appropriately.

The device plugin part is also responsible for making available the `mgctl` - the command line tool that provides allocation data and GPU usage observability on the container level.

### Workload monitoring and memory enforcement

MortalGPU monitoring loop is doing the mapping of GPU processes to containers running in Kubernetes Pods. 

This information is used to provide Kubernetes-aware observability in general and container-scoped resource usage in particular for both end-users and admins.

Furthermore, the GPU memory used by the container processes is then matched against defined allocation limits. MortalGPU enforces the memory limits by killing the processes.

### Observability

MortalGPU implements gRPC interface for providing the monitoring data to both `mgctl` tool and Prometheus exporter.

[Prometheus exporter](./cmd/mgex) provides both GPU Device level and Kubernetes-aware Process level metrics allowing to monitor GPU consumption of specific workloads in the shared environment.


## Installing MortalGPU

The prerequisite of using the MortalGPU is the installation of [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) on the Kubernetes nodes.

Use the provided [Helm Chart](https://artifacthub.io/packages/helm/mortalgpu/mortalgpu) to install MortalGPU.

## Configuring device sharing

You have to define the GPU sharing configuration via the `config.deviceSharing` in the `values.yaml`. 

It is recommended to define shares based on GPU memory (e.g. in `100MB` chunks), either manually specifying the desired `metagpusPerGpu` or relying on `metagpusPerGpuMemoryChunkMB` which takes a chunk size as an argument and defines the number of shares based on GPU memory.

For example, defining `320` shares for all worker nodes (homogeneous cluster with V100 32GB) will look like this:
```
  deviceSharing:
    - resourceName: mortalgpu/v100
      metagpusPerGpu: 320
```

Or automatically calculated number shares for A100 shared MIG partition, each share defines `100MB` memory chunk:
```
  deviceSharing:
    - resourceName: mortalgpu/a100-shared
      metagpusPerGpuMemoryChunkMB: 100
      migid:
       - 2
```

Use the `modelName` or `uuid` filters to define the heterogeneous environments configuration to be managed by the same MortalGPU DaemonSet:
```
  deviceSharing:
    - resourceName: mortalgpu/v100
      metagpusPerGpu: 320
      uuid: ['GPU-4cb75a4d-8c9e-b2ed-3eba-14a9ab7a0c29', 'GPU-4cb75a4d-c065-84cd-d364-32bee332258e']
    - resourceName: mortalgpu/a100
      metagpusPerGpuMemoryChunkMB: 100
      modelName: ['NVIDIA A100 80GB PCIe', 'NVIDIA A100 40GB PCIe']
```

Please follow the [Helm Chart documentation](https://artifacthub.io/packages/helm/mortalgpu/mortalgpu) for information about all possible configuration values.

## Configuring Allocator

When you have multiple GPUs on the cluster nodes, there is a possibility to alter the allocation 
logic within the node.

MortalGPU provides 3 allocator plugins to choose from when deploying the MortalGPU instance:
  - `collocate` (default) - tries to schedule all Pods on already used GPUs first;
  - `spread` - the opposite of `collocate`: tries to allocate new Pods on the least allocated GPUs;
  - `external` - an external executable provided by administrator is used to make allocation decision (read [documentation](pkg/allocator/external/README.md) for more detalis).

The allocator is configured in `allocator` section of Helm values.

There is no support for specifying an allocation strategy on per-Pod basis, since Kubernetes does not provide Pod information to DevicePlugin.

## Using MortalGPU resources in workloads

MortalGPU as *Device Plugin* just defines *extended resources* based on the sharing configuration.

To request the GPU resource under the MortalGPU management just use the normal way of `resources` definitions.

For example, you can see in the example output of the `kubectl describe nodes` MortalGPU extended resources are defined like this:
```
Capacity:
  cpu:                     96
  mortalgpu/a100-10g:      200
  mortalgpu/a100-20g:      200
  mortalgpu/a100-shared:   400
  memory:                  1056011536Ki
  pods:                    220
```

In the container specification 4GB of GPU RAM can be requested like this:

```yaml
    resources:
      limits:
        cpu: "8"
        mortalgpu/a100-shared: "40"
        memory: "107374182400"
      requests:
        cpu: 100m
        mortalgpu/a100-shared: "40"
        memory: 524288k
```


However, Kubernetes [does not allow](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#consuming-extended-resources) overcommit of the extended resources.
So `requests` and `limits` must be equal if both are present in a container spec.

To work around this and allow GPU memory overcommit, MortalGPU handles the specific Pod annotation `gpu-mem-limit.<resource name>`. 
For example, the following annotation allows to overcommit GPU memory up to 8GB:

```yaml
metadata:
  annotations:
    gpu-mem-limit.mortalgpu/a100-shared: "80"
```

### Retrieving GPU allocation data from application container

MortalGPU provides an interface to get GPU memory limits and requests from within the container
via `mgctl` tool. In the default configuration, MortalGPU automatically mounts `mgctl` binary into all containers requesting MortalGPU managed GPU resources.

> :information: Starting from v1.3.0 MortalGPU requires Service Account token to be mounted inside the Pod to use `mgctl`. This is part of gRPC security.

Running the `mgctl` as part of application wrapper scripts (or init container generating application config files), you can make an application aware of the memory constrains when GPU is shared. There are shortcuts for most common data: 
  - `mgctl get limits` - get GPU memory limit in bytes. Process will be killed if run over the limit.
  - `mgctl get requests` - get requested GPU memory in bytes. There is a risk of out-of-memory if run over the requested memory.
  - `mgctl get utilization-treshold` - get the fraction of allocated GPU device memory matching the specified requests (from 0 to 1). 
  You can add `--limits` flag to calculate utilization based on specified resource limits instead of requests.

For example, if application is using `APP_GPU_MEM` environment variable to define how much memory to allocate:

```sh
export APP_GPU_MEM=$( mgctl get limits )
/usr/local/bin/run-app
```

Another example, when application relies on GPU device memory utilization ratio:
```sh
/usr/local/bin/run-app --gpu-memory-utilization $( mgctl get utilization-treshold --limits )
```

Other container GPU allocation data can be queries using the `json` or `jsonpath` output of `mgctl`. 
For example, to query the GPU model name of allocated device:

```sh
mgctl get devices -o jsonpath --jsonpath '{.devices[*].model_name}'
```


## Notable Changes

### Upgrading to v1.3.0

Since v1.3.0 MortalGPU changed its gRPC API authentication mechanism from static JWT tokens 
(previously defined in `config.grpcSecurity` section) to Kubernetes ServiceAccount Tokens (using the [TokenReviews API](https://kubernetes.io/docs/reference/kubernetes-api/authentication-resources/token-review-v1/))
to improve the security and simplify mandatory configuration. Service Account token validation is part of the gRPC API and performed implicitly.

When upgrading to v1.3.0 you can cleanup static tokens from `config.grpcSecurity` section, they are ignored anyway.

You can fine-tune which workloads has access to all information via `privilegedServiceAccounts` option.

To use the optional `mgctl` tool from the workload Pod that requested MortalGPU resources it is now required to
mount the ServiceAccount token. The [Pod.spec.automountServiceAccountToken](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/#opt-out-of-api-credential-automounting)
is `true` by default, so unless you have it disabled explicitly no specific actions are needed. 

One of the target use-cases, the JupyterHub's [kubespawner](https://github.com/jupyterhub/kubespawner), for example, is not mounting the
ServiceAccount token by default. The recommendation is to create a Service Account with no privileges assigned
and mount it to user Pods. MortalGPU uses tokens for Pod identification only and no Kubernetes API access is necessary. 
