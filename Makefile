DIR="."

build:
	go build -ldflags="-X 'main.Build=$$(git rev-parse --short HEAD)' -X 'main.Version=1.0.0'" -v -o bin/mgdp cmd/mgdp/main.go

build-exporter:
	go build -ldflags="-X 'main.Build=$$(git rev-parse --short HEAD)' -X 'main.Version=1.0.0'" -v -o bin/mgex cmd/mgex/*.go

build-mgctl:
	go build -ldflags="-X 'main.Build=$$(git rev-parse --short HEAD)' -X 'main.Version=1.0.0'" -v -o bin/mgctl cmd/mgctl/*.go

build-proto:
	buf generate pkg/mgsrv/deviceapi
	buf lint
	buf build
	buf generate

docker-build: build-proto
	docker build \
	 --platform linux/x86_64 \
     --build-arg BUILD_SHA=$(shell git rev-parse --short HEAD) \
     --build-arg BUILD_VERSION=1.0.0 \
     -t mortalgpu:$(shell git rev-parse --abbrev-ref HEAD) .

test:
	go test ./pkg/... -v

test-allocator:
	go test ./pkg/allocator/... -v

test-gpumgr:
	go test ./pkg/gpumgr/... -v

golangci-lint-fix:
	@echo "Performing in-place golangci-lint with fix in \"${DIR}\""
	cd "${DIR}" && golangci-lint run --fix