package main

import (
	"fmt"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/external"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/plugin"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type AllocatorConfig struct {
	Plugin           string              `json:"plugin,omitempty"           yaml:"plugin,omitempty"`
	NoSplit          bool                `json:"noSplit,omitempty"          yaml:"noSplit,omitempty"`
	AllowCollocation bool                `json:"allowCollocation,omitempty" yaml:"allowCollocation,omitempty"`
	ExternalConfig   external.ExecConfig `json:"externalConfig,omitempty"   yaml:"externalConfig,omitempty"`
}

func (ac *AllocatorConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("plugin", ac.Plugin)
	enc.AddBool("no_split", ac.NoSplit)
	enc.AddBool("allow_collocation", ac.AllowCollocation)

	if err := enc.AddObject("external_config", &ac.ExternalConfig); err != nil {
		return fmt.Errorf("cannot encode ExternalConfig: %w", err)
	}

	return nil
}

type MGDPConfig struct {
	ServerAddr string `json:"serverAddr,omitempty" yaml:"serverAddr,omitempty"`

	ProcessDiscoveryPeriod int   `json:"processDiscoveryPeriod,omitempty" yaml:"processDiscoveryPeriod,omitempty"`
	DeviceCacheTTL         int64 `json:"deviceCacheTTL,omitempty"         yaml:"deviceCacheTTL,omitempty"`

	MGCTLMount              bool   `json:"mgctlMount,omitempty"              yaml:"mgctlMount,omitempty"`
	MGCTLMountHostPath      string `json:"mgctlMountHostPath,omitempty"      yaml:"mgctlMountHostPath,omitempty"`
	MGCTLMountContainerPath string `json:"mgctlMountContainerPath,omitempty" yaml:"mgctlMountContainerPath,omitempty"`

	PluginMounts []*plugin.DevicePluginMount `json:"pluginMounts,omitempty" yaml:"pluginMounts,omitempty"`

	DeviceSharing []*sharecfg.DeviceSharingConfig `json:"deviceSharing,omitempty" yaml:"deviceSharing,omitempty"`

	Allocator AllocatorConfig `json:"allocator,omitempty" yaml:"allocator,omitempty"`

	MemoryEnforcer           bool             `json:"memoryEnforcer,omitempty"           yaml:"memoryEnforcer,omitempty"`
	DeviceSpecs              bool             `json:"deviceSpecs,omitempty"              yaml:"deviceSpecs,omitempty"`
	ContainerEnvVarsOverride []*plugin.EnvVar `json:"containerEnvVarsOverride,omitempty" yaml:"containerEnvVarsOverride,omitempty"`

	PrivilegedServiceAccounts []*mgsrv.PrivilegedServiceAccount `json:"privilegedServiceAccounts,omitempty" yaml:"privilegedServiceAccounts"`
}

func (cfg *MGDPConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("server_addr", cfg.ServerAddr)
	enc.AddDuration("process_discovery_period", time.Second*time.Duration(cfg.ProcessDiscoveryPeriod))
	enc.AddDuration("device_cache_TTL", time.Second*time.Duration(cfg.DeviceCacheTTL))

	enc.AddBool("mgctl_mount", cfg.MGCTLMount)
	enc.AddString("mgctl_mount_host_path", cfg.MGCTLMountHostPath)
	enc.AddString("mgctl_mount_container_path", cfg.MGCTLMountContainerPath)

	zap.Objects("plugin_mounts", cfg.PluginMounts).AddTo(enc)

	zap.Objects("device_sharing", cfg.DeviceSharing).AddTo(enc)

	if err := enc.AddObject("allocator", &cfg.Allocator); err != nil {
		return fmt.Errorf("cannot encode Allocator: %w", err)
	}

	enc.AddBool("memory_enforcer", cfg.MemoryEnforcer)
	enc.AddBool("device_specs", cfg.DeviceSpecs)
	zap.Objects("container_env_vars_override", cfg.ContainerEnvVarsOverride).AddTo(enc)

	zap.Objects("privileged_service_accounts", cfg.PrivilegedServiceAccounts).AddTo(enc)

	return nil
}
