package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/go-logr/logr/funcr"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	k8sclient "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	contrlog "sigs.k8s.io/controller-runtime/pkg/log"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/collocate"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/external"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/spread"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator/types"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/log"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/nvmlutils"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/plugin"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

var ErrUnsupportedAllocator = errors.New("unsupported allocator plugin name")

type param struct {
	name      string
	shorthand string
	value     any
	usage     string
	required  bool
}

var (
	Version    string
	Build      string
	rootParams = []param{
		{name: "config", shorthand: "c", value: ".", usage: "path to configuration file"},
		{name: "json-log", shorthand: "", value: false, usage: "output logs in json format"},
		{name: "verbose", shorthand: "", value: false, usage: "enable verbose logs"},
		{name: "metrics-port", shorthand: "", value: 2113, usage: "Prometheus metrics exporter port"},
	}
	metaGpuRecalc              = make(chan bool)
	metaGpuDevicePluginVersion = &cobra.Command{
		Use:   "version",
		Short: "Print mortalgpu version and build sha",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("🐾 version: %s build: %s \n", Version, Build)
		},
	}
	metaGpuStart = &cobra.Command{
		Use:   "start",
		Short: "Start metagpu device plugin",
		Run: func(_ *cobra.Command, args []string) {
			logger := zapLogger.Desugar()

			// Init NVML library
			nvmlutils.InitUtils(logger)

			var fullConfiguration MGDPConfig

			if err := viper.Unmarshal(&fullConfiguration); err != nil {
				logger.Fatal("Can not load configuration", zap.Error(err))
			}

			logger.Info("Startup configuration", zap.Object("configuration", &fullConfiguration))

			allocatorPlugin, err := initAllocator(&fullConfiguration.Allocator)
			if err != nil {
				logger.Fatal("Can not init allocator plugin", zap.Error(err))
			}

			// Start Prometheus exporter.
			go func() {
				logger.Info("Starting Prometheus metrics exporter",
					zap.String("port", viper.GetString("metrics-port")),
					zap.String("path", "/metrics"),
				)

				http.Handle("/metrics", promhttp.Handler())

				err := http.ListenAndServe(":"+viper.GetString("metrics-port"), nil)
				if err != nil {
					logger.Fatal("Can not start Prometheus metrics exporter", zap.Error(err))
				}
			}()

			var plugins []*plugin.MetaGpuDevicePlugin
			// load gpu shares configuration
			shareConfigs := sharecfg.NewDeviceSharingConfig(logger, fullConfiguration.DeviceSharing)

			if l := logger.Check(zap.DebugLevel, "Device sharing configuration"); l != nil {
				l.Write(zap.Any("sharing_configuration", shareConfigs))
			}

			// load device plugin bind-mounts configuration
			pluginMounts := fullConfiguration.PluginMounts

			if fullConfiguration.MGCTLMount {
				// mount for mgctl
				pluginMounts = append(pluginMounts,
					&plugin.DevicePluginMount{
						HostPath:  filepath.Join(fullConfiguration.MGCTLMountHostPath, "mgctl"), //nolint:goconst // Let it be.
						MountPath: filepath.Join(fullConfiguration.MGCTLMountContainerPath, "mgctl"),
					})
			}

			clientConfig, err := rest.InClusterConfig()
			if err != nil {
				logger.Fatal("can not load In Cluster Kubernetes API client configuration", zap.Error(err))
			}

			clientSet, err := k8sclient.NewForConfig(clientConfig)
			if err != nil {
				logger.Fatal("can not create Kubernetes API client configuration", zap.Error(err))
			}

			// init plugins
			for _, c := range shareConfigs.Configs {
				iLog := logger.With(zap.Object("sharing_config", c))

				devMan, err := plugin.NewNvidiaDeviceManager(iLog,
					time.Duration(fullConfiguration.DeviceCacheTTL)*time.Second, c, allocatorPlugin)
				if err != nil {
					iLog.Fatal("device manager can not be started", zap.Error(err))
				}

				plugins = append(plugins,
					plugin.NewMetaGpuDevicePlugin(iLog,
						pluginMounts,
						fullConfiguration.DeviceSpecs,
						fullConfiguration.ContainerEnvVarsOverride,
						metaGpuRecalc,
						devMan))

				iLog.Debug("Created device plugin")
			}

			// start plugins
			for _, p := range plugins {
				p.Start()
			}

			// start grpc server
			mgsrv.NewMetaGpuServer(logger,
				viper.GetString("nodename"),
				fullConfiguration.MemoryEnforcer, fullConfiguration.ServerAddr,
				clientSet.AuthenticationV1().TokenReviews(),
				fullConfiguration.PrivilegedServiceAccounts,
				shareConfigs).Start()
			// handle interrupts
			sigCh := make(chan os.Signal, 1)
			signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

			for {
				select {
				case s := <-sigCh:
					zapLogger.Infof("signal: %s, shutting down", s)
					// stop all plugins
					for _, p := range plugins {
						p.Stop()
					}
					zapLogger.Info("bye bye 👋")
					os.Exit(0)
				}
			}
		},
	}
	rootCmd = &cobra.Command{
		Use:   "metagpu",
		Short: "Metagpu - fractional accelerator device plugin",
	}
)

var zapLogger *zap.SugaredLogger

func init() {
	cobra.OnInitialize(initConfig)
	setParams(rootParams, rootCmd)
	rootCmd.AddCommand(metaGpuDevicePluginVersion)
	rootCmd.AddCommand(metaGpuStart)
}

func initConfig() {
	viper.AutomaticEnv()

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.AddConfigPath(viper.GetString("config"))
	viper.SetEnvPrefix("METAGPU_DEVICE_PLUGIN")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	setupLogging()

	if err := viper.ReadInConfig(); err != nil {
		zapLogger.Fatalw("config file not found", "error", err)
	}

	viper.WatchConfig()

	viper.OnConfigChange(func(e fsnotify.Event) {
		zapLogger.Infof("config file changed, triggering meta gpu recalculation", "file", e.Name)
		metaGpuRecalc <- true
	})
}

func setParams(params []param, command *cobra.Command) {
	for _, param := range params {
		switch v := param.value.(type) {
		case int:
			command.PersistentFlags().IntP(param.name, param.shorthand, v, param.usage)
		case string:
			command.PersistentFlags().StringP(param.name, param.shorthand, v, param.usage)
		case bool:
			command.PersistentFlags().BoolP(param.name, param.shorthand, v, param.usage)
		}

		if err := viper.BindPFlag(param.name, command.PersistentFlags().Lookup(param.name)); err != nil {
			panic(err)
		}
	}
}

func setupLogging() {
	logger := log.SetupLogging(viper.GetBool("verbose"), viper.GetBool("json-log"), "mgdp")

	zapLogger = logger.Sugar()

	zap.ReplaceGlobals(logger)

	var controllerLogger func(args ...any)

	if viper.GetBool("verbose") {
		controllerLogger = zapLogger.Named("controller-log").Debug
	} else {
		controllerLogger = zapLogger.Named("controller-log").Info
	}

	contrlog.SetLogger(
		funcr.New(func(prefix, args string) { controllerLogger(prefix, args) },
			funcr.Options{
				LogCaller:     funcr.All,
				LogCallerFunc: true,
			},
		),
	)
}

func initAllocator(allocCfg *AllocatorConfig) (types.Allocator, error) {
	switch allocCfg.Plugin {
	case "collocate":
		return collocate.NewDeviceAllocator(allocCfg.NoSplit), nil
	case "spread":
		return spread.NewDeviceAllocator(allocCfg.AllowCollocation, allocCfg.NoSplit), nil
	case "external":
		return external.NewDeviceAllocator(allocCfg.AllowCollocation, allocCfg.NoSplit, &allocCfg.ExternalConfig), nil
	default:
		return nil, fmt.Errorf("%w: plugin=%q", ErrUnsupportedAllocator, allocCfg.Plugin)
	}
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
