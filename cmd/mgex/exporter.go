package main

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
)

// Logging keys, "lk".
const (
	lkPodName       = "pod_name"
	lkPodNamespace  = "pod_namespace"
	lkResourceName  = "resource_name"
	lkNodeName      = "node_name"
	lkContainerName = "container_name"
	lkContainerID   = "container_id"
)

// Prometheus metrics names.
const (
	promNamespace = "metagpu"
	promSSDevice  = "device"
	promSSProcess = "process"
)

// Prometheus shared labels, shl - SHared Label.
const (
	shlDeviceUUID      = "device_uuid"
	shlDeviceModelName = "device_model_name"
	shlDeviceIndex     = "device_index"
	shlResourceName    = lkResourceName
	shlNodeName        = lkNodeName

	// Should be MortalGPU UUID but TODO: check.
	shlMortalUUID   = "uuid"
	shlPid          = "pid"
	shlCmdline      = "cmdline"
	shlUser         = "user"
	shlPodName      = lkPodName
	shlPodNamespace = lkPodNamespace
)

var (
	//nolint:gochecknoglobals // Prometheus metrics.
	devicesCache map[string]*pbdevice.Device

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceShares = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "shares",
		Help:      "total shares for single gpu unit",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceAllocatedShares = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "allocated_shares",
		Help:      "allocated shares for single gpu unit",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceMemTotal = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "memory_total",
		Help:      "total memory per device",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceMemFree = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "memory_free",
		Help:      "free memory per device",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceMemUsed = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "memory_used",
		Help:      "used memory per device",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceMemShareSize = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSDevice,
		Name:      "memory_share_size",
		Help:      "metagpu memory share size",
	}, []string{shlDeviceUUID, shlDeviceModelName, shlDeviceIndex, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessAbsoluteGpuUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "absolute_gpu_utilization",
		Help:      "gpu process utilization in percentage",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMemoryUsage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "memory_usage",
		Help:      "process gpu-memory usage",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMetagpuRequests = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "metagpu_requests",
		Help:      "total metagpu requests in deployment spec",
	}, []string{shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMetagpuLimits = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "metagpu_limits",
		Help:      "total metagpu limits in deployment spec",
	}, []string{shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessRequestedMetagpuGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "requested_metagpu_gpu_utilization",
		Help:      "requested metagpu gpu utilization",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMaxAllowedMetagpuGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "max_allowed_metagpu_gpu_utilization",
		Help:      "max allowed metagpu gpu utilization",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMetagpuRelativeGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "metagpu_relative_gpu_utilization",
		Help:      "relative to metagpu limits gpu utilization",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessRequestedMetaGpuMemory = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "requested_metagpu_memory",
		Help:      "requested metagpu memory usage",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMaxAllowedMetaGpuMemory = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "max_allowed_metagpu_memory",
		Help:      "max allowed metagpu memory usage",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})

	//nolint:gochecknoglobals // Prometheus metrics.
	deviceProcessMetagpuRelativeMemoryUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: promNamespace,
		Subsystem: promSSProcess,
		Name:      "metagpu_relative_memory_utilization",
		Help:      "relative to metagpus limits memory utilization",
	}, []string{shlMortalUUID, shlPid, shlCmdline, shlUser, shlPodName, shlPodNamespace, shlResourceName, shlNodeName})
)

func prepareEnv(log *zap.Logger) (*grpc.ClientConn, string, error) {
	logger := log.Named("preparePrintDeviceProcessesEnv")

	conn := ctlutils.GetGrpcMetaGpuSrvClientConn(logger, viper.GetString(vpnMGSrv))
	if conn == nil {
		logger.Fatal("can't initiate connection to MortalGPU server", zap.String("server", viper.GetString(vpnMGSrv)))
	}

	// Read Token from the predefined Service Account Token Location.
	token, err := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")
	if err != nil {
		return nil, "", fmt.Errorf("can not read Service Account Token File: %w", err)
	}

	return conn, string(token), nil
}

func getGpuContainers(logger *zap.Logger, conn *grpc.ClientConn, token string) []*pbdevice.GpuContainer {
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetGpuContainersRequest{}
	ctx := ctlutils.AuthenticatedContext(token)

	resp, err := devices.GetGpuContainers(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Containers")

		return nil
	}

	return resp.GetGpuContainers()
}

func getGpuDevicesInfo(logger *zap.Logger, conn *grpc.ClientConn, token string) []*pbdevice.Device {
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetMetaDeviceInfoRequest{}
	ctx := ctlutils.AuthenticatedContext(token)

	resp, err := devices.GetMetaDeviceInfo(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Devices Info")

		return nil
	}

	return resp.GetDevices()
}

func setGpuDevicesCache(logger *zap.Logger, conn *grpc.ClientConn, token string) map[string]*pbdevice.Device {
	if devicesCache != nil {
		return devicesCache
	}

	devicesCache = make(map[string]*pbdevice.Device)
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetDevicesRequest{}
	ctx := ctlutils.AuthenticatedContext(token)

	resp, err := devices.GetDevices(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Devices")

		return nil
	}

	devicesCache = resp.GetDevice()

	return devicesCache
}

func clearGpuDevicesCache() {
	devicesCache = nil
}

func setDevicesMetrics(logger *zap.Logger, conn *grpc.ClientConn, token string) {
	// GPU device metrics
	for _, gpuDeviceInfo := range getGpuDevicesInfo(logger, conn, token) {
		labels := []string{
			gpuDeviceInfo.GetUuid(),
			gpuDeviceInfo.GetModelName(),
			strconv.FormatUint(uint64(gpuDeviceInfo.GetIndex()), 10),
			gpuDeviceInfo.GetResourceName(),
			gpuDeviceInfo.GetNodeName(),
		}

		deviceShares.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetShares()))
		deviceAllocatedShares.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetAllocatedShares()))
		deviceMemTotal.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetMemoryTotal()))
		deviceMemFree.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetMemoryFree()))
		deviceMemUsed.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetMemoryUsed()))
		deviceMemShareSize.WithLabelValues(labels...).Set(float64(gpuDeviceInfo.GetMemoryShareSize()))
	}
}

func resetProcessLevelMetrics() {
	deviceProcessAbsoluteGpuUtilization.Reset()
	deviceProcessMemoryUsage.Reset()
	deviceProcessMetagpuRequests.Reset()
	deviceProcessMetagpuLimits.Reset()
	deviceProcessRequestedMetagpuGPUUtilization.Reset()
	deviceProcessMaxAllowedMetagpuGPUUtilization.Reset()
	deviceProcessMetagpuRelativeGPUUtilization.Reset()
	deviceProcessRequestedMetaGpuMemory.Reset()
	deviceProcessMaxAllowedMetaGpuMemory.Reset()
	deviceProcessMetagpuRelativeMemoryUtilization.Reset()
}

func setProcessesMetrics(logger *zap.Logger, conn *grpc.ClientConn, token string) {
	// reset metrics
	resetProcessLevelMetrics()
	// GPU processes metrics
	for _, gpuContainer := range getGpuContainers(logger, conn, token) {
		// metagpu requests
		deviceProcessMetagpuRequests.WithLabelValues(
			gpuContainer.GetPodId(),
			gpuContainer.GetPodNamespace(),
			gpuContainer.GetResourceName(),
			gpuContainer.GetNodeName()).Set(float64(gpuContainer.GetMetagpuRequests()))
		// metagpu limits
		deviceProcessMetagpuLimits.WithLabelValues(
			gpuContainer.GetPodId(),
			gpuContainer.GetPodNamespace(),
			gpuContainer.GetResourceName(),
			gpuContainer.GetNodeName()).Set(float64(gpuContainer.GetMetagpuLimits()))

		// if pod has processes expose process metrics
		if len(gpuContainer.GetDeviceProcesses()) > 0 {
			for _, process := range gpuContainer.GetDeviceProcesses() {
				// set labels for device process level metrics
				labels := []string{
					process.GetDevuuid(),
					strconv.FormatUint(uint64(process.GetPid()), 10),
					process.GetCmdline(),
					process.GetUser(),
					gpuContainer.GetPodId(),
					gpuContainer.GetPodNamespace(),
					gpuContainer.GetResourceName(),
					gpuContainer.GetNodeName(),
				}

				// absolute memory and gpu usage
				deviceProcessAbsoluteGpuUtilization.WithLabelValues(labels...).Set(float64(process.GetGpuUtilization()))
				deviceProcessMemoryUsage.WithLabelValues(labels...).Set(float64(process.GetMemory()))

				// requested (relavive to metagpu requests) gpu and memory utilization
				deviceProcessRequestedMetagpuGPUUtilization.WithLabelValues(labels...).Set(getRequestedMetagpuGPUUtilization(logger, gpuContainer)) //nolint:lll // More readable like that.
				deviceProcessRequestedMetaGpuMemory.WithLabelValues(labels...).Set(getRequestedMetaGpuMemory(logger, gpuContainer))

				// max allowed (relative to metagpus limits) gpu and memory utilization
				deviceProcessMaxAllowedMetagpuGPUUtilization.WithLabelValues(labels...).Set(getMaxAllowedMetagpuGPUUtilization(logger, gpuContainer)) //nolint:lll // More readable like that.
				deviceProcessMaxAllowedMetaGpuMemory.WithLabelValues(labels...).Set(getMaxAllowedMetaGpuMemory(logger, gpuContainer))                 //nolint:lll // More readable like that.

				// relative to limits gpu and memory utilization
				deviceProcessMetagpuRelativeGPUUtilization.WithLabelValues(labels...).Set(getRelativeGPUUtilization(logger, gpuContainer, process))       //nolint:lll // More readable like that.
				deviceProcessMetagpuRelativeMemoryUtilization.WithLabelValues(labels...).Set(getRelativeMemoryUtilization(logger, gpuContainer, process)) //nolint:lll // More readable like that.
			}
		} else { // pod doesn't have any processes, all the metrics should be set to 0
			labels := []string{
				"-", "-", "-", "-",
				gpuContainer.GetPodId(),
				gpuContainer.GetPodNamespace(),
				gpuContainer.GetResourceName(),
				gpuContainer.GetNodeName(),
			}
			// absolute memory and gpu usage
			deviceProcessAbsoluteGpuUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMemoryUsage.WithLabelValues(labels...).Set(0)
			// max (relative to metagpus request) allowed gpu and memory utilization
			deviceProcessMaxAllowedMetagpuGPUUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMaxAllowedMetaGpuMemory.WithLabelValues(labels...).Set(0)
			// relative gpu and memory utilization
			deviceProcessMetagpuRelativeGPUUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMetagpuRelativeMemoryUtilization.WithLabelValues(labels...).Set(0)
		}
	}
}

func getMaxAllowedMetagpuGPUUtilization(logger *zap.Logger, container *pbdevice.GpuContainer) float64 {
	device, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get max allowed Metagpu GPU utilization",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
		)

		return 0
	}

	return float64((100 / device.GetDevice().GetShares()) * uint32(container.GetMetagpuLimits()))
}

func getMaxAllowedMetaGpuMemory(logger *zap.Logger, container *pbdevice.GpuContainer) float64 {
	device, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get max allowed Metagpu memory",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
			zap.Error(err),
		)

		return 0
	}

	return float64(uint64(container.GetMetagpuLimits()) * device.GetDevice().GetMemoryShareSize())
}

func getRequestedMetagpuGPUUtilization(logger *zap.Logger, container *pbdevice.GpuContainer) float64 {
	device, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get requested metagpu GPU Utilization",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
			zap.Error(err),
		)

		return 0
	}

	return float64((100 / device.GetDevice().GetShares()) * uint32(container.GetMetagpuRequests()))
}

func getRequestedMetaGpuMemory(logger *zap.Logger, container *pbdevice.GpuContainer) float64 {
	device, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get requested Metagpu memory",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
			zap.Error(err),
		)

		return 0
	}

	return float64(uint64(container.GetMetagpuRequests()) * device.GetDevice().GetMemoryShareSize())
}

func getFirstContainerDevice(container *pbdevice.GpuContainer) (*pbdevice.ContainerDevice, error) {
	if len(container.GetContainerDevices()) == 0 {
		return nil, errors.New("no allocated gpus found")
	}

	return container.GetContainerDevices()[0], nil
}

func getRelativeGPUUtilization(logger *zap.Logger,
	container *pbdevice.GpuContainer, process *pbdevice.DeviceProcess,
) float64 {
	d, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get MaxAllowedMetagpu GPU Utilization",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
			zap.Error(err),
		)

		return 0
	}

	maxMetaGpuUtilization := (100 / d.GetDevice().GetShares()) * uint32(container.GetMetagpuLimits())
	metaGpuUtilization := 0

	if maxMetaGpuUtilization > 0 && process.GetGpuUtilization() > 0 {
		metaGpuUtilization = int((process.GetGpuUtilization() * 100) / maxMetaGpuUtilization)
	}

	return float64(metaGpuUtilization)
}

func getRelativeMemoryUtilization(logger *zap.Logger,
	container *pbdevice.GpuContainer, process *pbdevice.DeviceProcess,
) float64 {
	device, err := getFirstContainerDevice(container)
	if err != nil {
		logger.Error("can't get MaxAllowedMetagpu GPU Utilization",
			zap.String(lkPodName, container.GetPodId()),
			zap.String(lkPodNamespace, container.GetPodNamespace()),
			zap.String(lkResourceName, container.GetResourceName()),
			zap.String(lkNodeName, container.GetNodeName()),
			zap.String(lkContainerName, container.GetContainerName()),
			zap.String(lkContainerID, container.GetContainerId()),
			zap.Error(err),
		)

		return 0
	}

	maxMetaMemory := int(uint64(container.GetMetagpuLimits()) * device.GetDevice().GetMemoryShareSize())
	metaMemUtilization := 0

	if maxMetaMemory > 0 {
		metaMemUtilization = (int(process.GetMemory()) * 100) / maxMetaMemory
	}

	return float64(metaMemUtilization)
}

func recordMetrics(logger *zap.Logger) {
	refreshInterval := viper.GetDuration(vpnRefreshInterval)

	go func() {
		for {
			conn, token, err := prepareEnv(logger)
			if err != nil {
				logger.Fatal("GRPC connection failed, can't continue", zap.Error(err))

				time.Sleep(refreshInterval)

				continue
			}

			// load devices cache
			setGpuDevicesCache(logger, conn, token)
			// set devices level metrics
			setDevicesMetrics(logger, conn, token)
			// set processes level metrics
			setProcessesMetrics(logger, conn, token)
			// close grcp connections
			conn.Close()
			// clear the cache
			clearGpuDevicesCache()
			time.Sleep(refreshInterval)
		}
	}()
}

func startExporter(logger *zap.Logger) {
	logger.Info("starting metagpu metrics exporter")

	prometheus.MustRegister(deviceShares)
	prometheus.MustRegister(deviceAllocatedShares)
	prometheus.MustRegister(deviceMemTotal)
	prometheus.MustRegister(deviceMemFree)
	prometheus.MustRegister(deviceMemUsed)
	prometheus.MustRegister(deviceMemShareSize)
	prometheus.MustRegister(deviceProcessAbsoluteGpuUtilization)
	prometheus.MustRegister(deviceProcessMemoryUsage)
	prometheus.MustRegister(deviceProcessMetagpuRequests)
	prometheus.MustRegister(deviceProcessMetagpuLimits)
	prometheus.MustRegister(deviceProcessRequestedMetagpuGPUUtilization)
	prometheus.MustRegister(deviceProcessMaxAllowedMetagpuGPUUtilization)
	prometheus.MustRegister(deviceProcessMetagpuRelativeGPUUtilization)
	prometheus.MustRegister(deviceProcessRequestedMetaGpuMemory)
	prometheus.MustRegister(deviceProcessMaxAllowedMetaGpuMemory)
	prometheus.MustRegister(deviceProcessMetagpuRelativeMemoryUtilization)
	recordMetrics(logger)

	addr := viper.GetString(vpnMetricsAddr)

	http.Handle("/metrics", promhttp.Handler())

	l, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Error("can't bind socket", zap.Error(err), zap.String("socket_addr", addr))

		return
	}

	logger.Sugar().Infof("metrics serving on http://%s/metrics", addr)

	if err := http.Serve(l, nil); err != nil {
		logger.Error("can't start metrics server", zap.Error(err))

		return
	}
}
