package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/log"
)

type param struct {
	name      string
	shorthand string
	value     any
	usage     string
}

// Viper parameter long names.
// vpn - Viper Parameter Name.
const (
	vpnMetricsAddr     = "metrics-addr"
	vpnMGSrv           = "mgsrv"
	vpnJSONLog         = "json-log"
	vpnVerbose         = "verbose"
	vpnRefreshInterval = "refresh-interval"
)

var (
	Version string
	Build   string
	rootCmd = &cobra.Command{
		Use:   "mgexporter",
		Short: "mgexporter - Metagpu metrics exporter",
	}
	version = &cobra.Command{
		Use:   "version",
		Short: "Print metagpu metric exporter version and build sha",
		Run: func(_ *cobra.Command, _ []string) {
			fmt.Printf("🐾 version: %s build: %s \n", Version, Build)
		},
	}
	startParams = []param{
		{name: vpnMetricsAddr, shorthand: "a", value: "0.0.0.0:2112", usage: "listen address"},
		{name: vpnMGSrv, shorthand: "s", value: "127.0.0.1:50052", usage: "metagpu device plugin gRPC server address"},
		{name: vpnJSONLog, shorthand: "", value: false, usage: "output logs in json format"},
		{name: vpnVerbose, shorthand: "", value: false, usage: "enable verbose logs"},
		{name: vpnRefreshInterval, shorthand: "", value: "15s", usage: "Interval between the exporter's cache refresh"},
	}

	start = &cobra.Command{
		Use:   "start",
		Short: "start metagpu metrics exporter",
		Run: func(_ *cobra.Command, args []string) {
			startExporter(zap.L())
		},
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	setParams(startParams, start)
	rootCmd.AddCommand(version)
	rootCmd.AddCommand(start)
}

func initConfig() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("MG_EX")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	setupLogging()
}

func setParams(params []param, command *cobra.Command) {
	for _, param := range params {
		switch defValue := param.value.(type) {
		case int:
			command.PersistentFlags().IntP(param.name, param.shorthand, defValue, param.usage)
		case string:
			command.PersistentFlags().StringP(param.name, param.shorthand, defValue, param.usage)
		case bool:
			command.PersistentFlags().BoolP(param.name, param.shorthand, defValue, param.usage)
		}

		if err := viper.BindPFlag(param.name, command.PersistentFlags().Lookup(param.name)); err != nil {
			panic(err)
		}
	}
}

func setupLogging() {
	logger := log.SetupLogging(viper.GetBool("verbose"), viper.GetBool("json-log"), "mgex")

	zap.ReplaceGlobals(logger)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		_, _ = fmt.Println(err)

		os.Exit(1)
	}
}
