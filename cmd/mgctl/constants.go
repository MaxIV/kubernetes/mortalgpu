package main

import (
	"errors"

	"google.golang.org/protobuf/encoding/protojson"
)

// MB just a megabyte.
const MB = 1024 * 1024

const (
	ColorCritical = "\u001B[31m"
	ColorGood     = "\u001B[32m"
	ColorWarning  = "\u001B[33m"
	ColorNeutral  = "\u001B[0m"
)

var (
	protoJSONPretty = protojson.MarshalOptions{ //nolint:gochecknoglobals // Protobuf JSON config.
		EmitUnpopulated:   true,
		EmitDefaultValues: true,
		Multiline:         true,
		UseProtoNames:     true,
		Indent:            "  ",
	}

	protoJSON = protojson.MarshalOptions{ //nolint:gochecknoglobals // Protobuf JSON config.
		EmitUnpopulated:   true,
		EmitDefaultValues: true,
		UseProtoNames:     true,
	}
)

type param struct {
	name      string
	shorthand string
	value     any
	usage     string
}

const (
	// flagAddr defines output format.
	flagOutput = "output"
	// flagOutputS short form of flagOutput.
	flagOutputS = "o"
	// flagJSONLog enables log json.
	flagJSONLog = "json-log"
	// flagVerbose enables verbose logging.
	flagVerbose = "verbose"
	// flagAddr MetaGPU server address, port.
	flagAddr = "addr"
	// flagAddrS short form of flagAddr.
	flagAddrS = "s"
	// flagPrettyOut enables indented JSON output for humans.
	flagPrettyOut = "pretty"
	// flagJSONPath JSON path value.
	flagJSONPath = "jsonpath"
)

var (
	Version    string     //nolint:gochecknoglobals // Cobra commands.
	Build      string     //nolint:gochecknoglobals // Cobra commands.
	rootParams = []param{ //nolint:gochecknoglobals // Cobra commands.
		{
			name: flagJSONLog, shorthand: "", value: false,
			usage: "output logs in json format",
		},
		{
			name: flagVerbose, shorthand: "", value: false,
			usage: "enable verbose logs",
		},
		{
			name: flagAddr, shorthand: flagAddrS, value: "localhost:50052",
			usage: "address to access the metagpu server",
		},
		{
			name: flagOutput, shorthand: flagOutputS, value: outTable,
			usage: "output format, one of: table|json|protojson|raw|jsonpath=\"{ .json.path }\"",
		},
		{
			name: flagPrettyOut, shorthand: "", value: false,
			usage: "pretty output for JSON",
		},
		{
			name: flagJSONPath, shorthand: "", value: ".",
			usage: "JSON path to filter with, for --output=jsonpath only",
		},
	}
)

type outFormat string

const (
	outJSON      outFormat = "json"
	outProtoJSON outFormat = "protojson"
	outTable     outFormat = "table"
	outRaw       outFormat = "raw"
	outJSONPath  outFormat = "jsonpath"
)

var (
	ErrUnsupportedOutputFormat = errors.New("output format is not supported")
	ErrJSONPathValueEmpty      = errors.New("empty JSONPath value, check help")
)

type printConfig struct {
	jsonPath string
	format   outFormat
	pretty   bool
}

// Just a list of reused print formats.
const (
	fmtRawPrint = "%+v\n"
	fmtMBPrint  = "%dMB"
)
