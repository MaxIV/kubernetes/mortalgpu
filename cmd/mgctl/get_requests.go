package main

import (
	"errors"
	"fmt"
	"io"
	"strconv"

	"github.com/spf13/cobra"
	"go.uber.org/zap"

	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
)

var getResourceRequestsCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra command
	Use:     "requests",
	Aliases: []string{"req"},
	Short:   "get GPU memory requests",
	Run: func(cmd *cobra.Command, _ []string) {
		logger := zap.L().Named("get_requests")

		if err := writeResourceRequests(logger, cmd.OutOrStdout()); err != nil {
			logger.Fatal("can not get resource requests", zap.Error(err))
		}
	},
}

var ErrUnexpectedContainersCount = errors.New("unexpected GPU containers count, expected 1")

func writeResourceRequests(log *zap.Logger, out io.Writer) error {
	logger := log.Named("writeResourceRequests")

	conn, token, err := prepareEnv(logger)
	if err != nil {
		return err
	}

	containersClient := pbdevice.NewDeviceServiceClient(conn)

	containers, err := containersClient.GetGpuContainers(
		ctlutils.AuthenticatedContext(token), &pbdevice.GetGpuContainersRequest{})
	if err != nil {
		return fmt.Errorf("can not get GPU containers: %w", err)
	}

	if l := logger.Check(zap.DebugLevel, "GetDevices GRPC response"); l != nil {
		l.Write(zap.Any("containers", containers))
	}

	if len(containers.GetGpuContainers()) != 1 {
		return fmt.Errorf("%w: received %d", ErrUnexpectedContainersCount, len(containers.GetGpuContainers()))
	}

	container1 := containers.GetGpuContainers()[0]

	memReq := (uint64(container1.GetMetagpuRequests()) *
		container1.GetContainerDevices()[0].GetDevice().GetMemoryShareSize())

	_, err = out.Write([]byte(strconv.FormatUint(memReq, 10))) //nolint:revive // Base 10 numbers system.
	if err != nil {
		return fmt.Errorf("can not write resource requests: %w", err)
	}

	return nil
}
