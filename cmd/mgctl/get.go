package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"
	"time"

	"atomicgo.dev/cursor"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
)

var getCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:     "get",
	Aliases: []string{"g"},
	Short:   "query information from MortalGPU",
}

var processGetParams = []param{ //nolint:gochecknoglobals // Cobra commands.
	{name: "watch", shorthand: "w", value: false, usage: "watch for the changes"},
}

var processesGetCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:     "processes",
	Aliases: []string{"p", "process"},
	Short:   "list container GPU processes and processes metadata",
	Run: func(cmd *cobra.Command, _ []string) {
		logger := zap.L().Named("get_processes")

		config := &printConfig{
			format:   outFormat(viper.GetString(flagOutput)),
			pretty:   viper.GetBool(flagPrettyOut),
			jsonPath: viper.GetString(flagJSONPath),
		}

		if viper.GetBool("watch") {
			if err := streamDevicesProcesses(logger, cmd.OutOrStdout(), config); err != nil {
				logger.Fatal("fail to stream devices processes", zap.Error(err))
			}
		} else {
			if err := writeDevicesProcesses(logger, cmd.OutOrStdout(), config); err != nil {
				logger.Fatal("fail to print devices processes", zap.Error(err))
			}
		}
	},
}

var getDevicesCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:     "devices",
	Aliases: []string{"d", "device"},
	Short:   "get information about allocated GPU devices",
	Run: func(cmd *cobra.Command, _ []string) {
		logger := zap.L().Named("get_devices")

		config := &printConfig{
			format:   outFormat(viper.GetString(flagOutput)),
			pretty:   viper.GetBool(flagPrettyOut),
			jsonPath: viper.GetString(flagJSONPath),
		}

		if err := printDevices(logger, cmd.OutOrStdout(), config); err != nil {
			logger.Fatal("fail to get allocated GPU devices", zap.Error(err))
		}
	},
}

func writeDevicesProcesses(log *zap.Logger, out io.Writer, config *printConfig) error {
	logger := log.Named("getDevicesProcesses")

	conn, token, err := prepareEnv(logger)
	if err != nil {
		return err
	}

	defer conn.Close()

	deviceClient := pbdevice.NewDeviceServiceClient(conn)

	if err := printProcessesOnce(logger, deviceClient, token, config, out); err != nil {
		return fmt.Errorf("fail to print processes: %w", err)
	}

	return nil
}

func streamDevicesProcesses(log *zap.Logger, out io.Writer, config *printConfig) error {
	logger := log.Named("streamDevicesProcesses")

	conn, token, err := prepareEnv(logger)
	if err != nil {
		return err
	}

	defer conn.Close()

	deviceClient := pbdevice.NewDeviceServiceClient(conn)
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	ticker := time.NewTicker(time.Second)

	for {
		select {
		case <-sigCh:
			ticker.Stop()
			cursor.ClearLine()
			logger.Info("shutting down")

			return nil
		case <-ticker.C:
			if err := printProcessesOnce(logger, deviceClient, token, config, out); err != nil {
				logger.Fatal("fail to print processes", zap.Error(err))
			}
		}
	}
}

func printDevices(log *zap.Logger, out io.Writer, config *printConfig) error {
	logger := log.Named("printDevices")

	conn, token, err := prepareEnv(logger)
	if err != nil {
		return err
	}

	deviceClient := pbdevice.NewDeviceServiceClient(conn)

	resp, err := deviceClient.GetDevices(
		ctlutils.AuthenticatedContext(token),
		&pbdevice.GetDevicesRequest{})
	if err != nil {
		logger.Fatal("can't GetDevices", zap.Error(err))
	}

	if l := logger.Check(zap.DebugLevel, "GetDevices GRPC response"); l != nil {
		l.Write(zap.Any("response", resp))
	}

	if err := (devicesPrinter2{}.FPrint(out, resp.GetDevice(), config)); err != nil {
		return fmt.Errorf("can not printDevices: %w", err)
	}

	return nil
}

func prepareEnv(log *zap.Logger) (*grpc.ClientConn, string, error) {
	logger := log.Named("preparePrintDeviceProcessesEnv")

	conn := ctlutils.GetGrpcMetaGpuSrvClientConn(logger, viper.GetString(flagAddr))
	if conn == nil {
		logger.Fatal("can't initiate connection to MortalGPU server", zap.String("server", viper.GetString(flagAddr)))
	}

	// Read Token from the predefined Service Account Token Location.
	token, err := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")
	if err != nil {
		return nil, "", fmt.Errorf("can not read Service Account Token File: %w", err)
	}

	return conn, string(token), nil
}

// printProcessesOnce prints GPU processes using given formatter and output stream.
func printProcessesOnce(log *zap.Logger,
	deviceClient pbdevice.DeviceServiceClient,
	mgdpToken string, config *printConfig, out io.Writer,
) error {
	gpuContainers, err := deviceClient.GetGpuContainers(
		ctlutils.AuthenticatedContext(mgdpToken),
		&pbdevice.GetGpuContainersRequest{})
	if errors.Is(err, io.EOF) {
		return nil
	}

	if err != nil {
		return fmt.Errorf("can not get GPU Containers: %w", err)
	}

	if l := log.Named("printProcessesOnce").Check(zap.DebugLevel, "GPRC response"); l != nil {
		l.Write(zap.Any("process_response", gpuContainers))
	}

	if err := (processesPrinter{}).FPrint(out, gpuContainers.GetGpuContainers(), config); err != nil {
		return fmt.Errorf("can not format output: %w", err)
	}

	return nil
}
