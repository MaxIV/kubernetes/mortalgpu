package main

import (
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/log"
)

var metaGpuCtlVersion = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:   "version",
	Short: "Print metagpuctl version and build sha",
	Run: func(cmd *cobra.Command, _ []string) {
		cmd.Printf("🐾 version: %s build: %s \n", Version, Build)
	},
}

var rootCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:   "mgctl",
	Short: "mgctl - cli client for metagpu management and monitoring",
}

func cobraInit() {
	cobra.OnInitialize(initConfig)
	setParams(processGetParams, processesGetCmd)
	setParams(rootParams, rootCmd)
	// processes
	getCmd.AddCommand(processesGetCmd)
	getCmd.AddCommand(getDevicesCmd)
	// allocation queries
	getCmd.AddCommand(getResourceRequestsCmd)
	getCmd.AddCommand(getResourceLimitsCmd)
	setParams(getResourceUtilizationParams, getResourceUtilizationCmd)
	getCmd.AddCommand(getResourceUtilizationCmd)
	// root commands
	rootCmd.AddCommand(getCmd)
	rootCmd.AddCommand(pingCmd)
	rootCmd.AddCommand(metaGpuCtlVersion)
}

func initConfig() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("MG_CTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	setupLogging()
}

func setParams(params []param, command *cobra.Command) {
	for i := range params {
		param := &params[i]

		switch paramVal := param.value.(type) {
		case int:
			command.PersistentFlags().IntP(param.name, param.shorthand, paramVal, param.usage)
		case string:
			command.PersistentFlags().StringP(param.name, param.shorthand, paramVal, param.usage)
		case outFormat:
			command.PersistentFlags().StringP(param.name, param.shorthand, string(paramVal), param.usage)
		case bool:
			command.PersistentFlags().BoolP(param.name, param.shorthand, paramVal, param.usage)
		}

		if err := viper.BindPFlag(param.name, command.PersistentFlags().Lookup(param.name)); err != nil {
			panic(err)
		}
	}
}

func setupLogging() {
	logger := log.SetupLogging(viper.GetBool("verbose"), viper.GetBool("json-log"), "mgctl")

	zap.ReplaceGlobals(logger)
}

func main() {
	cobraInit()

	if err := rootCmd.Execute(); err != nil {
		zap.L().Fatal("Can not execute command", zap.Error(err))
	}
}
