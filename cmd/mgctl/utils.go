package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"atomicgo.dev/cursor"
	"github.com/jedib0t/go-pretty/v6/table"

	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
)

type TableOutput struct {
	data         []byte
	header       table.Row
	footer       table.Row
	body         []table.Row
	lastPosition int
}

func (o *TableOutput) rowsCount() int {
	return 2 + len(o.body) //nolint:gomnd // 2 - to add one header and one footer.
}

// Write implements io.Writer.
func (o *TableOutput) Write(data []byte) (n int, err error) {
	o.data = append(o.data, data...)

	return len(data), nil
}

func (o *TableOutput) FPrint(out io.Writer) error {
	o.buildTable()

	if o.lastPosition > 0 {
		cursor.ClearLinesUp(o.lastPosition)
	}

	_, err := out.Write(o.data)
	if err != nil {
		return fmt.Errorf("can not FPrint data: %w", err)
	}

	o.lastPosition = o.rowsCount()

	return nil
}

func (o *TableOutput) buildTable() {
	o.data = nil

	rowConfigAutoMerge := table.RowConfig{AutoMerge: true}
	tbl := table.NewWriter()

	tbl.SetOutputMirror(o)
	tbl.AppendHeader(o.header, rowConfigAutoMerge)
	tbl.AppendRows(o.body)
	tbl.SetStyle(table.StyleColoredGreenWhiteOnBlack)
	tbl.AppendFooter(o.footer)
	tbl.Render()
}

func formatContainerDeviceIndexes(container *pbdevice.GpuContainer) string {
	var devIdxs []string

	for _, d := range container.GetContainerDevices() {
		devIdxs = append(devIdxs, strconv.FormatUint(uint64(d.GetDevice().GetIndex()), 10)) //nolint:revive // base-10 math.
	}

	if len(devIdxs) > 0 {
		return strings.Join(devIdxs, ":")
	}

	return "-"
}

func getPodName() (string, error) {
	override := os.Getenv("MGCTL_HOSTNAME_OVERRIDE")
	if override == "" {
		v, err := os.Hostname()
		if err != nil {
			return "", fmt.Errorf("can not determine Pod name: %w", err)
		}

		return v, nil
	}

	return override, nil
}
