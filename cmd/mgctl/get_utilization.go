package main

import (
	"fmt"
	"io"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
)

var getResourceUtilizationCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra command
	Use:     "utilization-treshold",
	Aliases: []string{"util"},
	Short:   "get GPU device memory utilization treshold",
	Run: func(cmd *cobra.Command, _ []string) {
		logger := zap.L().Named("get_utilization")

		if err := writeResourceUtilizationTreshold(logger, cmd.OutOrStdout(), viper.GetBool("limits")); err != nil {
			logger.Fatal("can not get GPU device memory utilization treshold", zap.Error(err))
		}
	},
}

var getResourceUtilizationParams = []param{ //nolint:gochecknoglobals // Cobra commands.
	{
		name:      "limits",
		shorthand: "l",
		value:     false,
		usage:     "output utilization treshold based on limits instead of requests",
	},
}

func writeResourceUtilizationTreshold(log *zap.Logger, out io.Writer, useLimits bool) error {
	logger := log.Named("writeResourceUtilizationTreshold")

	conn, token, err := prepareEnv(logger)
	if err != nil {
		return err
	}

	containersClient := pbdevice.NewDeviceServiceClient(conn)

	containers, err := containersClient.GetGpuContainers(
		ctlutils.AuthenticatedContext(token), &pbdevice.GetGpuContainersRequest{})
	if err != nil {
		return fmt.Errorf("can not get GPU containers: %w", err)
	}

	if l := logger.Check(zap.DebugLevel, "GetDevices GRPC response"); l != nil {
		l.Write(zap.Any("containers", containers))
	}

	if len(containers.GetGpuContainers()) != 1 {
		return fmt.Errorf("%w: received %d", ErrUnexpectedContainersCount, len(containers.GetGpuContainers()))
	}

	container1 := containers.GetGpuContainers()[0]

	var devicesTotalShares float64 = 0

	var memUtilization float64

	for _, dev := range container1.GetContainerDevices() {
		devicesTotalShares += float64(dev.GetDevice().GetShares())
	}

	if useLimits {
		memUtilization = float64(container1.GetMetagpuLimits()) / float64(devicesTotalShares)
	} else {
		memUtilization = float64(container1.GetMetagpuRequests()) / float64(devicesTotalShares)
	}

	_, err = out.Write([]byte(strconv.FormatFloat(memUtilization, 'f', 2, 64))) //nolint:revive // Formatting float to 2 decimal places
	if err != nil {
		return fmt.Errorf("can not write GPU memory utilization treshold: %w", err)
	}

	return nil
}
