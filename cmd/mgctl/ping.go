package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
)

var pingCmd = &cobra.Command{ //nolint:gochecknoglobals // Cobra commands.
	Use:   "ping",
	Short: "ping server to check connectivity",
	Run: func(_ *cobra.Command, _ []string) {
		conn := ctlutils.GetGrpcMetaGpuSrvClientConn(zap.L(), viper.GetString(flagAddr))

		logger := zap.L().Named("ping")

		if conn == nil {
			logger.Fatal("can't initiate connection to MortalGPU server", zap.String("server", viper.GetString(flagAddr)))
		} else {
			logger.Info("Ping Ok")
		}

		conn.Close()
	},
}
