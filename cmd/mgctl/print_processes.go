package main

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/jedib0t/go-pretty/v6/table"
	"k8s.io/client-go/util/jsonpath"

	pb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
)

type processesPrinter struct{}

func (printer processesPrinter) FPrint(out io.Writer,
	gpuContainers []*pb.GpuContainer, config *printConfig,
) error {
	switch config.format {
	case outJSON:
		return printer.outJSON(out, gpuContainers, config.pretty, false)
	case outProtoJSON:
		return printer.outJSON(out, gpuContainers, config.pretty, true)
	case outJSONPath:
		return printer.outJSONPath(out, gpuContainers, config.jsonPath)
	case outRaw:
		return printer.outRaw(out, gpuContainers)
	case outTable:
		return printer.outTable(out, gpuContainers)
	default:
		return fmt.Errorf("%w: GPU Processes: given format %q", ErrUnsupportedOutputFormat, config.format)
	}
}

func (processesPrinter) outJSON(out io.Writer, //nolint:revive // Flag parameter is okay here.
	gpuContainers []*pb.GpuContainer, pretty, proto3json bool,
) error {
	var (
		bts []byte
		err error
	)

	if proto3json {
		if pretty {
			bts, err = protoJSONPretty.Marshal(&pb.GpuContainers{Containers: gpuContainers})
		} else {
			bts, err = protoJSON.Marshal(&pb.GpuContainers{Containers: gpuContainers})
		}
	} else {
		if pretty {
			bts, err = json.MarshalIndent(&pb.GpuContainers{Containers: gpuContainers}, "", "  ")
		} else {
			bts, err = json.Marshal(&pb.GpuContainers{Containers: gpuContainers})
		}
	}

	if err != nil {
		return fmt.Errorf("can not Marshal JSON GPU processes: %w", err)
	}

	if _, err = out.Write(bts); err != nil {
		return fmt.Errorf("can not print GPU processes JSON: %w", err)
	}

	return nil
}

func (processesPrinter) outJSONPath(out io.Writer, gpuContainers []*pb.GpuContainer, jsonPath string) error {
	jPath := jsonpath.New(jsonPath)

	jPath.AllowMissingKeys(false)

	if err := jPath.Parse(jsonPath); err != nil {
		return fmt.Errorf("can not parse JSONPath template: %w", err)
	}

	if err := jPath.Execute(out, pb.GpuContainers{Containers: gpuContainers}); err != nil {
		return fmt.Errorf("GPU Processes JSONPath render error: %w", err)
	}

	return nil
}

func (processesPrinter) outRaw(out io.Writer, gpuContainers []*pb.GpuContainer) error {
	if _, err := fmt.Fprintf(out, fmtRawPrint, gpuContainers); err != nil {
		return fmt.Errorf("can not render GPU Processes raw view: %w", err)
	}

	return nil
}

func (processesPrinter) outTable(out io.Writer, gpuContainers []*pb.GpuContainer) error {
	tableOut := &TableOutput{}

	tableOut.header = table.Row{
		"Pod", "NS", "Device", "Node", "GPU",
		"Memory", "Pid", "Cmd", "Req/Limits",
	}

	// Compute table rows and totals.
	var (
		totalShares   uint64
		totalRequests int64
		totalLimits   int64
		totalUsedMem  uint64

		totalRequestsBytes uint64
		totalLimitsBytes   uint64
	)

	// Map container device UUIDs to devices for search purposes.
	gpuDevices := make(map[string]*pb.Device)

	for _, container := range gpuContainers {
		for _, containerDev := range container.GetContainerDevices() {
			gpuDev := containerDev.GetDevice()
			gpuDevices[gpuDev.GetDevuuid()] = gpuDev

			totalShares += uint64(gpuDev.GetShares())
		}
	}

	bodyRows := make([]table.Row, 0)

	for _, container := range gpuContainers {
		totalRequests += container.GetMetagpuRequests()
		totalLimits += container.GetMetagpuLimits()

		for _, process := range container.GetDeviceProcesses() {
			processDevice, ok := gpuDevices[process.GetDevuuid()]
			if !ok {
				continue
			}

			// Color GPU VRAM usage.
			processMem := process.GetMemory()

			totalUsedMem += processMem

			limMem := processDevice.GetMemoryShareSize() * uint64(container.GetMetagpuLimits())
			reqMem := processDevice.GetMemoryShareSize() * uint64(container.GetMetagpuRequests())

			totalLimitsBytes += limMem
			totalRequestsBytes += reqMem

			var gpuMemUsageColor string

			if processMem > limMem {
				gpuMemUsageColor = ColorCritical
			} else if processMem > reqMem {
				gpuMemUsageColor = ColorWarning
			} else {
				gpuMemUsageColor = ColorGood
			}

			memUsage := fmt.Sprintf("%s%dMB", gpuMemUsageColor, processMem/MB)

			// Color GPU compute usage as a proportion of the
			// usage to how much was requested.
			gpuComputeUsageColor := ColorGood
			relativeGpuUsageThreshold := 100 * container.GetMetagpuRequests() //nolint:gomnd,revive // 100 - percent.
			container.GetMetagpuLimits()
			relativeGPUUsage := float64(process.GetGpuUtilization()*processDevice.GetShares()) /
				float64(container.GetMetagpuLimits())

			if relativeGPUUsage > float64(relativeGpuUsageThreshold) {
				gpuComputeUsageColor = ColorWarning
			}

			if relativeGPUUsage >= 95 { //nolint:gomnd,revive // set 95% as the "RED" threshold.
				gpuComputeUsageColor = ColorCritical
			}

			gpuUsage := fmt.Sprintf("%s%d%%%s",
				gpuComputeUsageColor, int(relativeGPUUsage), ColorNeutral)

			bodyRows = append(bodyRows, table.Row{
				container.GetPodId(),
				container.GetPodNamespace(),
				formatContainerDeviceIndexes(container),
				container.GetNodeName(),
				gpuUsage, memUsage,
				process.GetPid(),
				process.GetCmdline(),
				fmt.Sprintf("%d/%d", container.GetMetagpuRequests(), container.GetMetagpuLimits()),
			})
		}
	}

	tableOut.body = bodyRows

	// Add footer.
	var totalColor string

	if totalUsedMem > totalLimitsBytes {
		totalColor = ColorCritical
	} else if totalUsedMem > totalRequestsBytes {
		totalColor = ColorWarning
	} else {
		totalColor = ColorNeutral
	}

	tableOut.footer = table.Row{
		len(gpuContainers),
		"", "", "", "",
		fmt.Sprintf("%s%d%s/%d/%dMB", totalColor, totalUsedMem/MB,
			ColorNeutral, totalRequestsBytes/MB, totalLimitsBytes/MB), "",
		"Total/Req/Lim:",
		fmt.Sprintf("%d/%d/%d", totalShares, totalRequests, totalLimits),
	}

	tableOut.buildTable()

	if err := tableOut.FPrint(out); err != nil {
		return fmt.Errorf("can not print GPU Processes table: %w", err)
	}

	return nil
}
