package main

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/jedib0t/go-pretty/v6/table"
	"k8s.io/client-go/util/jsonpath"

	pb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
)

type devicesPrinter2 struct{}

func (printer devicesPrinter2) FPrint(out io.Writer,
	devices map[string]*pb.Device, config *printConfig,
) error {
	switch config.format {
	case outJSON:
		return printer.outJSON(out, devices, config.pretty, false)
	case outProtoJSON:
		return printer.outJSON(out, devices, config.pretty, true)
	case outJSONPath:
		return printer.outJSONPath(out, devices, config.jsonPath)
	case outRaw:
		return printer.outRaw(out, devices)
	case outTable:
		return printer.outTable(out, devices)
	default:
		return fmt.Errorf("%w: GPU Devices: given format %q", ErrUnsupportedOutputFormat, config.format)
	}
}

func (devicesPrinter2) outJSONPath(out io.Writer, devices map[string]*pb.Device, jsonPath string) error {
	jPath := jsonpath.New(jsonPath)

	jPath.AllowMissingKeys(false)

	if err := jPath.Parse(jsonPath); err != nil {
		return fmt.Errorf("can not parse JSONPath template: %w", err) //nolint:goconst // No constant here.
	}

	devs := make([]*pb.Device, 0, len(devices))

	for _, d := range devices {
		devs = append(devs, d)
	}

	if err := jPath.Execute(out, &pb.Devices{Devices: devs}); err != nil {
		return fmt.Errorf("GPU Devices JSONPath render error: %w", err)
	}

	return nil
}

func (devicesPrinter2) outJSON(out io.Writer, //nolint:revive // Flag parameter is okay here.
	devices map[string]*pb.Device, pretty, proto3json bool,
) error {
	var (
		bts []byte
		err error

		devs = make([]*pb.Device, 0, len(devices))
	)

	for _, d := range devices {
		devs = append(devs, d)
	}

	if proto3json {
		if pretty {
			bts, err = protoJSONPretty.Marshal(&pb.Devices{Devices: devs})
		} else {
			bts, err = protoJSON.Marshal(&pb.Devices{Devices: devs})
		}
	} else {
		if pretty {
			bts, err = json.MarshalIndent(&pb.Devices{Devices: devs}, "", "  ")
		} else {
			bts, err = json.Marshal(&pb.Devices{Devices: devs})
		}
	}

	if err != nil {
		return fmt.Errorf("can not Marshal JSON Devices: %w", err)
	}

	if _, err = out.Write(bts); err != nil {
		return fmt.Errorf("can not print Devices JSON: %w", err)
	}

	return nil
}

func (devicesPrinter2) outRaw(out io.Writer, devices map[string]*pb.Device) error {
	if _, err := fmt.Fprintf(out, fmtRawPrint, devices); err != nil {
		return fmt.Errorf("can not render Devices raw view: %w", err)
	}

	return nil
}

func (devicesPrinter2) outTable(out io.Writer, devices map[string]*pb.Device) error {
	const (
		sharesFormat = "%d/%d"
		memFormat    = "%d/%dMB"
	)

	tableOut := &TableOutput{}

	tableOut.header = table.Row{"Idx", "UUID", "Memory", "Shares", "Share size, MB"}

	var (
		totMem     uint64
		usedMem    uint64
		totShares  uint32
		usedShares uint32
	)

	tableOut.body = make([]table.Row, 0, len(devices))

	for _, dev := range devices {
		totShares += dev.GetShares()
		usedShares += dev.GetAllocatedShares()
		totMem += dev.GetMemoryTotal()
		usedMem += dev.GetMemoryUsed()

		tableOut.body = append(tableOut.body, table.Row{
			dev.GetIndex(),
			dev.GetUuid(),
			fmt.Sprintf(memFormat, dev.GetMemoryUsed()/MB, dev.GetMemoryTotal()/MB),
			fmt.Sprintf(sharesFormat, dev.GetAllocatedShares(), dev.GetShares()),
			dev.GetMemoryShareSize() / MB,
		})
	}

	tableOut.footer = table.Row{
		fmt.Sprintf("CNT: %d", len(devices)), "",
		fmt.Sprintf(memFormat, usedMem/MB, totMem/MB),
		fmt.Sprintf(sharesFormat, usedShares, totShares), "",
	}

	if err := tableOut.FPrint(out); err != nil {
		return fmt.Errorf("can not render Devices table: %w", err)
	}

	return nil
}
